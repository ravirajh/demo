//USEUNIT FunctionLibrary
//USEUNIT GlobalVariables

//*************************************************************************
//Function Name    : fnInsertResult
//Sub Description  : To insert result in HTML Detailed report
//Inputs           : sDesc     -> Test step description
//                   sExpected -> Expected value of Test step
//                   sActual   -> Actual value of Test step
//                   sResult   -> status of the Test step
//                   sStepType -> Status of the test step - Proving/Non Proving
//Returns          : None 
//*************************************************************************
function fnInsertResult(sDesc, sStep, sExpected, sActual, sResult, sStepType){
  try{
  
     Indicator.PushText(sStep + " | " + sResult)
      
     var l_sFile;           //Bitmap File Name
     var FlagToIncrement = false;
     var StepArray = new Array();  //Variable to store step number
     StepArray = sStep.split(".")
     sResult = Trim(aqString.ToUpper(sResult));
     strDate=aqConvert.DateTimeToFormatStr(aqDateTime.Today(),"%d-%b-%Y");
     sTime = new Date();
     strTime1 = sTime.toTimeString();
     strTime = aqString.Replace(strTime1, ":", "_");
     var TagforTestStep = "";
    
     if (IsEmpty(sExpected) || IsNullorUndefined(sExpected)){
        Log.Warning(sStep+" - Expected Result is Incorrect");
     }
     if (IsEmpty(sActual) || IsNullorUndefined(sActual)){
        Log.Warning(sStep+" - Actual Result is Incorrect");
     }     
     if (!ContainsText(sStepType,"Proving")){ //Either it should be Proving or Non-Proving
        Log.Warning(sStep+" - Step Type is Incorrect");
     }
    
    //Checking whether the step is proving or non-proving
    if (sResult == "PASS"){
      sActual = aqString.Replace(sActual,"&","");    
    }
    else{
      sActual = aqString.Replace(sActual,"is&","is not");
      sActual = aqString.Replace(sActual,"is not not","is");  //Change double negation to positive
    }
    
     //Get the evidence for this step
    
     if(csvfilename1!=""){
          l_sFile=csvfilename;
     }else if(xmlfilename!=""){
          l_sFile=xmlfilename;     
      }
      else if(SettingsRequestxmlfilename!=""){
         l_sFile=SettingsRequestxmlfilename;     
      }   
      
      else if(GeneratedPDFReportTextFile!=""){
         l_sFile=GeneratedPDFReportTextFile;     
     }else{
          g_iCapture_Count = g_iCapture_Count + 1;
          var ScreenshotName =   strDate +"_"+ strTime +"_"+ g_iCapture_Count + ".png";
          GetScreenshot(sStepType, ScreenshotName, sResult);
          l_sFile =  g_sScreenName1 + "\\Screen " + ScreenshotName;      
     }     
    
     //Check whether Count needs to be incremented for this step.
     if(((aqString.Find(sStep,".1") >= 0) &&  (aqString.GetLength(StepArray[1])== 1)) || (aqString.Find(sStep,"NH") >= 0)){
          FlagToIncrement = true; //
     }else if((sStep.indexOf(".") != -1)){
          FlagToIncrement = false;
     }else{
          FlagToIncrement = true;
     }
     
     //Constuct the HTML tags for each cell
     var StartRowTag = "<TR COLS=7>";
     var sDescTag = "<TD class = 'wrapword' BGCOLOR=#EEEEEE WIDTH=10%><FONT FACE=VERDANA SIZE=2>" + sDesc + "</FONT></TD>";
     var sStepTag = "<TD class = 'wrapword' BGCOLOR=#EEEEEE WIDTH=9%><FONT FACE=VERDANA SIZE=2>" + sStep + "</FONT></TD>";
     var sStepTypeTag = "<TD class = 'wrapword' BGCOLOR=#EEEEEE WIDTH=9%><FONT FACE=VERDANA SIZE=2>" + sStepType + "</FONT></TD>";
     var sExpectedTag = "<TD class = 'wrapword' BGCOLOR=#EEEEEE WIDTH=25%><FONT FACE=VERDANA SIZE=2>" + sExpected + "</FONT></TD>";
     var sActualTag = "<TD class = 'wrapword' BGCOLOR=#EEEEEE WIDTH=25%><FONT FACE=VERDANA SIZE=2>" + sActual + "</FONT></TD>"
     var sActualTagWithLink = "<TD class = 'wrapword' BGCOLOR=#EEEEEE WIDTH=25%><FONT SIZE=5>&#9016;</FONT><FONT FACE=VERDANA SIZE=2><A HREF='" + l_sFile + "' target='_blank'>" + sActual + "</A></FONT></TD>";
     var sTimeTag = "<TD class = 'wrapword' BGCOLOR=#EEEEEE WIDTH=10%><FONT FACE=VERDANA SIZE=2>"+ strDate+" "+strTime1 +"</FONT></TD>";
     var sResultTagForPass = "<TD class = 'wrapword' BGCOLOR=#EEEEEE WIDTH=12% ><B><FONT COLOR=GREEN>&#10004;</FONT></B><FONT FACE=VERDANA SIZE=2 COLOR=GREEN><B>"+ sResult + "</A></B></FONT></TD>";
     var sResultTagForFail = "<TD class = 'wrapword' BGCOLOR=#EEEEEE WIDTH=12% ><B><FONT COLOR = RED>&#10008;</FONT></B><FONT FACE=VERDANA SIZE=2 COLOR=RED><B><font color=#FF0000>" + sResult + "</A></B></FONT></TD>";
     var StopRowTag = "</TR>";
     
     // Update the HTML tag with the screenshot link for VP.
     ProvingStep = aqString.Find(trim(sStepType),"Proving")
     if(ProvingStep==0) {       
          sActualTag =  sActualTagWithLink;
     }
     
     //Construct the HTML for PASS and FAIL Steps.
     if (sResult == "PASS"){
          TagforTestStep = StartRowTag + sDescTag + sStepTag + sStepTypeTag + sExpectedTag + sActualTag + sTimeTag + sResultTagForPass + StopRowTag;
          if (FlagToIncrement){
               g_iPass_Count = g_iPass_Count + 1; 
          }
     }else{
          TagforTestStep = StartRowTag + sDescTag + sStepTag + sStepTypeTag + sExpectedTag + sActualTagWithLink + sTimeTag + sResultTagForFail + StopRowTag;
          if (FlagToIncrement){
               g_iFail_Count = g_iFail_Count + 1;
          }                      
     }
          
     //To insert result with testcase description execution time and result
     g_DetailFile.Write(TagforTestStep);         
     Indicator.Clear();
  }catch(e){
     Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}  


//*************************************************************************
//Function Name    : fnOpenHtmlFile
//Sub Description  : To open a HTML file
//Inputs           : strSuiteName->Name of the HTML file
//Returns          : None 
//'*************************************************************************
function fnOpenHtmlFile(strSuiteName){
  try{   
  //Initialize the values
  g_objFS = Sys.OleObject("Scripting.FileSystemObject");//To create file system object   
  g_iTestcaseStatusPass_Count = 0;
  g_iTestcaseStatusFail_Count = 0;
  g_iTestcaseStatusExecuted_Count = 0;  
   
  // To change the current date format to DDMMYYYY 
  var RawCurrentTimeStamp = aqDateTime.Now();
  var strDate = aqConvert.DateTimeToFormatStr(RawCurrentTimeStamp,"%d-%b-%Y");
  var strTime = aqConvert.DateTimeToFormatStr(RawCurrentTimeStamp,"%I:%M:%S %p");
  var strTimeZone = (new Date()).toTimeString().split(" ")[1];
  var strTimeStamp = strDate + " " +  strTime + " " + strTimeZone;
  
  //Construct the respective path and update the global variables
  g_sResultsPath = g_sResultsPath + "\\" + strDate + "\\" + aqString.Replace(aqString.Replace(strTime, ":", "-"), " ", "-"); 
  g_sSummaryFileName = g_sResultsPath+ "\\Summary.html" ; 
  g_sFileName   = g_sResultsPath  + "\\Detailed.html"; 
  g_sScreenName = g_sResultsPath + "\\Bitmap" ;
  g_bitmap = g_sScreenName;
  g_sScreenName1 = "\Bitmap";
    
  //Create the result folders
  CreateFolders(g_sResultsPath);
  CreateFolders(g_sScreenName);
  
  //Initialize the HTML values before writing    
  var c_htmlres = "<TR><TD colspan =2 BGCOLOR=ffffcc><FONT FACE=VERDANA COLOR=NAVY SIZE=2><B>Automation Detailed Result :  " + strTimeStamp + " on Machine " + Sys.HostName+"</B></FONT></TD></TR>";
  var c_BrowserInfo = "<TR><TD BGCOLOR=ffffcc WIDTH=15.4%><FONT FACE=VERDANA COLOR=black SIZE=2><B>Browser:</B></FONT></TD><TD COLSPAN=6 BGCOLOR=ffffcc><FONT FACE=VERDANA COLOR=black SIZE=2><B>"+ g_browserinfo +" </B></FONT></TD></TR>";
  var c_OSInfo = "<TR><TD BGCOLOR=ffffcc WIDTH=15.4%><FONT FACE=VERDANA COLOR=black SIZE=2><B>Operating System:</B></FONT></TD><TD COLSPAN=6 BGCOLOR=ffffcc><FONT FACE=VERDANA COLOR=black SIZE=2><B>"+ g_OSInfo +" </B></FONT></TD></TR>";
  var c_TCInfo = "<TR><TD BGCOLOR=ffffcc WIDTH=15.4%><FONT FACE=VERDANA COLOR=black SIZE=2><B>TestComplete Version:</B></FONT></TD><TD COLSPAN=6 BGCOLOR=ffffcc><FONT FACE=VERDANA COLOR=black SIZE=2><B>"+ g_TCVersion +" </B></FONT></TD></TR>";
  var c_AppUrl = "<TR><TD BGCOLOR=ffffcc WIDTH=15.4%><FONT FACE=VERDANA COLOR=black SIZE=2><B>Application URL:</B></FONT></TD><TD COLSPAN=6 BGCOLOR=ffffcc><FONT FACE=VERDANA COLOR=black SIZE=2><B>"+ g_strWebAppURL +" </B></FONT></TD></TR>";
  var c_FrameworkVersion = "<TR><TD BGCOLOR=ffffcc WIDTH=15.4%><FONT FACE=VERDANA COLOR=black SIZE=2><B>Framework Version:</B></FONT></TD><TD COLSPAN=6 BGCOLOR=ffffcc><FONT FACE=VERDANA COLOR=black SIZE=2><B>"+ g_FrameworkVersion +" </B></FONT></TD></TR>";
  var c_FrameworkCheckSum = "<TR><TD BGCOLOR=ffffcc WIDTH=15.4%><FONT FACE=VERDANA COLOR=black SIZE=2><B>Framework CheckSum:</B></FONT></TD><TD COLSPAN=6 BGCOLOR=ffffcc><FONT FACE=VERDANA COLOR=black SIZE=2><B>"+ g_strFrameWorkChkSum +" </B></FONT></TD></TR>";
  var c_htmlappinfo = "<TR><TD BGCOLOR=ffffcc WIDTH=15.4%><FONT FACE=VERDANA COLOR=black SIZE=2><B>Application:</B></FONT></TD><TD COLSPAN=6 BGCOLOR=ffffcc><FONT FACE=VERDANA COLOR=black SIZE=2><B>" + g_strAppname+"</B></FONT></TD></TR>";

  //Updating information(date,time and hostname into html result file
  g_DetailFile = g_objFS.OpenTextFile(g_sFileName, 8, true,-1);//Open file as unicode text file
  g_DetailFile.Write (c_htmlbody);// specifying border and body in html file
  g_DetailFile.Write (c_htmlborder);  
  g_DetailFile.Write (c_htmlres);
  g_DetailFile.Write (c_BrowserInfo);
  g_DetailFile.Write (c_OSInfo);
  g_DetailFile.Write (c_TCInfo);
  g_DetailFile.Write (c_AppUrl);
  g_DetailFile.Write (c_FrameworkVersion);
  g_DetailFile.Write (c_FrameworkCheckSum);
  g_DetailFile.Write (c_htmlappinfo); 
  g_DetailFile.Write (c_htmlend);
  
  fnWriteSummary(strTimeStamp); 
  
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }         
}

//*************************************************************************
//Function Name   : fnWriteSummary
//Sub Description : To write HTML summary report
//Inputs          : strSuiteName->Name of the HTML file
//Returns         : None 
//*************************************************************************
function fnWriteSummary(strSuiteName){
  var APP_NAME;
  try{
  
  //Updating Function  result in summary file with date,time and host name
  var c_machineinfo = "<TR><TD colspan =4 BGCOLOR=ffffcc><FONT FACE=VERDANA COLOR=NAVY SIZE=2><B>Automation Summary Result: "+ strSuiteName +" on Machine " + Sys.HostName + "</B></FONT></TD></TR>";
  var c_BrowserInfo = "<TR><TD BGCOLOR=ffffcc WIDTH=10%><FONT FACE=VERDANA COLOR=black SIZE=2><B>Browser:</B></FONT></TD><TD COLSPAN=6 BGCOLOR=ffffcc><FONT FACE=VERDANA COLOR=black SIZE=2><B>"+ g_browserinfo +" </B></FONT></TD></TR>";
  var c_OSInfo = "<TR><TD BGCOLOR=ffffcc WIDTH=10%><FONT FACE=VERDANA COLOR=black SIZE=2><B>Operating System:</B></FONT></TD><TD COLSPAN=6 BGCOLOR=ffffcc><FONT FACE=VERDANA COLOR=black SIZE=2><B>"+ g_OSInfo +" </B></FONT></TD></TR>";
  var c_TCInfo = "<TR><TD BGCOLOR=ffffcc WIDTH=10%><FONT FACE=VERDANA COLOR=black SIZE=2><B>TestComplete Version:</B></FONT></TD><TD COLSPAN=6 BGCOLOR=ffffcc><FONT FACE=VERDANA COLOR=black SIZE=2><B>"+ g_TCVersion +" </B></FONT></TD></TR>";
  var c_AppUrl = "<TR><TD BGCOLOR=ffffcc WIDTH=10%><FONT FACE=VERDANA COLOR=black SIZE=2><B>Application URL:</B></FONT></TD><TD COLSPAN=6 BGCOLOR=ffffcc><FONT FACE=VERDANA COLOR=black SIZE=2><B>"+ g_strWebAppURL +" </B></FONT></TD></TR>";
  var c_FrameworkVersion = "<TR><TD BGCOLOR=ffffcc WIDTH=10%><FONT FACE=VERDANA COLOR=black SIZE=2><B>Framework Version:</B></FONT></TD><TD COLSPAN=6 BGCOLOR=ffffcc><FONT FACE=VERDANA COLOR=black SIZE=2><B>"+ g_FrameworkVersion +" </B></FONT></TD></TR>";
  var c_FrameworkCheckSum = "<TR><TD BGCOLOR=ffffcc WIDTH=15.4%><FONT FACE=VERDANA COLOR=black SIZE=2><B>Framework CheckSum:</B></FONT></TD><TD COLSPAN=6 BGCOLOR=ffffcc><FONT FACE=VERDANA COLOR=black SIZE=2><B>"+ g_strFrameWorkChkSum +" </B></FONT></TD></TR>";
  var c_appsummary = "<TR><TD BGCOLOR=ffffcc WIDTH=10%><FONT FACE=VERDANA COLOR=black SIZE=2><B>Application:</B></FONT></TD><TD COLSPAN=6 BGCOLOR=ffffcc><FONT FACE=VERDANA COLOR=black SIZE=2><B>"+ g_strAppname +" </B></FONT></TD></TR>";
  var c_testcasedesc = "<TR COLS=6><TD BGCOLOR=#FFCC99 WIDTH=20%><FONT FACE=VERDANA COLOR=BLACK SIZE=2><B>TestCase ID</B></FONT></TD><TD BGCOLOR=#FFCC99 WIDTH=55%><FONT FACE=VERDANA COLOR=BLACK SIZE=2><B>TestCaseName</B></FONT></TD><TD BGCOLOR=#FFCC99 WIDTH=10%><FONT FACE=VERDANA COLOR=BLACK SIZE=2><B>Result</B></FONT></TD><TD BGCOLOR=#FFCC99 WIDTH=15%><FONT FACE=VERDANA COLOR=BLACK SIZE=2><B>Execution Time</B></FONT></TD></TR>";
  
  //Updating application name
  g_SummaryFile = g_objFS.OpenTextFile(g_sSummaryFileName, 8, true,-1);   
  g_SummaryFile.Write (c_htmlbody);
  g_SummaryFile.Write (c_htmlborder);  
  g_SummaryFile.Write (c_machineinfo);
  g_SummaryFile.Write (c_BrowserInfo);
  g_SummaryFile.Write (c_OSInfo);
  g_SummaryFile.Write (c_TCInfo);
  g_SummaryFile.Write (c_AppUrl);
  g_SummaryFile.Write (c_FrameworkVersion);
  g_SummaryFile.Write (c_FrameworkCheckSum);
  g_SummaryFile.Write (c_appsummary);  
  g_SummaryFile.Write (c_testcasedesc);  
  
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }        
}

//*************************************************************************
// Function Name   : fnChangeDateFormat
// Description     : To change the current date format to "ddmmyyyy"
// Inputs          : None
// Returns         : Formatted Date
//*************************************************************************
function fnChangeDateFormat(DateFormat){ 
 //Declare variables
  var strMonth;
  var strYear;
  var strfixDate;
  var today = new Date();  //creating date object
  
  try{
  //Creating array of months
  var monthnames = new Array("January", "February", "March","April", "May", "June", "July", "August", "September","October", "November", "December");
  strDate = today.getDate();//To get the date in 'dd'format
  strMonth = today.getMonth();//To get the month
  strYear = today.getFullYear();  //To get the year name 
  
  switch(DateFormat){
    case "dd-mm-yyyy":
       strThisMonth = today.getMonth()+1;//To get the month
      if (strDate.length <= 2){
        strDate = "0" + strDate;
      }
      if (strMonth.length <= 2) {
        strMonth = "0" + strMonth;
      }
      strfixDate = strDate + "-" + strThisMonth + "-" + strYear;
      break;
    case "dd-mmmmmmmmm-yyyy":     
      if (strDate.length < 2){ 
      strDate = "0" + strDate;
      } 
      strfixDate = strDate+"-"+monthnames[strMonth]+"-"+strYear;
      break;
    }
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }      
  return strfixDate;//Returns date in dd-mmmmmmmmm-yyyy
}

//*************************************************************************
//Function Name   : fnCloseHtml
//Sub Description : To close HTML report
//Inputs          : None
//Returns         : None
//*************************************************************************
function fnCloseHtml(){
  try{  
    g_tEnd_Time = aqDateTime.Now();//get the current time
    g_iExecution_Count = g_iPass_Count + g_iFail_Count;
    g_StepsExecuted = g_iPass_Count + g_iFail_Count;
    g_StepsNotExecuted = g_TotalSteps - g_StepsExecuted;
    if (g_StepsNotExecuted != 0){
        Log.Error( g_StepsNotExecuted + " steps was not executed.");
    }
    var c_linebreak = "<TABLE><TR><TD BGCOLOR=#FFFFFF WIDTH=100% HEIGHT=25></TD></TR></TABLE>"; 
    var c_htmlpassedsteps = "</TABLE><TABLE><TR><TD BGCOLOR=BLACK WIDTH=18%><FONT FACE=VERDANA COLOR=WHITE SIZE=2><B>Passed Step(s) : " + g_iPass_Count+"</B></FONT></TD><TD BGCOLOR=BLACK WIDTH=20%><FONT FACE=VERDANA COLOR=WHITE SIZE=2><B>Failed Step(s) : " + g_iFail_Count+"</B></FONT></TD><TD BGCOLOR=BLACK WIDTH=20%><FONT FACE=VERDANA COLOR=WHITE SIZE=2><B>Step(s) Executed : " + g_StepsExecuted+"</B></FONT></TD><TD BGCOLOR=BLACK WIDTH=20%><FONT FACE=VERDANA COLOR=WHITE SIZE=2><B>Step(s) Not Executed : " + g_StepsNotExecuted+"</B></FONT></TD><TD BGCOLOR=BLACK WIDTH=22%><FONT FACE=VERDANA COLOR=WHITE SIZE=2><B>Execution Time: " + aqConvert.TimeIntervalToStr(aqDateTime.TimeInterval(g_tStart_Time,g_tEnd_Time))+" (Days:Hours:Minutes:Seconds).</B></FONT></TD></TR>"
    //To update the pass count,fail count of executed testcase and steps 
    g_DetailFile.Write (c_htmlpassedsteps);
    g_DetailFile.Write (c_htmlend);
    g_DetailFile.Write (c_linebreak);
    if (g_iFail_Count > 0) {
      Log.Error(g_TestDataSheet + " ended with " +g_iFail_Count+" failures");               
      g_iTestcaseStatusFail_Count = g_iTestcaseStatusFail_Count + 1;
    }
    else if (g_iPass_Count > 0) {
      g_iTestcaseStatusPass_Count = g_iTestcaseStatusPass_Count + 1;
    }
    g_iTestcaseStatusExecuted_Count = g_iTestcaseStatusPass_Count + g_iTestcaseStatusFail_Count;
   }
  catch(e){
  Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }     
}

//*************************************************************************
//Function Name   : fnCloseSummaryHtml
//Sub Description : To close HTML summary report
//Inputs          : None
//Returns         : None
//*************************************************************************
function fnCloseSummaryHtml(){  
  try{
    var c_htmltestcasepassed = "</TABLE><TABLE><TR><TD BGCOLOR=BLACK WIDTH=10%><FONT FACE=VERDANA COLOR=WHITE SIZE=2><B>Total TestCases Passed: " + g_iTestcaseStatusPass_Count +"</B></FONT></TD><TD BGCOLOR=BLACK WIDTH=10%><FONT FACE=VERDANA COLOR=WHITE SIZE=2><B>Total TestCases Failed: " + g_iTestcaseStatusFail_Count+"</B></FONT></TD><TD BGCOLOR=BLACK WIDTH=10%><FONT FACE=VERDANA COLOR=WHITE SIZE=2><B>Total TestCases Executed: " + g_iTestcaseStatusExecuted_Count+"</B></FONT></TD></TR>";
    //Updating total testcase passed and failed count to html result file.
    g_SummaryFile.Write (c_htmltestcasepassed);
    g_SummaryFile.Write (c_htmlend); 
    g_DetailFile.Close();   
    g_DetailFile = null;   
    g_SummaryFile.Close();
    g_SummaryFile = null ;
    g_objFS = null;
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }    
}

//*************************************************************************
//Function Name   : fnCloseSummaryHtmlModule
//Sub Description : To close HTML summary report for a module
//Inputs          : None 
//Returns         : None 
//*************************************************************************
function fnCloseSummaryHtmlModule(){ 
  try{ 
  var c_htmlpassedtestcase = "</TABLE><TABLE><TR><TD BGCOLOR=BLACK WIDTH=10%><FONT FACE=VERDANA COLOR=WHITE SIZE=2><B>Passed TestCases(s) : " + g_iTestcaseStatusPass_CountM+"</B></FONT></TD><TD BGCOLOR=BLACK WIDTH=9%><FONT FACE=VERDANA COLOR=WHITE SIZE=2><B>Failed TestCases(s) : " + g_iTestcaseStatusFail_CountM+"</B></FONT></TD><TD BGCOLOR=BLACK WIDTH=9%><FONT FACE=VERDANA COLOR=WHITE SIZE=2><B>Not Execute TestCases(s) : " + g_iTestcaseStatusNotExecuted_CountM+"</B></FONT></TD></TR>";
  g_SummaryFile.Write (c_htmlpassedtestcase);
  g_SummaryFile.Write (c_htmlend);   
  g_iTestcaseStatusPass_CountM = 0;
  g_iTestcaseStatusFail_CountM = 0;
  g_iTestcaseStatusNotExecuted_CountM = 0;
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }   
}

//*************************************************************************
//Function Name   : fnInsertSection
//Sub Description : To insert a section to HTML report
//Inputs          : strTestCase -> Test case name
//                  testCaseDesc -> Test case description
//Returns         : None 
//*************************************************************************
function fnInsertSection(strTestCase, testCaseDesc){
  g_iPass_Count = 0;
  g_iFail_Count = 0;
  g_iWarning_Count = 0;
  g_iCapt_Count = 0;
  g_tStart_Time = aqDateTime.Now();
    
  try{  
  g_DetailFile.Write ("<HTML><meta http-equiv="+"Content-Type"+" content="+"text/html; charset=UTF-8" +" /><BODY><a name=g_sTestCaseName></a>");
  g_DetailFile.Write ("<TABLE ID='" +g_TestDataSheet+ "' BORDER=1 CELLPADDING=3 CELLSPACING=1 WIDTH=100%>"); 
  if (aqString.Find(testCaseDesc,Chr(10))!=-1){
    testCaseDesc = aqString.Replace(testCaseDesc,Chr(10),"<BR />")
  }
  if(aqString.Find(strTestCase,Chr(10))!=-1){
  strTestCase = aqString.Replace(strTestCase,Chr(10),"<BR />")
  }
  var c_htmlreporttestcasedetail = "<TR><TD BGCOLOR=#EEEEEE WIDTH=15.4%><FONT FACE=VERDANA SIZE=2><B>Requirement:</B></FONT></TD><TD BGCOLOR=#EEEEEE COLSPAN=6><FONT FACE=VERDANA SIZE=2><B>" + strTestCase +"</B></FONT></TD></TR>";
  var c_htmlreporttestcasedesc = "<TR><TD BGCOLOR=#EEEEEE WIDTH=15.4%><FONT FACE=VERDANA SIZE=2><B>Test Design:</B></FONT></TD><TD BGCOLOR=#EEEEEE COLSPAN=6><FONT FACE=VERDANA SIZE=2><B>" + testCaseDesc + "</B></FONT></TD></TR>";
  var c_htmlreporttestcaseId = "<TR><TD BGCOLOR=#EEEEEE WIDTH=15.4%><FONT FACE=VERDANA SIZE=2><B>TestCase ID</B></FONT></TD><TD BGCOLOR=#EEEEEE COLSPAN=6><FONT FACE=VERDANA SIZE=2><B>" + g_sAutomationTestCase + "</B></FONT></TD></TR>";
  var c_htmlreporttestcasedata = "<TR COLS=7><TD class = 'wrapword' BGCOLOR=#FFCC99 WIDTH=10%><FONT FACE=VERDANA COLOR=BLACK SIZE=2><B>Keyword Verification</B></FONT></TD><TD class = 'wrapword' BGCOLOR=#FFCC99 WIDTH=8%><FONT FACE=VERDANA COLOR=BLACK SIZE=2><B>Step Number</B></FONT></TD><TD class = 'wrapword' BGCOLOR=#FFCC99 WIDTH=10%><FONT FACE=VERDANA COLOR=BLACK SIZE=2><B>Step Type</B></FONT></TD><TD class = 'wrapword' BGCOLOR=#FFCC99 WIDTH=25%><FONT FACE=VERDANA COLOR=BLACK SIZE=2><B>Expected Result</B></FONT></TD><TD class = 'wrapword' BGCOLOR=#FFCC99 WIDTH=25%><FONT FACE=VERDANA COLOR=BLACK SIZE=2><B>Actual Result</B></FONT></TD><TD class = 'wrapword' BGCOLOR=#FFCC99 WIDTH=10%><FONT FACE=VERDANA COLOR=BLACK SIZE=2><B>Execution Time</B></FONT></TD><TD class = 'wrapword' BGCOLOR=#FFCC99 WIDTH=10% ><FONT FACE=VERDANA COLOR=BLACK SIZE=2><B>Result</B></FONT></TD></TR>";
  //Pushing testcase name and decription to html result section
  g_DetailFile.Write (c_htmlreporttestcasedetail);
  g_DetailFile.Write (c_htmlreporttestcasedesc);    
  g_DetailFile.Write (c_htmlreporttestcaseId); 
  g_DetailFile.Write (c_htmlreporttestcasedata);
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }    
}
//*************************************************************************
//Function Name   : fnInsertSectionSummary
//Sub Description : To insert a section to HTML summary report
//Inputs          : strModuleName -> Module name
//Returns         : None 
//*************************************************************************
function fnInsertSectionSummary(strModuleName){ 
  try{
  g_SummaryFile.Write (c_htmlborder);//c_htmlborder ->borderspace
  var c_modulename = "<TR COLS=6><TD BGCOLOR=#EEEEEE WIDTH=25%><FONT FACE=VERDANA COLOR=BLACK SIZE=2><B>Module Name:</B></FONT></TD><TD BGCOLOR=#EEEEEE WIDTH=25%><FONT FACE=VERDANA COLOR=BLACK SIZE=2><B>"+strModuleName+"</B></FONT></TR>";
  g_SummaryFile.Write (c_modulename);
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }    
}
//*************************************************************************
//Function Name   : fnInsertSummaryResult
//Sub Description : To insert result in HTML summary report
//Inputs          : sTestCase -> Test case name
//                  sResult   -> status of the Test case
//Returns         : None
//*************************************************************************
function fnInsertSummaryResult(g_sAutomationTestcase,sTestCase,sResult){   
  try{ 
  strDate=aqConvert.DateTimeToFormatStr(aqDateTime.Today(),"%d-%b-%Y");
  sTime = new Date();
  strTime1 = sTime.toTimeString();
  var ss = "DummyRequirement";
  var c_htmlsummryres = "<TR COLS=5><TD BGCOLOR=#EEEEEE WIDTH=20%><FONT FACE=VERDANA SIZE=2>" + g_sAutomationTestcase + "</FONT></TD><TD BGCOLOR=#EEEEEE WIDTH=55%><FONT FACE=VERDANA SIZE=2>" + sTestCase + "</FONT></TD><TD BGCOLOR=#EEEEEE WIDTH=10%><FONT FACE=VERDANA SIZE=2><a href='detailed.html#" + g_TestDataSheet + "'>" + sResult + "</a></FONT></TD><TD BGCOLOR=#EEEEEE WIDTH=15%><FONT FACE=VERDANA SIZE=2>"+ strDate+" "+strTime1 +"</FONT></TD></TR>";
  //To insert testcase execution result to summary           
  g_SummaryFile.Write (c_htmlsummryres);
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }    
}

//'*************************************************************************
//'Function Name        : GetCurrentDateTime
//'Function Description : To get current date and time
//'Inputs               : Nothing
//'Returns              : Current Date and Time
//'************************************************************************
function GetCurrentDateTime(){
  try{
  //Declaring variables
  var d = new Date();//Creating date object
  var h = d.getHours();//To get the hour from date object
  var m = d.getMinutes();//To get the minute from date object
  var s = d.getSeconds();//To get the second from date object
  //Condition to check whether hour is in AM or PM format
  if (h >= 12){              
    period = "PM";
  } 
  else{
    period = "AM"; 
  } 
  hours = ((h > 12) ? h - 12 : h)   //To check whether the hours is in 'h' or in 'h-12'                       
  var strCurrentTime = hours + ':' + m + ':'+s;//To get the current time
  var strCurrentDate = fnChangeDateFormat("dd-mmmmmmmmm-yyyy");//To get the current date
  var strCurrentDateTime = strCurrentDate+ "  " +strCurrentTime+ "  " + period;//To get current date and time
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }    
  return strCurrentDateTime; 
}

//'*************************************************************************
//'Function Name        : CreateFolders
//'Function Description : To create folder in the path specified. If the path does not exist, then it would be created.
//'Inputs               : Folder Path which includes the new folder name also.
//'Returns              : true if the folder was created, false otherwise.
//'************************************************************************
function CreateFolders(strFolder){
  var ReturnValue = false;
  try{    
     if (g_objFS.FolderExists(strFolder)){
         ReturnValue =  true;
         return ReturnValue;
     }else{
         CreateFolders(g_objFS.GetParentFolderName(strFolder))
     }
     g_objFS.CreateFolder(strFolder);
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }    
  return ReturnValue;      
     
}


//'*************************************************************************
//'Function Name        : GetScreenshot
//'Function Description : To take screenshot for the Verification Point
//'Inputs               : StepNumber and FileName
//'Returns              : FilePath
//'************************************************************************
function GetScreenshot(StepType, FileName, StepStatus){
     
     var FilePath =  g_bitmap + "\\Screen " + FileName;
     var ScreenshotObject = null;
     var TakeFullPageScreenshot = false;
     try{
          var ProvingStep = aqString.Find(trim(StepType),"Proving");
          if ( ProvingStep==0 || StepStatus != "PASS" ){ //If the step is VP or a failure
           if (g_IsLastObjectWeb) { //If the last interacted object is Web
              TakeFullPageScreenshot = true;   
           }else{
              Log.Warning("Full page screenshot is not taken.")
           }
          }
          
          //Take Screenshot of the entire page if TakeFullPageScreenshot is set as true.
          if (TakeFullPageScreenshot){
               try{
                    if (IsNullorUndefined(oPageObject)){
                        oPageObject = eval(g_PageObject);
                    }
                    ScreenshotObject = oPageObject.PagePicture();
               }catch(PagePictureDidNotWork){
                    TakeFullPageScreenshot = false; //Incase, if unable to take PagePicture screenshot due to unexpected window.
                    Log.Warning("PagePicture was unable to take a screenshot.");
               }
          }
          
          //Take Desktop screenshot if TakeFullPageScreenshot is set as false. 
          if (!TakeFullPageScreenshot){
              ScreenshotObject = Sys.Desktop.Picture();
          }
          
          ScreenshotObject.SaveToFile(FilePath); 
       }catch(e){
           Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
       }finally{
            g_IsLastObjectWeb = false;//Switch to false after taking a screenshot.
       }
       return FilePath;                
}


