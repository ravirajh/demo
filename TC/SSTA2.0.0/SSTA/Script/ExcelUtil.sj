//USEUNIT GlobalVariables
//USEUNIT FunctionLibrary

//************************************* Global Variables for ExcelUtil*************
  
  var oCDConnection = null;
  var oCDRecordSet = null;  //RecordSet for Caluclation Data
  var oRDRecordSet = null;  //RecordSet for Regimen Data
  var oCDPrecisionRecordSet = null;
  var oTDConnection = null;
  var oTDRecordSet = null;
  var oLocaleFile = null; //Object to hold the Localization Property file.
  var TotalCalculationRecordsProcessed = 0;
  var CurrentRecordNU = 0;
  var SimulationReason = "";
  var LastExchangeStartTime = "";
  var LastDwellDuration = "";
  var sTestDataSheetName = null;
 
  
  
//*************************************************************************************************
// Function Name        : StartExcelUtil
// Function Description : To initialize a connection for calculation data and test data sheets
// Inputs               : N/A 
// Returns              : Connection object and connection strings
//*************************************************************************************************
  function StartExcelUtil(){  
    try{
      
      //Start Excel Util only for Adequest
      if (!ContainsText(g_strAppname,"Adequest")){
          return;
      }else{
          IsAdequest = true;    //Set the flag as true if it is Adequest
      }
      
      //Load for Calculation Data
      var CDString = BuildConnectionString("CalculationData.xlsx", true);
      oCDConnection = OpenConnection(CDString);
    
      //Load for Test Data
      sTestDataSheetName = FetchRegionData("EnvironmentName");
      var TDString = BuildConnectionString("TestData.xlsx", true);
      oTDConnection = OpenConnection(TDString);

      //Load for Localization
      var PropertyFileName = GetTestData("LocalizationFileName");
      var PropertyFilePath = "LocalizationData//messages_" + PropertyFileName + ".ini";
      oLocaleFile = LoadIniFile(PropertyFilePath);
      
    }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    }   
  }


//*************************************************************************************************
// Function Name        : BuildConnectionString
// Function Description : To construct a connection strings
// Inputs               : Excel File name and Mode(read or writable) 
// Returns              : Connection strings
//*************************************************************************************************
  function BuildConnectionString(FileName,IsReadOnly){ //SheetPath,Mode
    var ReturnValue = "";  //Initializing return value
    try{
      //If IsReadOnly is invalid, then make it as true.
      if (IsNullorUndefined(IsReadOnly)){
        IsReadOnly = true;
      }
      if (!(IsNullorUndefined(FileName))){  //Checking parameters are not empty
          ReturnValue = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + gStoresPath + FileName + ";Extended Properties='Excel 12.0 Xml;HDR=YES'"; //;IMEX=1
          //ReturnValue = "Driver={Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb)};DBQ=" + gStoresPath + FileName + "; ReadOnly="+  IsReadOnly+";";
      }
    }catch(e){
        Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    }
    return ReturnValue;
  }
  
//*************************************************************************************************
// Function Name        : OpenConnection
// Function Description : To Open a ADODB connection
// Inputs               : Connection string 
// Returns              : Connection Object
//*************************************************************************************************
  function OpenConnection(ConnectionString){
    var ReturnValue = null; //Initializing return value
    var oConnection = ADO.CreateADOConnection(); //Initializing ADODB connection
    try{
      if (!(IsNullorUndefined(ConnectionString))){  //Checking parameters are not empty
        oConnection.ConnectionString = ConnectionString;
        oConnection.LoginPrompt = false;
        oConnection.Open();
        if (oConnection.Connected){
          ReturnValue = oConnection;
        }
     }
    }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    }
    return ReturnValue; 
  }
  
//*************************************************************************************************
// Function Name        : RunSQLQuery
// Function Description : To get the record Set after executing the SQL Query on an Excel.
// Inputs               : Connection object and Query
// Returns              : Record set
//*************************************************************************************************
 function RunSQLQuery(oConnection, Query){ 
     var RecordSet = null;
     try{    
        if (oConnection.connected && !(IsNullorUndefined(Query))){
          RecordSet = oConnection.Execute_(Query);
        }
     }catch(e){
        Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
     }
     return RecordSet;
 }

//*************************************************************************************************
// Function Name        : CloseConnection
// Function Description : To close a ADODB connection
// Inputs               : connection object 
// Returns              : true or false
//*************************************************************************************************
  function CloseConnection(oConnection){
    var ReturnValue = false;
    try{
      if (IsObject(oConnection)) {
        if (oConnection.Connected){
          oConnection.close();
        }
        if (!oConnection.Connected){
          ReturnValue = true;
        }
      }
    }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    }
    return ReturnValue;
  } 

//*************************************************************************************************
// Function Name        : StopExcelUtil
// Function Description : To De-initialize all objects initialized or created in Excel Util
// Inputs               : N/A 
// Returns              : N/A
//**************************************************************************************************
  function StopExcelUtil(){ 
    try{
    
      //Stop Excel Util only for Adequest
      if (!IsAdequest){
          return;
      }
          
      //Release objects used for Calculation Data
      CDString = null;
      if (CloseConnection(oCDConnection)){
       oCDConnection = null; 
      }
      
      //Release objects used for Test Data
      TDString = null;
      if (CloseConnection(oTDConnection)){
       oTDConnection = null; 
      }
      
      //Release objects used for Localization Data
      oLocaleFile = null;
      
    }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);  
    }
  }

//*************************************************************************************************
// Function Name        : BuildQuery
// Function Description : To construct a query
// Inputs               : Table name and where part of Query
// Returns              : Full query
//**************************************************************************************************
  function BuildQuery(Condition,TableName){
    var ReturnValue = null; //Initializing ReturnValue 
    var ConditionPair = null; 
    var Splitted = null;
    var ConditionName = null;
    var ConditionValue = null;
    try{
        
        //Check the parameters passed
        if (IsNullorUndefined(TableName) && IsNullorUndefined(Condition)){
          return ReturnValue;
        }
        
        //If the last character is semicolon ; then remove it.
        var sLastCharacter = aqString.GetChar(Condition,Condition.length-1)
        if (sLastCharacter == ";"){
          Condition = Condition.substring(0,Condition.length-1)
        }
        
        ConditionPair = Condition.split(";");
          for (j = 0; j<ConditionPair.length; j++){
            
            //equalTo = operation. Ex: Gender = M
            if (ConditionPair[j].indexOf("=")>0) {
                Splitted = ConditionPair[j].split("=",2);
                ConditionName = Trim(Splitted[0]);
                ConditionValue = Trim(Splitted[1]);
                if (IsNullorUndefined(ConditionValue)){
                  ConditionValue = ConditionValue; //Allowing Null to be part of SQL Query.
                }else if (IsDate(ConditionValue)){
                  ConditionValue = "#"+ConditionValue+"#"; //Enclose date with #  
                }else if (IsNumeric(ConditionValue)){
                  ConditionValue = ConditionValue;                                   
                }else{ 
                    ConditionValue = "'"+ConditionValue+"'"; //Enclose string with '
                }
                ConditionPair[j]= ConditionName + " = " + ConditionValue; //Updated the array
            //In operation. Ex: Gender in (F,M)
            }else if (ConditionPair[j].indexOf(" in ")>0) {
                Splitted = ConditionPair[j].split(" in ",2);
                ConditionName = Trim(Splitted[0]);
                ConditionValue = Trim(Splitted[1]);
                ConditionValue = ReplaceAll(ConditionValue," ", "");
                
                //Get the first value from Condition and test it for String, numeric and date.
                var FirstCondition = ConditionValue.substring(1,ConditionValue.length);
                FirstCondition = FirstCondition.substring(0, FirstCondition.indexOf(","));
                FirstCondition = Trim(FirstCondition);
                var Delimiter = "";
                if (IsNullorUndefined(FirstCondition) || IsNumeric(FirstCondition)){
                    Delimiter = "";
                }else if (IsDate(FirstCondition)){
                  Delimiter = "#";
                }else{
                    Delimiter = "'";
                }
                
                //Make the CondidiotnValue to SQL Query format
                ConditionValue = ConditionValue.replace("(","("+Delimiter);
                ConditionValue = ConditionValue.replace(")",Delimiter+")");
                ConditionValue = ReplaceAll(ConditionValue,",",Delimiter+", "+Delimiter);
                ConditionPair[j]= ConditionName + " in " + ConditionValue; //Updated the array
            }
            
        }
        
          Condition = ConditionPair.join(" AND ");
          ReturnValue = "SELECT * from ["+TableName+"$] WHERE " + Condition;
    }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    }
    return ReturnValue;
  }
  
//*************************************************************************************************
// Function Name        : GetValueFromRecordSet
// Function Description : To get the value from the recordset
// Inputs               : Column name
// Returns              : value of respective column 
//**************************************************************************************************  
  function GetValueFromRecordSet(RecordSet,ColumnName){
    var ReturnValue = null;
    try{
      if (IsObject(RecordSet) && (!IsNullorUndefined(ColumnName))){
          if(!RecordSet.EOF){
              try{
                ReturnValue = RecordSet.Fields(ColumnName).value;
                if (!IsNullorUndefined(ReturnValue)) {
                      ReturnValue = Trim(ReturnValue);
                }else{
                      ReturnValue = "EMPTY"; //Set Empty string, as the Cell might be blank and it would have returned null.
                }                
              }catch(ColumnError){
                Log.Error(GetExceptionLocation(), "'" + ColumnName + "' column does not exist" , pmHighest);                
                ReturnValue = null;
              }
          }
      }
    }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    }
    return ReturnValue;
  }

//*************************************************************************************************
// Function Name        : GetCalculationData
// Function Description : To get the value of specified column from a calculation excel file
// Inputs               : Column name
// Returns              : Value of a specified column
//**************************************************************************************************  
  function GetCalculationData(ColumnName, IsRoundingRequired){
    var ReturnValue = null;
    var sPrecisionQuery =null;
    var oPrecisionRecordSet = null;
    var iDecimalPrecision = 0;
    try{
      if (!IsNullorUndefined(ColumnName)){
        
        //Get the value from Calculation Sheet
        ReturnValue = GetValueFromRecordSet(oCDRecordSet,ColumnName);
        
        if (CompareText(ReturnValue,"EMPTY")){
            ReturnValue = "0";    
        }
        
        if (IsRoundingRequired){
          //Get the Decimal Precision for the corresponding field.
          iDecimalPrecision = GetValueFromRecordSet(oCDPrecisionRecordSet,ColumnName);  
          if (!CompareText(iDecimalPrecision,"EMPTY")){
              ReturnValue = RoundOff(ReturnValue,iDecimalPrecision);
          }
        }
        
      }
    }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    }
    return ReturnValue;
  }
  
//*************************************************************************************************
// Function Name        : GetRegimenData
// Function Description : To get the value of specified column from a calculation excel file in the regimen sheet.
// Inputs               : Column name
// Returns              : Value of a specified column
//**************************************************************************************************  
  function GetRegimenData(ColumnName){
    var ReturnValue = null;
    
    try{
      if (!IsNullorUndefined(ColumnName)){
          
          //Get the Regiment Record Set if it is null.
          if (IsNullorUndefined(oRDRecordSet)){
               var oConnection = oCDConnection;
               var SqlQuery = BuildQuery("RECORDNU = " + CurrentRecordNU ,"RegimenData");
               SqlQuery = SqlQuery.replace(" WHERE "," WHERE RECORDNU is not NULL AND ");
               oRDRecordSet = RunSQLQuery(oConnection,SqlQuery);
           }      
        
            //Get the value from Calculation Sheet
            ReturnValue = GetValueFromRecordSet(oRDRecordSet,ColumnName);
            
            if (CompareText(ReturnValue,"EMPTY")){
                ReturnValue = "0";    
            }
      }
    }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    }
    return ReturnValue;
  }  

//*************************************************************************************************
// Function Name        : GetTestData
// Function Description : To get the value of specified column from TestData excel file
// Inputs               : Column name
// Returns              : Value of a specified column
//**************************************************************************************************  
  function GetTestData(DataKey){
    var ReturnValue = null;
    var sQuery = null;
    try{
      if (!IsNullorUndefined(DataKey)){
        sQuery = BuildQuery("Key = "+DataKey,sTestDataSheetName);
        oTDRecordSet = RunSQLQuery(oTDConnection,sQuery)
        if (oTDRecordSet.RecordCount>0){
          ReturnValue = GetValueFromRecordSet(oTDRecordSet,GetDicParam("DataSetName"))
        }  
      }
    }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    }
    return ReturnValue;
  }

//*****************************************************************************************************
// Function Name        : GetLocalizationData
// Function Description : To get the value of specified column from a LocalizationData excel file
// Inputs               : Key whose corresponding string is needed.
// Returns              : value stored against the Key in Locale file, if not found or error return null
//************************************************************************************************** 
  function GetLocalizationData(sKey){
    var ReturnValue = null;
    var sQuery 
    try{
      if (!IsNullorUndefined(sKey) && !IsEmpty(sKey)){
            if (ContainsText(sKey,";")){
            
                //Truncate the last character if it is ;
                if (aqString.GetChar(sKey,sKey.length-1) == ";") {
                  sKey = sKey.substring(0,sKey.length-1)
                }    
                
                //Parse the InputsValues   
                var Parameters = sKey.split(";");
                var ParameterIndex;
                var CurrentParameter;
                var CurrentOutput;
                ReturnValue = ReplaceAll(sKey,";","");
                for (ParameterIndex = 0; ParameterIndex<Parameters.length; ParameterIndex++){
                    CurrentParameter = Parameters[ParameterIndex];
                    CurrentOutput = GetValueFromIniFile(oLocaleFile, CurrentParameter);
                    ReturnValue = ReturnValue.replace(CurrentParameter,CurrentOutput);
                }                
            }else{
                ReturnValue = GetValueFromIniFile(oLocaleFile, sKey);
            }
      }else{
          Log.Warning("Invalid key to read the string from Localization property file.")
      }
    }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    }
    return ReturnValue;
  }
  
  
  
/**************************************************************************************************
  Function Name        : LoadIniFile
  Function Description : To Load any ini file.                   
  Inputs               : Relative File path from the stores folder.                   
  Returns              : returns the object if it was successful or else null.
/**************************************************************************************************/
  function LoadIniFile(FilePath){
   
      var PropertyFilePath = null;
      var oFile = null;
      var ReturnValue = false;
      
      try{      
          //PropertyFileName = "messages_" + LocaleName + ".ini"; 
          PropertyFilePath = gStoresPath + FilePath;
              
          //Load the ini file into an object.
          oFile = Storages.INI(PropertyFilePath);
        
          //Get the first subsection and check if any content is present.
          oFile = oFile.GetSubSectionByIndex(0);
          if (oFile.OptionCount > 0) {
              ReturnValue = oFile;
          }else{
              Log.Error("There are no content in " + PropertyFileName);
              oFile = null;
          }
      }catch(e){
         Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
      } 
      return ReturnValue;
  }
  
/**************************************************************************************************
  Function Name        : GetValueFromIniFile
  Function Description : To read value from ini files.                   
  Inputs               : oFile - ini file object
                       : sKey - Key whose value is needed.                   
  Returns              : returns the value if it was found or else null.
/**************************************************************************************************/  
  function GetValueFromIniFile(oFile, sKey){
      var ReturnValue = sKey;  
      try{  
          // Checks whether the value exists
          if (oFile.OptionExists(sKey)){   
              ReturnValue = oFile.GetOption(sKey, "Key Not available");
              ReturnValue = Trim(ReturnValue);
              ReturnValue = ConvertUnicode(ReturnValue);          
          }
      }catch(e){
          Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
      } 
      return ReturnValue;            
  }
