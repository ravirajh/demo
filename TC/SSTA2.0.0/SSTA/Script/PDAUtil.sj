//USEUNIT FunctionLibrary
//USEUNIT GlobalVariables
//USEUNIT ApplicationFunction

//*************************************************************************************************
// Function Name        : PDASearchPatient
// Function Description : Function placeholder to search a patient in PD Adequest.              
// Inputs               : Patient Name      
// Returns              : None
//**************************************************************************************************
function PDASearchPatient(){
  RunKeyword("Patient search failed in PD Adequest");
}

//*************************************************************************************************
// Function Name        : FieldValidation
// Function Description : Function placeholder for field validations             
// Inputs               : None      
// Returns              : None
//**************************************************************************************************
function FieldValidation(){
  PerformUserActions();
}
//*************************************************************************************************
// Function Name        : VerifyFieldRangeLabels
// Function Description : Function placeholder for fields range labels             
// Inputs               : None      
// Returns              : None
//**************************************************************************************************
function VerifyFieldRangeLabels(){
try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepCounter = 1; //Variable to store pass/fail count
    var strRangeObjPath="/../..//span[@class='range-label']";
    var strRangeObjPath;
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        strRangeObjName=(g_dicColNames_obj.item(strObjectName)).concat(strRangeObjPath);
        objName = BuildWebObj(strRangeObjName);  //Build object from the given object name     
        strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol     
        strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol      
        strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol   
        intStepCounter = DataEntry(strAction,objName,strInputValue,strObjectName+" fieldRangeLabel",strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepCounter,intIterCnt);
      }    
    }
  g_stepnum = strStep;
  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
  
}

//*************************************************************************************************
// Function Name        : Verify
// Function Description : Function placeholder for Verification Actions
// Inputs               : None      
// Returns              : None
//**************************************************************************************************
function Verify(){
  PerformUserActions();
}


//*************************************************************************************************
// Function Name        : VerifyTableRowCount
// Function Description : To verify that the child object count found by XPath matches.                    
// Inputs               : N/A     
// Returns              : N/A
//*************************************************************************************************

 function VerifyTableRowCount(){
 
  var sObjectName= "";     //Variable for storing object name  
  var sStepNo = "";           //Variable for storing step number
  var iStepStatus = 1;      //Initialize the status to Fail
  var iInputValue;
     
   try{
     
    sObjectName = ReadExcel(g_intStrtRow,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
    sStepNo = ReadExcel(g_intStrtRow,c_intStepCol);  //Reads the step from c_intStepCol
    iInputValue = ReadExcel(g_intStrtRow,c_intInputValCol);  //Reads the input value from c_intInputValCol
                  
    if (!IsNullorUndefined(sObjectName)){
        objName = BuildWebObj(g_dicColNames_obj.item(sObjectName));
        objName = GetChildObjectsByXPath(g_dicColNames_obj.item(sObjectName));
        ScrollIntoView(objName);
        RowsCount =  objName.length; 
        ActualResult = "Expected Object count is "+RowsCount+" and the actual is "+iInputValue;
        if (CompareText(RowsCount,iInputValue)){
          iStepStatus = 0;
          Log.Message(sStepNo + " - " + ActualResult);
        }else{
          Log.Error(sStepNo + " - " + ActualResult);  
        }                 
    } 
     g_stepnum = sStepNo;     
   }catch(e){
        Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
   }finally{
        VerificationPoint(g_strFuncCall,sStepNo,iStepStatus,g_intStrtRow);
   }
 }
 
//*************************************************************************************************
// Function Name        : AddPatientToAdequest
// Function Description : To add a sharesource patient to  Adequest             
// Inputs               : None      
// Returns              : None
//**************************************************************************************************
function AddPatientToAdequest(){
  PerformUserActions();
}

//*************************************************************************************************
// Function Name        : EditAdequestPatient
// Function Description : To edit Adequest Patient              
// Inputs               : None      
// Returns              : None
//**************************************************************************************************
function EditAdequestPatient(){
  PerformUserActions();
}

//*************************************************************************************************
// Function Name        : SharesourceSearchPatient
// Function Description : To search patient in the Sharesource Application               
// Inputs               : None      
// Returns              : None
//**************************************************************************************************
function SharesourceSearchPatient(){

  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value 
    var strAction;         //Variable for storing action 
    var strStep = "";           //Variable for storing step number
    var intStepStatus = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      intStepStatus = 1; 
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
        strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value rom c_intInputValCol    
        strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol    
        strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
        g_OtherInputValue = ReadExcel(intIterCnt,c_intInputVal1Col);
           
        //Build object
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName)); 
        if (CompareText(strObjectName,"PatAdm_PatSrch_FirstName") || CompareText(strObjectName,"PatAdm_PatSrch_LastName") || CompareText(strObjectName,"PatientFirstName_frame") || CompareText(strObjectName,"PatientLastName_frame")) {
          strInputValue = GetExternalData(g_OtherInputValue, strInputValue);  
          strInputValue = ConvertToPDAFormat(strInputValue,",",strObjectName);
          oObject = WaitForObject(objName);
          oObject.SetText(strInputValue);
          ExpectedMessage = strInputValue + " shall be entered in "+strObjectName;
          if (CompareText(oObject.value,strInputValue)){
            intStepStatus = 0; 
            ActualMessage = strInputValue + " is entered in "+strObjectName;
          }else{
            ActualMessage = strInputValue + " is not entered in "+strObjectName;
          }
        }else{            
          //This function performs the strActions specified in the excel 
          intStepStatus = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        }
        VerificationPoint(g_strFuncCall,strStep,intStepStatus,intIterCnt);
      }
    }    
    g_stepnum = strStep;
    } 
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
  
}

//*************************************************************************************************
// Function Name        : ConvertToPDAFormat
// Function Description : To Split the variable as delimeter basis               
// Inputs               : String, delimeter and object name      
// Returns              : Splitted value
//**************************************************************************************************
function ConvertToPDAFormat(sInputString,sDelimeter,oObjectName){
  var ReturnValue = sInputString;
  try{
    if (!IsNullorUndefined(sInputString) && !IsNullorUndefined(sDelimeter)){
      sInputString = CleanUpText((sInputString))
      SplittedValue = sInputString.split(sDelimeter);
      if (SplittedValue.length > 1){
        if (ContainsText(oObjectName,"FirstName")){  
          ReturnValue = Trim(SplittedValue[1]);
        }else if (ContainsText(oObjectName,"LastName")){
          ReturnValue = Trim(SplittedValue[0]);
        }else if (ContainsText(oObjectName,"BaxterPatientIDValue")){
           ReturnValue = Trim(SplittedValue[1]);    
        }else if (ContainsText(oObjectName,"Physician")){
          if (ContainsText(oObjectName,"PatientList")){
              SplittedValue[0] = SplittedValue[0].replace(".",""); //Removing "." from Salutation as its not displayed in PDAdequest.
              SplittedValue[1] = SplittedValue[1].replace(",","");
              ReturnValue = trim(SplittedValue[2])+" "+ trim(SplittedValue[1]);
          }else if (CompareText(oObjectName,"PatientIdentification_PhysicianValue")){
              SplittedValue[0] = SplittedValue[0].replace(".",""); //Removing "." from Salutation as its not displayed in PDAdequest.
              SplittedValue[1] = SplittedValue[1].replace(",","");
              ReturnValue = trim(SplittedValue[0])+" "+trim(SplittedValue[2])+" "+trim(SplittedValue[1]);
          }else{
          if (SplittedValue.length>2) {
            //If Salutation is present 
            SplittedValue[0] = SplittedValue[0].replace(".",""); //Removing "." from Salutation as its not displayed in PDAdequest.
            SplittedValue[0] = GetSalutation(SplittedValue[0]);
            SplittedValue[0] = GetExternalData("LocalizationData",SplittedValue[0]);
            ReturnValue = trim(SplittedValue[0])+" "+trim(SplittedValue[1])+", "+trim(SplittedValue[2]);
          }else{
            //If Salutation is not present
            ReturnValue = trim(SplittedValue[0])+", "+ trim(SplittedValue[1])
          }
          ReturnValue = ReturnValue.replace(",","");                     
           }
        }else if (ContainsText(oObjectName,"DiabeticStatus")){//Case for Diabetic value
          switch (sInputString){            
          case "Type 1 – Insulin Dependent":
              ReturnValue = GetExternalData("LocalizationData","diabeticType1");
              break;
            case "Type 2 – Non Insulin Dependent":
              ReturnValue = GetExternalData("LocalizationData","diabeticType2NonInsulin");
              break;
            case "Type 2 – Insulin Dependent":
              ReturnValue = GetExternalData("LocalizationData","diabeticType2Insulin");
              break;
            case "Diabetic – Type Unknown":
              ReturnValue = GetExternalData("LocalizationData","diabeticTypeUnknown");
              break;
            case "No":
              ReturnValue = GetExternalData("LocalizationData","diabeticNo");
              break;
            case "Unknown":
              ReturnValue = GetExternalData("LocalizationData","diabeticUnknown");
              break;
            case "Undisclosed":
              ReturnValue = GetExternalData("LocalizationData","diabeticUndisclosed");
              break;              
            default:              
              ReturnValue = sInputString.toUpperCase();
              break;
          }
        }else if (ContainsText(oObjectName,"Time") && ContainsText(sInputString,sDelimeter)){
          ReturnValue = (Number(SplittedValue[0])*60)+Number(SplittedValue[1]);
        }
      }
    }
  }catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
  return ReturnValue;
}

//*************************************************************************************************
// Function Name        : VerifyPatientListData
// Function Description : To verify specified patient is avilable in a given patient list web table               
// Inputs               : None      
// Returns              : None
//**************************************************************************************************
function VerifyPatientListData(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value  
    var strStep = "";           //Variable for storing step number
    var intStepStatus = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    var oApplicationObject=null;
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      //Read object name from excel 
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      g_OtherInputValue = ReadExcel(intIterCnt,c_intInputVal1Col);
      intStepStatus = 1; 
      if (strObjectName != null){
        //Build object
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name
        oApplicationObject = eval(objName);             
        //Read input value from excel
        strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
        strInputValue = GetExternalData(g_OtherInputValue, strInputValue);
        //Read strStep from excel
        strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
  
        strInputValue = ConvertToPDAFormat(strInputValue," ",strObjectName);       
        if (CompareText(strInputValue,"---")) {
          strInputValue = "";
        }
        
       }
        intStepStatus = VerifyTextEquals(strStep,strObjectName,oApplicationObject,strInputValue);
        VerificationPoint(g_strFuncCall,strStep,intStepStatus,intIterCnt);
      }
     
    g_stepnum = strStep;
  }catch(e){
   Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }
}

//*************************************************************************************************
// Function Name        : VerifyPatientListExists
// Function Description : To Verify Sharesource/PDA patientslist is available               
// Inputs               : None      
// Returns              : None
//**************************************************************************************************
function VerifyPatientListExists(){
  PerformUserActions();
}

//*************************************************************************************************
// Function Name        : VerifyPatientIdentification
// Function Description : To verify patient identification displayed correctly               
// Inputs               : None      
// Returns              : None
//**************************************************************************************************
function VerifyPatientIdentification(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value  
    var strStep = "";           //Variable for storing step number
    var intStepStatus = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    var oApplicationObject=null;
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      
      intStepStatus = 1; 
      
      //Read object name from excel 
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
      strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
      g_OtherInputValue = ReadExcel(intIterCnt,c_intInputVal1Col);
      if (strObjectName != null){
      
        //Build object
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name
        oApplicationObject = eval(objName); 
        ScrollIntoView(oApplicationObject) ;  		
        strInputValue = GetExternalData(g_OtherInputValue, strInputValue);
        if (!CompareText(strInputValue,"---")){
            if(ContainsText(strObjectName,"Physician")){
              strInputValue = ConvertToPDAFormat(strInputValue," ",strObjectName);
            }else if(CompareText(strObjectName,"PatientIdentification_BaxterPatientIDValue")){
              strInputValue = ConvertToPDAFormat(strInputValue,":",strObjectName);
            }else if(ContainsText(strObjectName,"DiabeticStatus")){
            if (!CompareText(g_OtherInputValue,"LocalizationData")){
                strInputValue = ConvertToPDAFormat(strInputValue,"",strObjectName);
              }           
            }else if(CompareText(strObjectName,"PatientIdentification_AgeValue")){
              var DOBYear = new Date(strInputValue);
              var CurrentYear = new Date();
              strInputValue = GetAge(DOBYear,CurrentYear);
            }else if(CompareText(strObjectName,"PatientIdentification_DOBValue")){          
              strInputValue = aqString.SubString(strInputValue, 0, 6) + aqString.SubString(strInputValue,strInputValue.length-5,5)  
            }else if(ContainsText(strObjectName,"SexValue")){          
              if (CompareText(g_OtherInputValue,"RunTimeData")){
              strInputValue = GetExternalData("LocalizationData",strInputValue);
              }  
            }
        }
        
        intStepStatus = VerifyTextEquals(strStep,strObjectName,oApplicationObject,strInputValue);
          VerificationPoint(g_strFuncCall,strStep,intStepStatus,intIterCnt);
      }
    }  
    g_stepnum = strStep;
  }
 catch(e){
   Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }
}

//*************************************************************************************************
// Function Name        : SelectPhysician
// Function Description : To interact with the Physcian name related objects.              
// Inputs               : None      
// Returns              : None
//**************************************************************************************************
function SelectPhysician(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var sInputValue;     //Variable for storing input value  
    var strStep = "";           //Variable for storing step number
    var intStepStatus = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    var oApplicationObject=null;
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      
      intStepStatus = 1; 
      
      //Read object name from excel 
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      if (strObjectName != null){
      
        //Build object
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name
        oApplicationObject = eval(objName); 
        ScrollIntoView(oApplicationObject) ;
          	
        strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
        strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol 
        sInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol

        //Parse the InputsValues
        sInputValue = CleanUpText(sInputValue);
        if (aqString.GetChar(sInputValue,sInputValue.length-1) == ";") {
          sInputValue = sInputValue.substring(0,sInputValue.length-1)
        }       
        //Convert the Parameter values if it needs to come from ExternalData
        if (ContainsText(sInputValue,"=")){
          var CurrentParameter = sInputValue.split("=");
          var ParameterName = Trim(CurrentParameter[0]);
          var ParameterValue = Trim(CurrentParameter[1]);
          sInputValue = GetExternalData(ParameterName, ParameterValue)
        }
        
        //sInputValue = ConvertToPDAFormat(sInputValue," ",strObjectName);
        intStepStatus = DataEntry(strAction,objName,sInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepStatus,intIterCnt);
      }
    }
    
  }catch(e){
   Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}

//*************************************************************************************************
// Function Name        : PDASelectDate
// Function Description : To select the date value in Adequest date widget      
// Inputs               : Calendar object, Date value
// Returns              : None
//**************************************************************************************************

function PDASelectDate(ObjectName, DateValue){

  ReturnValue = 1; //Fail
  ActualMessage = "Date " + DateValue + " is not selected in " +  ObjectName ;
  ExpectedMessage = "Date " + DateValue + " shall be selected in " +  ObjectName ;
  
  var DateSplitted = null;
  var DayValue = null;
  var MonthValue = null;
  var YearValue = null;
  var YearPeriod = null;
  var YearPeriodLower = null;
  var YearPeriodUpper = null;          
  var IsObjectFound = false;
  var MonthNames = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");  
  var Delay = 0.05;
  try{
    
    //Check if the Date is in the correct format
    if (IsNullorUndefined(DateValue) || IsEmpty(DateValue)){
     ActualMessage = "Date " + DateValue + " is not selected, since it is empty or null.";
     Log.Error(ActualMessage);
     return  ReturnValue;    
    }
    DateValue = Trim(DateValue);
    DateSplitted = DateValue.split("/");
    if (DateSplitted.length == 3){
      MonthValue = MonthNames[Trim(DateSplitted[0])-1];
      DayValue = Trim(DateSplitted[1]);
      YearValue = Trim(DateSplitted[2]);
    }else{
       ActualMessage = "Date " + DateValue + " is not selected, since it is not in the format dd MMM yyyy";
       Log.Error(ActualMessage);
       return  ReturnValue;        
    }
    
    var ActionObject = eval(BuildWebObj(g_dicColNames_obj.item(ObjectName)));
    if (!IsExists(ActionObject)){
     ActualMessage = "Date " + DateValue + " is not selected, since object " + ObjectName + " does not exist";
     Log.Error(ActualMessage);
     return  ReturnValue;
    }else{
     IsObjectFound = true;
     ActionObject.Click();//Click the field to display the calendar widget
     ScrollIntoView(ActionObject);
     Wait(Delay);
    }
    
    //Get the objects
    var Calendar_Widget = eval(BuildWebObj(g_dicColNames_obj.item("Calendar_Widget")));
    if (!IsExists(Calendar_Widget)){
     ActualMessage = "Date " + DateValue + " is not selected, since calendar is not displayed after clicking " + ObjectName;
     Log.Error(ActualMessage);
     return  ReturnValue;
    }
    
    //Check if the correct month and Year are displayed.
    var Calendar_SwitchYearOrMonth = eval(BuildWebObj(g_dicColNames_obj.item("Calendar_SwitchYearOrMonth")));
    var CurrentYearAndMonth = GetText(Calendar_SwitchYearOrMonth).split(" ");
    var CurrentYear = Trim(CurrentYearAndMonth[1]);
    var CurrentMonth = Trim(CurrentYearAndMonth[0]);
    CurrentMonth = aqString.SubString(CurrentMonth, 0, 3)
    var isYearFound = CompareText(CurrentYear,YearValue)
    var isMonthFound = CompareText(CurrentMonth,MonthValue);
    if (!isMonthFound) {
      Calendar_SwitchYearOrMonth.Click();//Click to go 'Select Month' widget
      Wait(Delay);
    }
    
    //Put the widget in the correct Year
    if (!isYearFound) {
      Calendar_SwitchYearOrMonth.Click();  //Click to go to 'Select Year' widget
      var Calendar_PreviousButton = eval(BuildWebObj(g_dicColNames_obj.item("Calendar_PreviousButton")));
      var Calendar_NextButton = eval(BuildWebObj(g_dicColNames_obj.item("Calendar_NextButton")));      
      while (!isYearFound){
        YearPeriod = GetText(Calendar_SwitchYearOrMonth);
        if (!ContainsText(YearPeriod,"-")){
          Calendar_SwitchYearOrMonth.Click(); 
          Wait(Delay);
        }
        YearPeriod = YearPeriod.split("-");
        YearPeriodLower = Trim(YearPeriod[0]);
        YearPeriodUpper = Trim(YearPeriod[1]);       
        if (YearPeriodLower > YearValue){
          Calendar_PreviousButton.Click(); // Click to go to lesser year
          Wait(Delay);
        }else if (YearPeriodUpper < YearValue){
          Calendar_NextButton.Click(); // Click to go to greater year
          Wait(Delay);
        }else{
          var Calendar_SelectYear =  g_dicColNames_obj.item("Calendar_SelectYear").replace("YearValue",YearValue);
          Calendar_SelectYear = eval(BuildWebObj(Calendar_SelectYear));
          Calendar_SelectYear.Click(); //Select the desired Year
          Wait(Delay);
          isYearFound = true;
          isMonthFound = false; //Set the value as false, even it was found before.  
        }     
      }//Loop until the Year is found.
    }
        
    //Select Month in the Calendar Widget
    if (!isMonthFound) {
      var Calendar_SelectMonth =  g_dicColNames_obj.item("Calendar_SelectMonth").replace("MonthValue",MonthValue);
      Calendar_SelectMonth = eval(BuildWebObj(Calendar_SelectMonth));
      Calendar_SelectMonth.Click(); //Select the desired Month
      Wait(Delay);
    }

    //Select Day in the Calendar Widget
    DayValue = aqConvert.VarToInt(DayValue); //To Convert String to Integer, so 08 is converted to 8. 
    var Calendar_SelectDay = g_dicColNames_obj.item("Calendar_SelectDay").replace("DayValue",DayValue);
    Calendar_SelectDay = eval(BuildWebObj(Calendar_SelectDay));
    Calendar_SelectDay.Click();
    Wait(Delay);

    //Check if the date is selected correctly.
    var ActualText = GetText(ActionObject);
    
    //Append Leading Zeros if it is not present in the input value.
    var DayValueLeadingZero  = aqConvert.VarToInt(Trim(DateSplitted[1]));;
    var MonthValueLeadingZero = aqConvert.VarToInt(Trim(DateSplitted[0]));
    DayValueLeadingZero =  (DayValueLeadingZero < 10) ? ("0" + DayValueLeadingZero) : DayValueLeadingZero;
    MonthValueLeadingZero =  (MonthValueLeadingZero < 10) ? ("0" + MonthValueLeadingZero) : MonthValueLeadingZero;
    var DateToCompare = MonthValueLeadingZero + "/" + DayValueLeadingZero + "/" + YearValue;
    if (CompareText(DateToCompare,ActualText)){
      ActualMessage = "Date " + DateValue + " is selected in " +  ObjectName ; 
      ReturnValue = 0; 
    }else{
      ActualMessage = "Date " + DateValue + " is not selected in " +  ObjectName ;
      Log.Error(ActualMessage);
    }
  
  }catch(e){
   Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }finally{
    if (IsObjectFound && IsExists(Calendar_Widget) && Calendar_Widget.Visible){
      ActionObject.Click(); //Click the field, to close the Calendar if it is still open.
      Log.Warning("Calendar was still open, so clicked again to close it.")
    }
  }
  return ReturnValue;
}

//*************************************************************************************************
// Function Name        : ConstructSQLQuery
// Function Description : To construct the SQL Queries           
// Inputs               : None
// Returns              : returns the constructed query as string
//**************************************************************************************************

function ConstructSQLQuery(){

    var sObjectName= "";     //Variable for storing object name
    var sInputValue = "";     //Variable for storing input value 
    var SqlQuery = "";
    var sStepNo = "";           //Variable for storing step number
    var iStepStatus = 1;
    var ShareSourceSchema = "NH_ST3_2C_CORERENAL";
    var PDASchema = "NH_ST3_2M_PDADEQUEST";
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword
    var Parameters = null;
    var ParameterIndex = 0;
    var CurrentParameter = null;
    var ParameterHolder = null;
    var isSQLFound = true;
    var PATIENT_ID = null;
    var ParameterCount = null;
     try{
      ShareSourceSchema = FetchRegionData("SharesourceSchema");
      PDASchema = FetchRegionData("AdequestSchema");     
      sObjectName= ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      sInputValue = ReadExcel(intIniRwNo,c_intInputValCol);  //Reads the input value from c_intInputValCol
      sStepNo = ReadExcel(intIniRwNo,c_intStepCol);  //Reads the step from c_intStepCol      
      ExpectedMessage = "SQL Query for "+sObjectName+" shall be constructed.";
      ActualMessage = "SQL Query for "+sObjectName+" is not constructed.";  

      //Parse the InputsValues
      sInputValue = CleanUpText(sInputValue);
      if (aqString.GetChar(sInputValue,sInputValue.length-1) == ";") {
        sInputValue = sInputValue.substring(0,sInputValue.length-1)
      }       
      Parameters = sInputValue.split(";");
      ParameterCount = Parameters.length;
      for (ParameterIndex = 0; ParameterIndex<ParameterCount; ParameterIndex++){
          Parameters[ParameterIndex] = Trim(Parameters[ParameterIndex]);
          CurrentParameter = Parameters[ParameterIndex];
          //Convert the Parameter values if it needs to come from ExternalData
          if (ContainsText(CurrentParameter,"=")){
            CurrentParameter = CurrentParameter.split("=");
            ParameterName = Trim(CurrentParameter[0]);
            ParameterValue = Trim(CurrentParameter[1]);
            Parameters[ParameterIndex] = GetExternalData(ParameterName, ParameterValue)
          }
          //Escape Quotes
          if (ContainsText(Parameters[ParameterIndex],"'")){
              Parameters[ParameterIndex] = Parameters[ParameterIndex].replace("'","''");
          }
       }
          
      //Get the SQL Query required. 
      switch(sObjectName){
        case "DeleteAll24Hours":
          PATIENT_ID = GetPatientIDQuery(Parameters[0],Parameters[1],"PATIENTID");
          SqlQuery = "Delete from PDASchema.TWENTY_FOUR_HOUR_COLLECTION where PATIENT_ID = ( "+ PATIENT_ID +" )";
          break;
        case "DeleteAll24HoursForClinic":
          PATIENT_ID = GetPatientIDQuery("",Parameters[0],"PATIENTID");
          SqlQuery = "Delete from PDASchema.TWENTY_FOUR_HOUR_COLLECTION where PATIENT_ID in ( "+ PATIENT_ID +" )";
          break;          
        case "DeleteAllPET":
          PATIENT_ID = GetPatientIDQuery(Parameters[0],Parameters[1],"PATIENTID");
          SqlQuery = "Delete from PDASchema.PET_COLLECTION where PATIENT_ID = ( "+ PATIENT_ID +" )";
          break;
        case "DeleteAllPETForClinic":
          PATIENT_ID = GetPatientIDQuery("",Parameters[0],"PATIENTID");
          SqlQuery = "Delete from PDASchema.PET_COLLECTION where PATIENT_ID in ( "+ PATIENT_ID +" )";
          break;  
        case "DeleteAllRegimenCycle":
          PATIENT_ID = GetPatientIDQuery(Parameters[0],Parameters[1],"PATIENTID");
          SqlQuery = "Delete from PDASchema.REGIMEN_CYCLE ";
          SqlQuery = SqlQuery + "where REGIMEN_CYCLE_ID in ( (Select REGIMEN_ID from PDASchema.REGIMEN ";
          SqlQuery = SqlQuery + "where TWENTY_FOUR_HOUR_COLLECTION_ID in (Select ID from PDASchema.TWENTY_FOUR_HOUR_COLLECTION where PATIENT_ID = ( "+ PATIENT_ID +" ) ) ";
          SqlQuery = SqlQuery + "or PET_COLLECTION_ID in (Select ID from PDASchema.PET_COLLECTION where PATIENT_ID = ( "+ PATIENT_ID +" ))))";
          break;
        case "DeleteAllRegimen":
          PATIENT_ID = GetPatientIDQuery(Parameters[0],Parameters[1],"PATIENTID");
          SqlQuery = "Delete from PDASchema.REGIMEN ";
          SqlQuery = SqlQuery + "where TWENTY_FOUR_HOUR_COLLECTION_ID in (Select ID from PDASchema.TWENTY_FOUR_HOUR_COLLECTION where PATIENT_ID = ( "+ PATIENT_ID +" ) )";
          SqlQuery = SqlQuery + "or PET_COLLECTION_ID in (Select ID from PDASchema.PET_COLLECTION where PATIENT_ID = ( "+ PATIENT_ID +" ))"; 
          break;  
        case "24HoursCount":
          PATIENT_ID = GetPatientIDQuery(Parameters[0],Parameters[1],"PATIENTID");
          SqlQuery = "Select COUNT(*) as RecordCount from PDASchema.TWENTY_FOUR_HOUR_COLLECTION where PATIENT_ID = ( "+ PATIENT_ID +" ) and ACTIVE = {2}";
          break;
        case "PETCount":
          PATIENT_ID = GetPatientIDQuery(Parameters[0],Parameters[1],"PATIENTID");
          SqlQuery = "Select COUNT(*) as RecordCount from PDASchema.PET_COLLECTION where PATIENT_ID = ( "+ PATIENT_ID +" ) and ACTIVE = {2}";
          break;
        case "RegimenCount":
          PATIENT_ID = GetPatientIDQuery(Parameters[0],Parameters[1],"PATIENTID");
          SqlQuery = "Select COUNT(*) as RecordCount from PDASchema.REGIMEN ";
          SqlQuery = SqlQuery + "where ACTIVE = {2} and (TWENTY_FOUR_HOUR_COLLECTION_ID in (Select ID from PDASchema.TWENTY_FOUR_HOUR_COLLECTION where PATIENT_ID = ( "+ PATIENT_ID +" ) )";
          SqlQuery = SqlQuery + "or PET_COLLECTION_ID in (Select ID from PDASchema.PET_COLLECTION where PATIENT_ID = ( "+ PATIENT_ID +" )))"; 
          break;  
        case "AdequestPatientCount":
          SqlQuery = "Select COUNT(*) as PatientCount from PDASchema.PDA_PATIENT where VISIBLE = {1} AND PATIENT_ID IN (";
          SqlQuery = SqlQuery + "Select Patient_ID from ShareSourceSchema.PATIENT where Status = 'A' and CAREOF_CLINIC_ID = (";
          SqlQuery = SqlQuery + "Select CLINIC_ID from ShareSourceSchema.CLINIC where NAME = '{0}'))";
          break;
        case "PatientCount":
          SqlQuery = "Select COUNT(*) as PatientCount from ShareSourceSchema.PATIENT where Status = 'A' and CAREOF_CLINIC_ID = (";
          SqlQuery = SqlQuery + "Select CLINIC_ID from ShareSourceSchema.CLINIC where NAME = '{0}') AND PATIENT_ID not in (";
          SqlQuery = SqlQuery + "Select Patient_ID from PDASchema.PDA_PATIENT)";
          break;
        case "UpdatePatientGenderAndDOB":
          PATIENT_ID = GetPatientIDQuery(Parameters[0],Parameters[1],"PERSONID");
          if (!CompareText(Parameters[2],"null")){
              Parameters[2] = "'" + Parameters[2] + "'";
          }
          if (!CompareText(Parameters[3],"null")){
              Parameters[3] = "'" + Parameters[3] + "'";
          }          
          SqlQuery = "update ShareSourceSchema.PERSON set Gender = {2}, Date_Of_Birth = TO_DATE({3}, 'MM/DD/YYYY') where PERSON_ID = ( "+ PATIENT_ID +" )";
          break;
        case "UpdatePatientPediatricStatus":
          PATIENT_ID = GetPatientIDQuery(Parameters[0],Parameters[1],"PATIENTID");
          if (Parameters[2] == "N" ){
              Parameters[2] = 0; //Adult
          }else if (Parameters[2] == "Y" ){
              Parameters[2] = 1; //Pediatric
          }
          SqlQuery = "update PDASchema.PDA_PATIENT set PEDIATRIC = {2} where PATIENT_ID = ( "+ PATIENT_ID +" )";
          break; 
        case "RemovePatientFromPDA":
          PATIENT_ID = GetPatientIDQuery(Parameters[0],Parameters[1],"PATIENTID");
          SqlQuery = "Delete from PDASchema.PDA_PATIENT where PATIENT_ID in ( "+ PATIENT_ID +" )";
          break;
        case "UpdatePatientVisibility":
          PATIENT_ID = GetPatientIDQuery(Parameters[0],Parameters[2],"PATIENTID");
          SqlQuery = "Update PDASchema.PDA_PATIENT set VISIBLE = '{1}' where PATIENT_ID in ( "+ PATIENT_ID +" )";
          break;          
        case "UpdatePDASettings":
          if (Parameters[0] == "W" ){
              Parameters[0] = "WATSON_AND_WATSON";
          }else if (Parameters[0] == "H" ){
              Parameters[0] = "HUME_AND_WEYERS";
          }
          if (Parameters[1] == "R" ){
              Parameters[1] = "RANDERSON";
          }else if (Parameters[1] == "B" ){
              Parameters[1] = "BERGSTROM";
          }             
          SqlQuery = SqlQuery + "update PDASchema.SETTINGS set BODY_VOLUME_METHOD = '{0}' ";
          SqlQuery = SqlQuery + ", NPCR_METHOD = '{1}'";
          SqlQuery = SqlQuery + ", CORRECTION_FACTOR = {2} ";
          SqlQuery = SqlQuery + "where CLINIC_ID = (Select CLINIC_ID from ShareSourceSchema.CLINIC where NAME = '{3}')";
          break;  
        case "UpdateCompanyRegion":
          if (!CompareText(Parameters[0],"null")){
              Parameters[0] = "'" + Parameters[0] + "'";
          }
          SqlQuery = "Update ShareSourceSchema.COMPANY set COMPANY_REGION = {0} where NAME = '{1}' ";
          break;                                        
        default:
          SqlQuery = "Invalid case for ConstructSQLQuery";
          isSQLFound = false;
		      Log.Error(sStepNo+" -"+ActualMessage);
      }
      
      if(isSQLFound){
        //Place the Parameter in the SQL Statements
        for (ParameterIndex = 0; ParameterIndex<ParameterCount; ParameterIndex++){
          ParameterHolder = "{"+ParameterIndex+"}";
          SqlQuery = SqlQuery.replace(ParameterHolder, Parameters[ParameterIndex]);
        }
      
        //Update the Schema Names, Patient ID's
        SqlQuery = ReplaceAll(SqlQuery, "ShareSourceSchema", ShareSourceSchema);
        SqlQuery = ReplaceAll(SqlQuery, "PDASchema", PDASchema);
        SqlQuery = ReplaceAll(SqlQuery, "= PATIENT_ID", PATIENT_ID);
        iStepStatus = 0;
        ActualMessage = "SQL Query for "+sObjectName+" is constructed.";
		Log.Message(sStepNo+" - "+ActualMessage);  
      }
      
      
      
     }catch(e){
          Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
     }finally{
          WriteExcel(intIniRwNo,c_intInputVal1Col,g_TestDataSheet,SqlQuery);//Write NULL before running the Query.
          VerificationPoint(g_strFuncCall,sStepNo,iStepStatus,intIniRwNo);
          g_stepnum = sStepNo;          
     }
     
     return iStepStatus;
}

/*************************************************************************************************
   Function Name        : GetPatientIDQuery
   Function Description : To replace all matching substrings with a new value             
   Inputs               : InputString - String to bo updated.
                        : FindString - String to bo searched.
                        : ReplaceString - String to bo replaced.                        
   Returns              : Updated string with the values replaced with new values    
*************************************************************************************************/ 
 function GetPatientIDQuery(PatientName, ClinicName, IDRequired){
 
    var PatientLastName = "";
    var PatientSecondName = ""; 
    var ReturnValue = "Select PATIENT.PATIENT_ID from ShareSourceSchema.PERSON PERSON INNER JOIN ShareSourceSchema.PATIENT PATIENT on PERSON.PERSON_ID = PATIENT.PERSON_ID ";
    ReturnValue = ReturnValue + "where PATIENT.CAREOF_CLINIC_ID = (Select CLINIC_ID from ShareSourceSchema.CLINIC where NAME = '{2}')";
    //Add Where caluse for Patient Name only when it is provided.
    if (Trim(PatientName).length > 0 ) {
      ReturnValue = ReturnValue + "and PERSON.LAST_NAME = '{0}' ";
      if (ContainsText(PatientName,", ")){
          ReturnValue = ReturnValue + "and PERSON.FIRST_NAME = '{1}' ";
      }
    }
    
    if (CompareText(IDRequired,"PERSONID")){
      ReturnValue = ReturnValue.replace("Select PATIENT.PATIENT_ID", "Select PERSON.PERSON_ID");
    }
    
    try{
        //Replace Patient Name only when it is provided.
        if (Trim(PatientName).length > 0 ){
            PatientName = PatientName.split(", ");
            PatientLastName = PatientName[0];
            PatientFirstName = PatientName[1];
            ReturnValue = ReturnValue.replace("{0}",PatientLastName);
            if (ContainsText(ReturnValue,"{1}")){
                ReturnValue = ReturnValue.replace("{1}",PatientFirstName);
            }
        }
        
        ReturnValue = ReturnValue.replace("{2}",ClinicName);
    }catch(e){
        Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    }
    return ReturnValue;
 }
//*************************************************************************************************
// Function Name        : VerifyMandatoryOptionalField
// Function Description : to verify whether the mandatory asterik symbol is displayed or not.
// Inputs               : 	Step - Step Number
//							ObjName - Name of the Object
//							objDesc - Object on which the verification should be done.
//							FieldType - mandatory or optional
// Returns              : 0 if Pass and 1 for Fail.
//**************************************************************************************************
 
 function VerifyMandatoryOptionalField(Step,ObjName,objDesc,FieldType){
  var ReturnValue = 1;
  
  try{
    var AttributeValue = GetAttribute(objDesc, "class");
    switch(FieldType){
      case "mandatory":
        ExpectedMessage = "Mandatory Symbol '*' shall be displayed for " + ObjName;
        ActualMessage = "Mandatory Symbol '*' is displayed for " + ObjName;
        if(!(ContainsText(AttributeValue,"mandatory"))){
          ActualMessage = "Mandatory Symbol '*' is not displayed for " + ObjName;
          strLogMessage = Step +" - "+ActualMessage; 
          Log.Error(strLogMessage);
        }else{
          ReturnValue = 0;
          strLogMessage = Step +" - "+ ActualMessage;
          Log.Message(strLogMessage);
        }
        break;
      case "optional":
        ExpectedMessage = "Mandatory Symbol '*' shall not be displayed for " + ObjName;
        ActualMessage = "Mandatory Symbol '*' is not displayed for " + ObjName;
        if(ContainsText(AttributeValue,"mandatory")){
          ActualMessage = "Mandatory Symbol '*' is displayed for " + ObjName;
          strLogMessage = Step +" - "+ActualMessage; 
          Log.Error(strLogMessage);
        }else{
          ReturnValue = 0;
          strLogMessage = Step +" - "+ ActualMessage;
          Log.Message(strLogMessage);
        }
        break;  
    }
  }catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
  return ReturnValue;               
 }
 
 

//*************************************************************************************************
// Function Name        : VerifyCalculations
// Function Description : To verify the calculations which does not have a direct mapping to a column in CalculationData               
// Inputs               : None      
// Returns              : None
//**************************************************************************************************
function VerifyCalculations(){
  try{
  
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value  
    var strStep = "";           //Variable for storing step number
    var intStepStatus = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    var Parameters = null;
    var Summation = 0;
    var sLastCharacter = "";
    
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      
      intStepStatus = 1; 
      
      //Read object name from excel 
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
      strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
      strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
      
      if (strObjectName != null){
      
        //Build object
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name
        
        //Process the Inpput Value
        if (aqString.GetChar(strInputValue,strInputValue.length-1) == ";") {
          strInputValue = strInputValue.substring(0,strInputValue.length-1)
        }        
        Parameters = strInputValue.split(";");
        for (ParameterIndex = 0; ParameterIndex<Parameters.length; ParameterIndex++){
          Parameters[ParameterIndex] = Trim(Parameters[ParameterIndex]);
          CurrentParameter = Parameters[ParameterIndex];
          //Convert the Parameter values if it needs to come from ExternalData
          if (ContainsText(CurrentParameter,"=")){
            CurrentParameter = CurrentParameter.split("=");
            ParameterName = Trim(CurrentParameter[0]);
            ParameterValue = Trim(CurrentParameter[1]);
            if (CompareText(ParameterName,"CalculationData")){
                Parameters[ParameterIndex] = GetCalculationData(ParameterValue,false);
            }else{
                Parameters[ParameterIndex] = GetExternalData(ParameterName, ParameterValue);
            }
          }
          Summation = Summation + aqConvert.StrToFloat(Parameters[ParameterIndex]);          
         }        
                  
        strInputValue = RoundOff(Summation,2); //Default Rounding for Calculation Output is 2
        intStepStatus = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepStatus,intIterCnt);
      }
    }  
    g_stepnum = strStep;
  }
 catch(e){
   Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }
} 

//*************************************************************************************************
// Function Name        : GetAmputationStatus
// Function Description : To get the Amputation status of the Record.                    
// Inputs               : N/A     
// Returns              : NO - if there is no Amputation.
//                      : UPPERBODY - if Amputation is only in UpperBody
//                      : LOWERBODY - if Amputation is only in LowerBody
//                      : BOTH - if Amputation is in both LowerBody and UpperBody.
//*************************************************************************************************
function GetAmputationStatus(){

  var ReturnValue = "NO";
  var AmputationUpperArm = 0;
  var AmputationForeArm = 0;
  var AmputationHand = 0;
  var AmputationThigh = 0;
  var AmputationLowerLeg = 0;
  var AmputationFoot = 0;
    
  try{

      AmputationUpperArm = GetExternalData("CalculationData","AmputationUpperArm");
      AmputationForeArm = GetExternalData("CalculationData","AmputationForeArm");
      AmputationHand = GetExternalData("CalculationData","AmputationHand");
      AmputationThigh = GetExternalData("CalculationData","AmputationThigh");
      AmputationLowerLeg = GetExternalData("CalculationData", "AmputationLowerLeg");
      AmputationFoot = GetExternalData("CalculationData","AmputationFoot");
      
      var UpperBody = AmputationUpperArm + AmputationForeArm + AmputationHand;
      var LowerBody = AmputationThigh + AmputationLowerLeg + AmputationFoot;
      
      if ((UpperBody + LowerBody) > 0 ){
          ReturnValue = "BOTH";
          if (UpperBody == 0){
              ReturnValue = "LOWERBODY"; 
          }else if (LowerBody == 0){
              ReturnValue = "UPPERBODY"; 
          }
      }
      return ReturnValue;
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}


//*************************************************************************************************
// Function Name        : UpdateAmputationFields
// Function Description : To the Amputation fields in the UI based on the values from Calculation File.                    
// Inputs               : N/A     
// Returns              : N/A
//*************************************************************************************************
function UpdateAmputationFields(){
 
  var sObjectName = null;     //Variable for storing object name  
  var sInputValue = null;
  var sAction = null;
  var sInputValue1 = null;
  var sStepNo = null;           //Variable for storing step number

  var intIniRwNo = g_intStrtRow;        //Variable for storing initial row number
  var intEndRwNo = g_intEndRow;        //Variable for storing end row number  
  var intStepStatus = 1;      //Initialize the status to Fail
  var strData = null;
  var AmputationStatus = null;
     
  try{
      AmputationStatus = GetAmputationStatus();
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      
          //Initialize the status to fail for each each step.
          intStepStatus = 1;  //Fail
      
          sObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
          sInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
          sAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
          sInputValue1 = ReadExcel(intIterCnt,c_intInputVal1Col);
          sStepNo = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
          
          if (sObjectName != null){
              if(CompareText(sObjectName,"Amputee_Status_Dropdown")){
                  //Select Yes, if the patient is an amputee
                  if (CompareText(AmputationStatus,"NO")){
                      sInputValue = "no"
                  }else{
                      sInputValue = "yes"
                  }
                  
              }else if(ContainsText(sObjectName,"Amputee_HeightWithoutAmputation_Textbox")){
                  //Do not Enter Amputation Without Height, when only Upper Body is amputated.
                  if(CompareText(AmputationStatus,"UPPERBODY")){
                      sAction = "VerifyNotExist";  
                  }else{
                      sAction = "KeyInData";
                  }
              }else if(ContainsText(sObjectName,"Left")){
                  //Mark the Left Amputation Checkboxes as Yes, if the value is 1 or 2.
                  strData = GetExternalData(sInputValue1,sInputValue);
                  sAction = "Unchecked";
                  if (CompareText(strData,"1") || CompareText(strData,"2")){
                      sAction = "Checked";
                  }
              }else if(ContainsText(sObjectName,"Right")){
                  //Mark the Right Amputation Checkboxes as Yes, only if the value is 2
                  strData = GetExternalData(sInputValue1,sInputValue);
                  sAction = "Unchecked";
                  if (CompareText(strData,"2")){
                      sAction = "Checked";
                  }
              }
    
              //Perform the Action
              objName = BuildWebObj(g_dicColNames_obj.item(sObjectName));  //Build object from the given object name
              intStepStatus = DataEntry(sAction,objName,sInputValue,sObjectName,sStepNo);
              VerificationPoint(g_strFuncCall,sStepNo,intStepStatus,intIterCnt);
              
              //If not amputated, then process only the first row  to set No to Amputee drop Down field.
              if (CompareText(AmputationStatus,"NO")){
                  intIterCnt = intEndRwNo +1;
              }
          }
          
      } 
      g_stepnum = sStepNo; 
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}
 



//*************************************************************************************************
// Function Name        : VerifyAmputeeCalculations
// Function Description : To the Amputation fields in the UI based on the values from Calculation File.                    
// Inputs               : N/A     
// Returns              : N/A
//*************************************************************************************************
function VerifyAmputeeCalculations(){
 
  var sObjectName = null;     //Variable for storing object name  
  var sInputValue = null;
  var sAction = null;
  var sInputValue1 = null;
  var sStepNo = null;           //Variable for storing step number

  var intIniRwNo = g_intStrtRow;        //Variable for storing initial row number
  var intEndRwNo = g_intEndRow;        //Variable for storing end row number  
  var intStepStatus = 1;      //Initialize the status to Fail
  
  var AmputationStatus = null;
     
  try{
      AmputationStatus = GetAmputationStatus();
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      
          //Initialize the status to fail for each each step.
          intStepStatus = 1;  //Fail
      
          sObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
          sInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
          sAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
          sInputValue1 = ReadExcel(intIterCnt,c_intInputVal1Col);
          sStepNo = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
          
          if (sObjectName != null){
              
              //If not amputated, then process the first row alone to set No to Amputee drop Down field.
              sAction = "VerifyCalculatedValues";
              if(CompareText(AmputationStatus,"NO")){
                  sAction = "VerifyNotExist";  
              }
              
              //Perform the Action
              objName = BuildWebObj(g_dicColNames_obj.item(sObjectName));  //Build object from the given object name
              intStepStatus = DataEntry(sAction,objName,sInputValue,sObjectName,sStepNo);
              VerificationPoint(g_strFuncCall,sStepNo,intStepStatus,intIterCnt);
          }
          
      } 
      g_stepnum = sStepNo; 
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}


//*************************************************************************************************
// Function Name        : UpdateSolutionFields
// Function Description : To the Solution fields in the UI based on the values from Calculation File.                    
// Inputs               : N/A     
// Returns              : N/A
//*************************************************************************************************
function UpdateSolutionFields(){
 
  var sObjectName = null;     //Variable for storing object name  
  var sInputValue = null;
  var sAction = null;
  var sInputValue1 = null;
  var sStepNo = null;           //Variable for storing step number

  var intIniRwNo = g_intStrtRow;        //Variable for storing initial row number
  var intEndRwNo = g_intEndRow;        //Variable for storing end row number  
  var intStepStatus = 1;      //Initialize the status to Fail
  var strData = null;
     
  try{
      
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      
          //Initialize the status to fail for each each step.
          intStepStatus = 1;  //Fail
      
          sObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
          sInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
          sAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
          sInputValue1 = ReadExcel(intIterCnt,c_intInputVal1Col);
          sStepNo = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
          
          if (sObjectName != null){
              //Perform the Action
              objName = BuildWebObj(g_dicColNames_obj.item(sObjectName));  //Build object from the given object name
              sInputValue = GetSolutionValue(sInputValue,sObjectName);
              intStepStatus = DataEntry(sAction,objName,sInputValue,sObjectName,sStepNo);
              VerificationPoint(g_strFuncCall,sStepNo,intStepStatus,intIterCnt);
          }
          
      } 
      g_stepnum = sStepNo; 
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}




//*************************************************************************************************
// Function Name        : UpdateTimeFields
// Function Description : To the Time fields in the UI based on the values from Calculation File.                    
// Inputs               : N/A     
// Returns              : N/A
//*************************************************************************************************
function UpdateTimeFields(){
 
  var sObjectName = null;     //Variable for storing object name  
  var sInputValue = null;
  var sAction = null;
  var sInputValue1 = null;
  var sStepNo = null;           //Variable for storing step number

  var intIniRwNo = g_intStrtRow;        //Variable for storing initial row number
  var intEndRwNo = g_intEndRow;        //Variable for storing end row number  
  var intStepStatus = 1;      //Initialize the status to Fail
  var strData = null;
  var Hour = 0;
  var Minutes = 0 ;
     
  try{
      
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      
          //Initialize the status to fail for each each step.
          intStepStatus = 1;  //Fail
      
          sObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
          sInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
          sAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
          sInputValue1 = ReadExcel(intIterCnt,c_intInputVal1Col);
          sStepNo = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
          
          //Read the value from CalculationData always.
          sInputValue = GetExternalData("CalculationData",sInputValue);
          if (IsNumeric(sInputValue) && aqConvert.VarToStr(sInputValue).length > 0 ){
              Hour = Math.floor(sInputValue / 60);
              Hour = (Hour<10) ? "0" + Hour : Hour;
              Minutes = (sInputValue % 60);
              Minutes = (Minutes<10) ? "0" + Minutes : Minutes;
              sInputValue = Hour + ":" + Minutes;
          }else{
              sInputValue = "0:00";
          }
          
          if (sObjectName != null){
              //Perform the Action
              objName = BuildWebObj(g_dicColNames_obj.item(sObjectName));  //Build object from the given object name
              intStepStatus = DataEntry(sAction,objName,sInputValue,sObjectName,sStepNo);
              VerificationPoint(g_strFuncCall,sStepNo,intStepStatus,intIterCnt);

          }
      } 
      g_stepnum = sStepNo; 
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}


//*************************************************************************************************
// Function Name        : VerifyPETinformation
// Function Description : To verify the Transport type and Simulation status of a PET record based on the calculation file.                    
// Inputs               : N/A     
// Returns              : N/A
//*************************************************************************************************
function VerifyPETinformation(){
 
  var sObjectName = null;     //Variable for storing object name  
  var sInputValue = null;
  var sAction = null;
  var sInputValue1 = null;
  var sStepNo = null;           //Variable for storing step number

  var intIniRwNo = g_intStrtRow;        //Variable for storing initial row number
  var intEndRwNo = g_intEndRow;        //Variable for storing end row number  
  var intStepStatus = 1;      //Initialize the status to Fail
  var strData = null;
  var isSimulated = null;  
  try{
      isSimulated = GetSimulationStatus();
      
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      
          //Initialize the status to fail for each each step.
          intStepStatus = 1;  //Fail
      
          sObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
          sInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
          sAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
          sInputValue1 = ReadExcel(intIterCnt,c_intInputVal1Col);
          sStepNo = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol

          //Get the corresponding Localzation Key based on the Transport type
          if (ContainsText(sObjectName,"Transport")){
              sInputValue = "TransportType";
              if (isSimulated){
                  sInputValue = "SimulatedTransportType";
              }
              //Read the value from CalculationData always.
              sInputValue = GetExternalData("CalculationData",sInputValue);
              switch (sInputValue){
                  case "Low":
                      sInputValue = "lowTransport";
                      break;
                  case "Low Average":
                      sInputValue = "lowAverageTransport";
                      break;              
                  case "High Average":
                      sInputValue = "highAverageTransport";
                      break;     
                  case "High":
                      sInputValue = "highTransport";
                      break; 
                  case "N/A":
                  case " ":
                      sInputValue = "naTransport";
                      if (CompareText(sObjectName,"PETSummary_TransportTypeValue")){
                          sAction = "VerifyNotExist"; //Transport Type N/A should not be displayed in PET Summary page alone.    
                      }
                      break;                                                  
              }
          }else if (ContainsText(sObjectName,"PETSummary_Simulated_Tag")){
              if (isSimulated){
                  sInputValue = "simulated";
              }else{
                  sAction = "VerifyNotExist";
              }
          }else{
              //PETList_SimulatedValue or PatientSummary_SimulatedValue
              if (isSimulated){
                  sInputValue = "yes";
              }else{
                  sInputValue = "no";
              }                     
          }
          
          if (sObjectName != null){
              //Perform the Action
              objName = BuildWebObj(g_dicColNames_obj.item(sObjectName));  //Build object from the given object name
              intStepStatus = DataEntry(sAction,objName,sInputValue,sObjectName,sStepNo);
              VerificationPoint(g_strFuncCall,sStepNo,intStepStatus,intIterCnt);
          }
          
      } 
      g_stepnum = sStepNo; 
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}


//*************************************************************************************************
// Function Name        : VerifyPETOutput
// Function Description : To verify the Calculation Output of a PET                    
// Inputs               : N/A     
// Returns              : N/A
//*************************************************************************************************
function VerifyPETOutput(){
 
  var sObjectName = null;     //Variable for storing object name  
  var sInputValue = null;
  var sAction = null;
  var sInputValue1 = null;
  var sStepNo = null;           //Variable for storing step number

  var intIniRwNo = g_intStrtRow;        //Variable for storing initial row number
  var intEndRwNo = g_intEndRow;        //Variable for storing end row number  
  var intStepStatus = 1;      //Initialize the status to Fail
  var strData = null;
  var CalculationKeys = new Array(); //Array to hold the calculation Keys
  var isSimulated = false;
  var PETType = null; 
  try{
  
      isSimulated = GetSimulationStatus();
      PETType = GetCalculationData("PETType",false);
      PETType = PETType.toUpperCase();
 
      //Mapping of the Object Name and Calculaion Key
      CalculationKeys["PETSummary_Urea0HourValue"]        = !isSimulated ? "Urea0Hour" : "SimulatedUrea0Hour";
      CalculationKeys["PETSummary_Urea1HourValue"]        = !isSimulated ? "Urea1Hour" : "SimulatedUrea1Hour";
      CalculationKeys["PETSummary_Urea2HourValue"]        = !isSimulated ? "Urea2Hour" : "SimulatedUrea2Hour";
      CalculationKeys["PETSummary_Urea4HourValue"]        = !isSimulated ? "Urea4Hour" : "SimulatedUrea4Hour";
      CalculationKeys["PETSummary_Creatinine0HourValue"]  = !isSimulated ? "Creatinine0Hour" : "SimulatedCreatinine0Hour";
      CalculationKeys["PETSummary_Creatinine1HourValue"]  = !isSimulated ? "Creatinine1Hour" : "SimulatedCreatinine1Hour";
      CalculationKeys["PETSummary_Creatinine2HourValue"]  = !isSimulated ? "Creatinine2Hour" : "SimulatedCreatinine2Hour";
      CalculationKeys["PETSummary_Creatinine4HourValue"]  = !isSimulated ? "Creatinine4Hour" : "SimulatedCreatinine4Hour";
      CalculationKeys["PETSummary_Glucose0HourValue"]     = !isSimulated ? "Glucose0Hour" : "1";
      CalculationKeys["PETSummary_Glucose1HourValue"]     = !isSimulated ? "Glucose1Hour" : "SimulatedGlucose1Hour";
      CalculationKeys["PETSummary_Glucose2HourValue"]     = !isSimulated ? "Glucose2Hour" : "SimulatedGlucose2Hour";
      CalculationKeys["PETSummary_Glucose4HourValue"]     = !isSimulated ? "Glucose4Hour" : "SimulatedGlucose4Hour";  
      CalculationKeys["PETSummary_Sodium0HourValue"]  	  = !isSimulated ? "Sodium0Hour" : "---";
      CalculationKeys["PETSummary_Sodium1HourValue"]  	  = !isSimulated ? "Sodium1Hour" : "---";
      CalculationKeys["PETSummary_Sodium2HourValue"]  	  = !isSimulated ? "Sodium2Hour" : "---";
      CalculationKeys["PETSummary_Sodium4HourValue"]  	  = !isSimulated ? "Sodium4Hour" : "---";            
      
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
          
          intStepStatus = 1;  //Initialize the status to fail for each each step.
          sObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
          sInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
          sAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
          sInputValue1 = ReadExcel(intIterCnt,c_intInputVal1Col);
          sStepNo = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
          
          //Get the corresponding Calculation Key based on the Object and Perform the Action
          sInputValue = CalculationKeys[sObjectName];
          if (ContainsText(sInputValue,"Hour")){
              sInputValue = GetCalculationData(sInputValue,true);
          }
          
          //Update the Action based on PET Type
          if (CompareText(PETType,"STANDARD")){
              if (ContainsText(sObjectName,"1Hour") || ContainsText(sObjectName,"Sodium")){
                  sAction = "VerifyNotExist";
              }
          }else if (CompareText(PETType,"MODIFIED")){
              if (isSimulated  && ContainsText(sObjectName,"Sodium")){// Sodium will not appear if it is simulated.
                  sAction = "VerifyNotExist";
              }
          }else if (CompareText(PETType,"FAST PET")){
              if (ContainsText(sObjectName,"0Hour") || ContainsText(sObjectName,"1Hour") || ContainsText(sObjectName,"2Hour") || ContainsText(sObjectName,"Sodium")){
                  sAction = "VerifyNotExist";
              }          
          }
          
          if (sObjectName != null){
              objName = BuildWebObj(g_dicColNames_obj.item(sObjectName));  //Build object from the given object name
              intStepStatus = DataEntry(sAction,objName,sInputValue,sObjectName,sStepNo);
              VerificationPoint(g_strFuncCall,sStepNo,intStepStatus,intIterCnt);
          }
          
      } 
      g_stepnum = sStepNo; 
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}

//*************************************************************************************************
// Function Name        : GetSimulationStatus
// Function Description : To check whether the current record holds data for Simulated PET or not.                    
// Inputs               : N/A     
// Returns              : true if simulated and false otherwise
//*************************************************************************************************
function GetSimulationStatus(){
    var ReturnValue = false;//standard PET
    var ConditionValue = "";
    var PETType = "";
    var Serum1TimeMin = 0;
    var Serum1TimeMax = 0;
    var Dialysate1TimeMin = 0;
    var Dialysate1TimeMax = 0;
    var Dialysate2TimeMin = 0;
    var Dialysate2TimeMax = 0;
    var Dialysate3TimeMin = 0;
    var Dialysate3TimeMax = 0;
    try{
    
        //Assign the Dialysate and Serum Time based on the PET Type.
        PETType  = GetCalculationData("PETType",false)
        PETType = Trim(PETType.toUpperCase());
        if (ContainsText(PETType,"STANDARD")){
            Serum1TimeMin = 110;
            Serum1TimeMax = 130;
            Dialysate1TimeMin = 0;
            Dialysate1TimeMax = 10;
            Dialysate2TimeMin = 50;
            Dialysate2TimeMax = 70;             
            Dialysate3TimeMin = 110;
            Dialysate3TimeMax = 130;
            Dialysate4TimeMin = 230;
            Dialysate4TimeMax = 250;            
        }else if (ContainsText(PETType,"FAST")){
            Serum1TimeMin = 230;
            Serum1TimeMax = 250;
            Dialysate1TimeMin = 0;
            Dialysate1TimeMax = 10;
            Dialysate2TimeMin = 50;
            Dialysate2TimeMax = 70;             
            Dialysate3TimeMin = 110;
            Dialysate3TimeMax = 130;
            Dialysate4TimeMin = 230;
            Dialysate4TimeMax = 250;            
        }else if (ContainsText(PETType,"MODIFIED")){
            Serum1TimeMin = 110;
            Serum1TimeMax = 130;
            Dialysate1TimeMin = 0;
            Dialysate1TimeMax = 10;
            Dialysate2TimeMin = 50;
            Dialysate2TimeMax = 70;            
            Dialysate3TimeMin = 110;
            Dialysate3TimeMax = 130;
            Dialysate4TimeMin = 230;
            Dialysate4TimeMax = 250;  
        }
        
        //Simulated Check by Four Hour Solution Concentration. This is applicable  Standard PET and Fast PET.
        if (ContainsText(PETType,"STANDARD") || ContainsText(PETType,"FAST")){
            ConditionValue = Trim(GetCalculationData("FourHourSolution",false));
            if ( !(CompareText(ConditionValue,"2.5") ||  CompareText(ConditionValue,"2.27"))){
                ReturnValue = true;
                SimulationReason = "Simulated due to Four Hour Solution: " + ConditionValue;
                Log.Message(SimulationReason);
                return ReturnValue;
            }
        }
        
        //Simulated Check by Four Hour Solution Concentration. This is applicable  Modified PET
        if (ContainsText(PETType,"MODIFIED")){
            ConditionValue = Trim(GetCalculationData("FourHourSolution",false));
            if ( CompareText(ConditionValue,"1.5")){ //1.5 alone will be simulated.
                ReturnValue = true;
                SimulationReason = "Simulated due to Four Hour Solution: " + ConditionValue;
                Log.Message(SimulationReason);
                return ReturnValue;
            }
        }        
        
        //Simulated Check by Serum #1 Time. This is applicable  Standard PET, Fast PET and modified PET.
        if (ContainsText(PETType,"STANDARD") || ContainsText(PETType,"FAST") || ContainsText(PETType,"MODIFIED")){        
          ConditionValue = Trim(GetCalculationData("Serum1Time",true));
          if ( ConditionValue < Serum1TimeMin || ConditionValue > Serum1TimeMax ){
              ReturnValue = true;
              SimulationReason = "Simulated due to Serum 1 Time: " + ConditionValue;
              Log.Message(SimulationReason);
              return ReturnValue;
          }
        }  
        
        //Simulated Check by Dialysate #1 Time. This is applicable Standard PET and modified PET.
        if (ContainsText(PETType,"STANDARD") || ContainsText(PETType,"MODIFIED")){
            ConditionValue = Trim(GetCalculationData("Dialysate1Time",true));
            if ( ConditionValue < Dialysate1TimeMin || ConditionValue > Dialysate1TimeMax ){
                ReturnValue = true;
                SimulationReason = "Simulated due to Dialysate #1 Time: " + ConditionValue;
                Log.Message(SimulationReason);
                return ReturnValue;
            }
        }
        
        //Simulated Check by Dialysate #2 Time. This is applicable Modified PET. 
        if (ContainsText(PETType,"MODIFIED")){
            ConditionValue = Trim(GetCalculationData("Dialysate2Time",true));
            if ( ConditionValue < Dialysate2TimeMin || ConditionValue > Dialysate2TimeMax ){
                ReturnValue = true;
                SimulationReason = "Simulated due to Dialysate #2 Time: " + ConditionValue;
                Log.Message(SimulationReason);
                return ReturnValue;
            }
        }        
        
        //Simulated Check by Dialysate #3 Time. This is applicable Standard PET  and modified PET.
        if (ContainsText(PETType,"STANDARD") || ContainsText(PETType,"MODIFIED")){
            ConditionValue = Trim(GetCalculationData("Dialysate3Time",true));
            if ( ConditionValue < Dialysate3TimeMin || ConditionValue > Dialysate3TimeMax ){
                ReturnValue = true;
                SimulationReason = "Simulated due to Dialysate #3 Time: " + ConditionValue;
                Log.Message(SimulationReason);
                return ReturnValue;
            }
        }
        
        //Simulated Check by Dialysate #4 Time. This is applicable  Standard PET, Fast PET and modified PET.
        if (ContainsText(PETType,"STANDARD") || ContainsText(PETType,"FAST") || ContainsText(PETType,"MODIFIED")){ 
          ConditionValue = Trim(GetCalculationData("Dialysate4Time",true));
          if ( ConditionValue < Dialysate4TimeMin || ConditionValue > Dialysate4TimeMax ){
              ReturnValue = true;
              SimulationReason = "Simulated due to Dialysate #4 Time: " + ConditionValue;
              Log.Message(SimulationReason);
              return ReturnValue;
          }
        }
        
        
        //Simulated Check by Volume Infused. This is applicable  Standard PET, Fast PET and modified PET.
        if (ContainsText(PETType,"STANDARD") || ContainsText(PETType,"FAST") || ContainsText(PETType,"MODIFIED")){ 
            ConditionValue = Trim(GetCalculationData("PediatricStatus",false));
            if (CompareText(ConditionValue,"Y")){
                //Pediatric Patient
                ConditionValue = Trim(GetCalculationData("FourHourVolumeInfused",true));
                var BSValue = Trim(GetCalculationData("BSA",true));
                if (GetAmputationStatus()){
                    BSValue = Trim(GetCalculationData("CorrectedBSA",true));//For Amputated Patient, take Corrected BSA
                }
                ConditionValue = ConditionValue / BSValue;
                if ( ConditionValue < 1045 || ConditionValue > 1155){
                    ReturnValue = true;
                    SimulationReason = "Simulated due to Volume Infused/BSA: " + ConditionValue;
                    Log.Message(SimulationReason);
                    return ReturnValue;
                }            
            }else{
                //Adult Patient
                ConditionValue = Trim(GetCalculationData("FourHourVolumeInfused",true));
                if ( ConditionValue < 1900 || ConditionValue > 2100){
                    ReturnValue = true;
                    SimulationReason = "Due to Volume Infused" + ConditionValue;
                    Log.Message(SimulationReason);
                    return ReturnValue;
                }
            }
        }
        
        
    }catch(e){
        Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    }
    return ReturnValue;
}

//*************************************************************************************************
// Function Name        : VerifyPhysicianNameSort
// Function Description : To check if the Physician name is sorted.                    
// Inputs               : N/A     
// Returns              : N/A  
//*************************************************************************************************
function VerifyPhysicianNameSort(){
  var sObjectName = null;     //Variable for storing object name  
  var sStepNo = null;           //Variable for storing step number
  var intIniRwNo = g_intStrtRow;        //Variable for storing initial row number
  var intEndRwNo = g_intEndRow;        //Variable for storing end row number  
  var intStepStatus = 1;      //Initialize the status to Fail
  var DropdownAllValue = [];
  try{
      
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){

          sObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol    
          sStepNo = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
            
          //Initialize the status to fail for each each step.
          intStepStatus = 1;  //Fail
          ExpectedMessage = "Physcian name in "+sObjectName+" shall be sorted.";
          ActualMessage = "Physcian name in "+sObjectName+" is not sorted.";          
      
          if (sObjectName != null){
              objName = BuildWebObj(g_dicColNames_obj.item(sObjectName));  //Build object from the given object name
          }
          objName = eval(objName);
          DropdownCount = objName.wItemCount;
          //Starting from 2, to ignore the first value which is All Physician
          for (DropdownLoop = 2; DropdownLoop<=DropdownCount; DropdownLoop++) {
            DropdownValue = objName.wItem(DropdownLoop-1);
            DropdownSplitValue = DropdownValue.split(" ");
            if (DropdownSplitValue.length>2){
                DropdownValue = Trim(DropdownSplitValue[1]) + " " + Trim(DropdownSplitValue[2]);                          
            }
            DropdownAllValue[DropdownLoop-2] = Trim(DropdownValue);
          }
          if (IsSorted(DropdownAllValue,"Ascending", "String")) {
            intStepStatus = 0;
            ActualMessage = "Physcian name in "+sObjectName+" is sorted.";
          }
      } 
      g_stepnum = sStepNo; 
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }finally{
      VerificationPoint(g_strFuncCall,sStepNo,intStepStatus,intIterCnt);
  }
}

//*************************************************************************************************
// Function Name        : VerifyPDDataSorting
// Function Description : To verify the Date sorting of Latest 24 Hour and PET Columns
// Inputs               : None      
// Returns              : None
//**************************************************************************************************
function VerifyPDDataSorting(){
  
  var sObjectName = null;     //Variable for storing object name  
  var sStepNo = null;           //Variable for storing step number
  var intIniRwNo = g_intStrtRow;        //Variable for storing initial row number
  var intEndRwNo = g_intEndRow;        //Variable for storing end row number  
  var intStepStatus = 1;      //Initialize the status to Fail
  var sInputValue;
  var Incomplete = "";
  var arrComplete = [];
  var AddPET = "";
  var Add24Hour = "";
  try{
    
    sObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
    sStepNo = ReadExcel(intIniRwNo,c_intStepCol);  //Reads the step from c_intStepCol
    if (sObjectName != null){
      
        sInputValue = ReadExcel(intIniRwNo,c_intInputValCol);  //Reads the input value from c_intInputValCol
        Incomplete = GetExternalData("LocalizationData","Incomplete");
        AddPET = GetExternalData("LocalizationData","addPETLink");
        Add24Hour = GetExternalData("LocalizationData","add24HourLink"); 
            
        objName = GetChildObjectsByXPath(g_dicColNames_obj.item(sObjectName));
        for (TableRows = 0; TableRows<objName.length; TableRows++){
            ScrollIntoView(objName[TableRows]);
            sObjectText = GetText(objName[TableRows]);
            if (ContainsText(sObjectText,Add24Hour) || ContainsText(sObjectText,AddPET)){
              sObjectText = "02 Jan 2025"; //Highest Value for - Add 24 Hour
            }else if(ContainsText(sObjectText,Incomplete)){
              sObjectText = "01 Jan 2025";// One value lesser than the highest values for InComplete PET's
            }
            arrComplete[TableRows] = sObjectText;
        }
    }
    //Switch the sorting, as the latest date would be first in Ascending.
    switch (trim(sInputValue.toUpperCase())){
      case "ASCENDING":
        sInputValue = "DESCENDING";
        break;
      case "DESCENDING":
        sInputValue = "ASCENDING";
        break;
    }
      
    if (IsSorted(arrComplete,sInputValue,"date")){
      intStepStatus = 0;
    }
  }catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }finally{
    VerificationPoint(g_strFuncCall,sStepNo,intStepStatus,intIterCnt);
  }
}
  
//*************************************************************************************************
// Function Name        : ChangePDASettings
// Function Description : To change the settings of Adequest.               
// Inputs               : None      
// Returns              : None
//**************************************************************************************************
function ChangePDASettings(){
  try{
    var intIniRwNo;        //Variable for storing initial row number
    var intEndRwNo;        //Variable for storing end row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strInputValue;     //Variable for storing input value  
    var strStep = "";           //Variable for storing step number
    var intStepStatus = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
    intEndRwNo = g_intEndRow;  //Searches the g_intEndRow in testdata sheet and reads the row number of a keyword
    CalculationDifferenceTolerance = 0;
    var SettingsObject = null;
    
    for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      //Read object name from excel 
      strObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
      intStepStatus = 1;
      
      //Update CalculationDifferenceTolerance % based on the settings
      SettingsObject = strObjectName.toUpperCase();
      if (ContainsText(SettingsObject,"MMOL") || ContainsText(SettingsObject,"INCHES") || ContainsText(SettingsObject,"POUNDS")){
          CalculationDifferenceTolerance = 1; //Change Tolerance to 1% for settings other than the default ones.    
      }
      
      if (strObjectName != null){
        //Build object
        objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name
        strInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
        g_OtherInputValue = ReadExcel(intIterCnt,c_intInputVal1Col); //Reads the other input value from c_intInputVal1Col
        strStep = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
        strAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
       }
        intStepStatus = DataEntry(strAction,objName,strInputValue,strObjectName,strStep);
        VerificationPoint(g_strFuncCall,strStep,intStepStatus,intIterCnt);
      }
    g_stepnum = strStep;
  }catch(e){
   Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }
}
  


//*************************************************************************************************
// Function Name        : CheckSimulationStatus
// Function Description : To check the simulation status of a record                 
// Inputs               : None  
// Returns              : None  
//**************************************************************************************************

function CheckSimulationStatus(){

    try{

        var intIniRwNo;       //Variable for storing initial row number
        var strInputValue;    //Variable for storing input value
        
        intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword 
        strInputValue = ReadExcel(intIniRwNo,c_intInputValCol);
        var strStep = ReadExcel(intIniRwNo, c_intStepCol);  //Reads the step from c_intStepCol
        var strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
        var StepStatus = "Fail";
        var ExpectedSimulation = GetCalculationData("SimulationStatus",false);
        var ActualSimulation = "0";
        var SimulationReasonText = "";
        CurrentRecordNU = "RecordNU " + GetCalculationData("RecordNU",false) + " is ";
        if (GetSimulationStatus()){
            ActualSimulation = "1";
            SimulationReasonText = SimulationReason;
        }
        
        var ExpectedResult = CurrentRecordNU + "Simulation Status shall be " + ExpectedSimulation;
        var ActualResult = "Simulation Status is " + ActualSimulation + ". " + SimulationReasonText;
        if (ExpectedSimulation == ActualSimulation){
            StepStatus = "Pass"
            Log.Message(strStep+" - "+ActualResult);
        }else{
            Log.Error(strStep+" - "+ActualResult);
        }
        
    }catch(e){
        Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    }finally{
        fnInsertResult("CheckSimulationStatus",strStep,ExpectedResult,ActualResult,StepStatus, strStepType);
        g_stepnum = strStep;
    }
 
 } 
 
 
 
//*************************************************************************************************
// Function Name        : UpdateLastFillFields
// Function Description : To modify the Last Fill fields in the Regimen page based on the values from Calculation File.                    
// Inputs               : N/A     
// Returns              : N/A
//*************************************************************************************************
function UpdateLastFillFields(){
 
  var sObjectName = null;     //Variable for storing object name  
  var sInputValue = null;
  var sAction = null;
  var sInputValue1 = null;
  var sStepNo = null;           //Variable for storing step number

  var intIniRwNo = g_intStrtRow;        //Variable for storing initial row number
  var intEndRwNo = g_intEndRow;        //Variable for storing end row number  
  var intStepStatus = 1;      //Initialize the status to Fail
  var strData = null;
  var LastFillStatus = false;
     
  try{
      if (GetRegimenData("DayExchangeCount") > 0){
          LastFillStatus = true;    
      }
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      
          //Initialize the status to fail for each each step.
          intStepStatus = 1;  //Fail
      
          sObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
          sInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
          sAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
          sInputValue1 = ReadExcel(intIterCnt,c_intInputVal1Col);
          sStepNo = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
          
          if (sObjectName != null){
              objName = BuildWebObj(g_dicColNames_obj.item(sObjectName));  //Build object from the given object name
              if(CompareText(sObjectName,"Regimen_LastFill_CheckBox")){
                  //Select Yes, if the record has Last Fill
                  if (LastFillStatus){
                      sAction = "Checked";
                  }else{
                      sAction = "Unchecked";
                  }
              }else if(ContainsText(sObjectName,"Solution")){
                  sInputValue = GetSolutionValue(sInputValue,sObjectName);
              }else if(ContainsText(sObjectName,"StartTime")){
                  sInputValue = GetRegimenData(sInputValue);
                  sInputValue = ConvertTime(sInputValue, "MINUTE")
                  sInputValue = GetDwellEndTime(sInputValue);
              }
    
              //Perform the Action
              intStepStatus = DataEntry(sAction,objName,sInputValue,sObjectName,sStepNo);
              VerificationPoint(g_strFuncCall,sStepNo,intStepStatus,intIterCnt);
              
              //If Last Fill exchange is not present, then process only the first row to set the checkbox box status.
              if (!LastFillStatus){
                  intIterCnt = intEndRwNo +1;
              }
          }
          
      } 
      g_stepnum = sStepNo; 
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}

//*************************************************************************************************
// Function Name        : UpdateDryFields
// Function Description : To modify the Dry fields in the Regimen page based on the values from Calculation File.                    
// Inputs               : N/A     
// Returns              : N/A
//*************************************************************************************************
function UpdateDryFields(){
 
  var sObjectName = null;     //Variable for storing object name  
  var sInputValue = null;
  var sAction = null;
  var sInputValue1 = null;
  var sStepNo = null;           //Variable for storing step number

  var intIniRwNo = g_intStrtRow;        //Variable for storing initial row number
  var intEndRwNo = g_intEndRow;        //Variable for storing end row number  
  var intStepStatus = 1;      //Initialize the status to Fail
  var strData = null;
  var isDryTimeAvaiable = false;
  try{
  
      oRDRecordSet.MoveFirst(); //Move the record to first, so Dry Time and Night Exchange can be processed.
      if (GetCalculationData("RegimenDryTime", true) > 0 ){
         isDryTimeAvaiable = true;
      }
      
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      
          //Initialize the status to fail for each each step.
          intStepStatus = 1;  //Fail
      
          sObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
          sInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
          sAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
          sInputValue1 = ReadExcel(intIterCnt,c_intInputVal1Col);
          sStepNo = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
          
          if (sObjectName != null){
              objName = BuildWebObj(g_dicColNames_obj.item(sObjectName));  //Build object from the given object name
              if (!isDryTimeAvaiable){
                  sAction = "VerifyExist";    
              }else if(ContainsText(sObjectName,"StartTime")){
                  sInputValue = GetCalculationData(sInputValue);
                  sInputValue = GetDwellEndTime(sInputValue);
              }
    
              //Perform the Action
              intStepStatus = DataEntry(sAction,objName,sInputValue,sObjectName,sStepNo);
              VerificationPoint(g_strFuncCall,sStepNo,intStepStatus,intIterCnt);
              
              //If Dry Time exchange is not present, then process only the first row.
              if (!isDryTimeAvaiable){
                  intIterCnt = intEndRwNo +1;
              }
          }
          
      } 
      g_stepnum = sStepNo; 
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}

//*************************************************************************************************
// Function Name        : UpdateNightExchangeFields
// Function Description : To update the Night Exchange Fields in the Regimen page based on the values from Calculation File.                    
// Inputs               : N/A     
// Returns              : N/A
//*************************************************************************************************
function UpdateNightExchangeFields(){
 
  var sObjectName = null;     //Variable for storing object name  
  var sInputValue = null;
  var sAction = null;
  var sInputValue1 = null;
  var sStepNo = null;           //Variable for storing step number

  var intIniRwNo = g_intStrtRow;        //Variable for storing initial row number
  var intEndRwNo = g_intEndRow;        //Variable for storing end row number  
  var intStepStatus = 1;      //Initialize the status to Fail
  var strData = null;
  var TotalNight = null;
  var NightExchangeCount = null;
  var DwellTime = null;     
  try{

      TotalNight = GetRegimenData("TotalNightTheraphy");
      NightExchangeCount = GetRegimenData("NightExchangeCount");
      DwellTime = (TotalNight / NightExchangeCount); //This would be in hours
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      
          //Initialize the status to fail for each each step.
          intStepStatus = 1;  //Fail
      
          sObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
          sInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
          sAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
          sInputValue1 = ReadExcel(intIterCnt,c_intInputVal1Col);
          sStepNo = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
          
          if (sObjectName != null){
              objName = BuildWebObj(g_dicColNames_obj.item(sObjectName));  //Build object from the given object name
              if(ContainsText(sObjectName,"DwellTime")){
                  sInputValue = ConvertTime(DwellTime,"MINUTE");
              }else if(ContainsText(sObjectName,"Solution")){
                  sInputValue = GetSolutionValue(sInputValue,sObjectName);
              }else if(ContainsText(sObjectName,"Concentration")){
                  sInputValue = "NightExchangeSolution";
                  if (CompareText(GetCalculationData("RegimenType", false),"TIDAL")){
                      sInputValue = "NightExchangeSolutionTidal";
                  }
              }else if(ContainsText(sObjectName,"StartTime")){
                  sInputValue = ConvertTime(DwellTime,"MINUTE");
                  sInputValue = GetDwellEndTime(sInputValue);
              }
    
              //Perform the Action
              intStepStatus = DataEntry(sAction,objName,sInputValue,sObjectName,sStepNo);
              VerificationPoint(g_strFuncCall,sStepNo,intStepStatus,intIterCnt);
          }
      } 
      
      g_stepnum = sStepNo; 
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}

//*************************************************************************************************
// Function Name        : UpdateManualDayExchangeFields
// Function Description : To update the Manual Day Manual Fields in the CAPD Regimen page based on the values from Calculation File.                    
// Inputs               : N/A     
// Returns              : N/A
//*************************************************************************************************
function UpdateManualDayExchangeFields(){
 
  var sObjectName = null;     //Variable for storing object name  
  var sInputValue = null;
  var sAction = null;
  var sInputValue1 = null;
  var sStepNo = null;           //Variable for storing step number

  var intIniRwNo = g_intStrtRow;        //Variable for storing initial row number
  var intEndRwNo = g_intEndRow;        //Variable for storing end row number  
  var intStepStatus = 1;      //Initialize the status to Fail
  var strData = null;
  var TotalDayExchange = 0;
  var ExpectedRecordCount = 0; 
  var RegimenType = null;
  var CurrentDayExchangeIndex = 0;
  var TotalDayDuration = 0;
  try{
  
      TotalDayExchange = GetRegimenData("DayExchangeCount");
      ExpectedRecordCount = oRDRecordSet.RecordCount;
      RegimenType = GetCalculationData("RegimenType", false);
      if (CompareText(RegimenType,"CAPD")){
          //For CAPD, Move to the first day exchange
          oRDRecordSet.MoveFirst();
      if (TotalDayExchange != (ExpectedRecordCount - 1)){
          Log.Error("Total Day Exchange and the total record count did not match.")
      }    
      }else{
          //For APD and TIDAL, Move to the second day exchange since first day exchange was processed for Last Fill
          oRDRecordSet.MoveFirst();   
          oRDRecordSet.MoveNext(); 
          if (TotalDayExchange > 0){
              if (TotalDayExchange != ExpectedRecordCount){
                  Log.Error("Total Day Exchange and the total record count did not match.")
              }           
              TotalDayExchange = TotalDayExchange - 1;//Decrement 1, since first day exchange was processed for Last Fill
          }
      }      
      
      
      
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      
          //Initialize the status to fail for each each step.
          intStepStatus = 1;  //Fail
      
          sObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
          sInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
          sAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
          sInputValue1 = ReadExcel(intIterCnt,c_intInputVal1Col);
          sStepNo = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
          
          if (sObjectName != null){
              objName = BuildWebObj(g_dicColNames_obj.item(sObjectName));  //Build object from the given object name
              if(CompareText(sObjectName,"Regimen_DayExchangeCount_Textbox")){
                  sInputValue = TotalDayExchange;  
              }else if(ContainsText(sObjectName,"Solution")){
                  sInputValue = GetSolutionValue(sInputValue,sObjectName);
              }else if(ContainsText(sObjectName,"StartTime")){
                  sInputValue = GetRegimenData(sInputValue);
                  if (CompareText(RegimenType,"APD") || CompareText(RegimenType,"TIDAL")){
                      sInputValue = ConvertTime(sInputValue,"MINUTE");
                  }
                  sInputValue = GetDwellEndTime(sInputValue);
              }else if(ContainsText(sObjectName,"Volume")){
                  sInputValue = GetRegimenData(sInputValue);
              }
    
              //Perform the Action
              intStepStatus = DataEntry(sAction,objName,sInputValue,sObjectName,sStepNo);
              VerificationPoint(g_strFuncCall,sStepNo,intStepStatus,intIterCnt);
              if(ContainsText(sObjectName,"Volume_Textbox")){
                  CurrentDayExchangeIndex++; //Volume is the last field to indicate that a Exchange is processed
                  if (CurrentDayExchangeIndex < TotalDayExchange){
                      oRDRecordSet.MoveNext(); //Move to the next recordset once the last field (volume) in one day exchange is processed.
                  }else{
                      intIterCnt = intEndRwNo +1;//Although still rows are present, parse only the rows which needs to be entered for this record.
                  }
              }
              
              //If Day exchange is not present, then process only the first row to set the Exchange count.
              if (TotalDayExchange == 0){
                  intIterCnt = intEndRwNo +1;  
              }  
              
          }
      } 
//      //Set the LastDwellDuration as the Total Duration of Day Exchange. So the Start Time of Dry would be correct.
//      if (TotalDayDuration > 0){
//          LastDwellDuration = TotalDayDuration;
//      }
      g_stepnum = sStepNo; 
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}

//*************************************************************************************************
// Function Name        : UpdateAutomatedDayExchangeFields
// Function Description : To update the Automated Day Exchange Fields in the Regimen page based on the values from Calculation File.                    
// Inputs               : N/A     
// Returns              : N/A
//*************************************************************************************************
function UpdateAutomatedDayExchangeFields(){
 
  var sObjectName = null;     //Variable for storing object name  
  var sInputValue = null;
  var sAction = null;
  var sInputValue1 = null;
  var sStepNo = null;           //Variable for storing step number

  var intIniRwNo = g_intStrtRow;        //Variable for storing initial row number
  var intEndRwNo = g_intEndRow;        //Variable for storing end row number  
  var intStepStatus = 1;      //Initialize the status to Fail
  var strData = null;
  var TotalDayExchange = 0;
  var IsRecordSetProcessed = false;
  var TotalDwellTime = 0;
  var TotalConcentration = 0;
  var TotalVolume = 0;
  
  try{
  
      var TotalDayExchange = GetRegimenData("DayExchangeCount") - 1; //Since One Exchange was used for Last Fill.
      if (TotalDayExchange <= 0){
          TotalDayExchange = 0;
      }  
      if (TotalDayExchange != oRDRecordSet.RecordCount -1){
          Log.Error("Total Day Exchange and the total record count did not match.")
      }
      oRDRecordSet.MoveNext();  //Move from the first day change (Last Fill) to the next row. 
  
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      
          //Initialize the status to fail for each each step.
          intStepStatus = 1;  //Fail
      
          sObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
          sInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
          sAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
          sInputValue1 = ReadExcel(intIterCnt,c_intInputVal1Col);
          sStepNo = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
          
          if (sObjectName != null){
              objName = BuildWebObj(g_dicColNames_obj.item(sObjectName));  //Build object from the given object name
                  if(CompareText(sObjectName,"Regimen_DayExchangeCount_Textbox")){
                      sInputValue = TotalDayExchange;  
                  }else if(ContainsText(sObjectName,"Concentration")){
                      sInputValue = (TotalConcentration / TotalDayExchange);
                  }else if(ContainsText(sObjectName,"DwellTime")){
                      sInputValue = (TotalDwellTime / TotalDayExchange);
                      sInputValue = ConvertTime(sInputValue, "MINUTE");
                  }else if(ContainsText(sObjectName,"Solution")){
                      sInputValue = GetSolutionValue(sInputValue,sObjectName);
                  }else if(ContainsText(sObjectName,"Volume")){
                      sInputValue = (TotalVolume / TotalDayExchange);
                  }else if(ContainsText(sObjectName,"StartTime")){
                      sInputValue = TotalDwellTime;
                      sInputValue = ConvertTime(sInputValue, "MINUTE");
                      sInputValue = GetDwellEndTime(sInputValue);
                  }
    
              //Perform the Action
              intStepStatus = DataEntry(sAction,objName,sInputValue,sObjectName,sStepNo);
              VerificationPoint(g_strFuncCall,sStepNo,intStepStatus,intIterCnt);
              
              //Read the values from the Regimen record set to be processed
              if (!IsRecordSetProcessed){
                  for (var iDayExchange=1; iDayExchange<=TotalDayExchange; iDayExchange++){
                      TotalDwellTime = TotalDwellTime + Number(GetRegimenData("DayExchangeDwellTime"));
                      TotalVolume = TotalVolume + Number(GetRegimenData("DayVolumeInfused"));
                      TotalConcentration = TotalConcentration + Number(GetRegimenData("DayExchangeSolution"));
                      oRDRecordSet.MoveNext();
                  }
                  IsRecordSetProcessed = true;
              }              
              
              //If Day exchange is not present, then process only the first row to set the Exchange count.
              if (TotalDayExchange == 0){
                  intIterCnt = intEndRwNo +1;  
              } 
          }
      } 
      
      g_stepnum = sStepNo; 
     
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}

//*************************************************************************************************
// Function Name        : GetSolutionValue
// Function Description : To get the appropriate solution value based on the Company Region                    
// Inputs               : SolutionInput - Solution Concentration number
//						  SolutionObjectName - Name of the Solution Object
// Returns              : Solution Output
//*************************************************************************************************
function GetSolutionValue(SolutionInput, SolutionObjectName ){
    var ReturnValue = "";
    var SolutionObjectType = "";
    var SugarName = "";
    var Parameters = "";
    var CurrentParameter = "";
    var ParameterName = "";
    var ParameterValue = "";
    try{
    
          //Parse the InputsValues
          SolutionInput = CleanUpText(SolutionInput);
          if (SolutionInput.length == 0){
            SolutionInput = "NA";//if the concentration is not present
          }  
          if (aqString.GetChar(SolutionInput,SolutionInput.length-1) == ";") {
            SolutionInput = SolutionInput.substring(0,SolutionInput.length-1)
          }
          if (ContainsText(SolutionInput,";") || ContainsText(SolutionInput,"=")){
            Parameters = SolutionInput.split(";");
            Parameters[0] = Trim(Parameters[0]);
            CurrentParameter = Parameters[0];
            //Convert the Parameter values if it needs to come from ExternalData
            if (ContainsText(CurrentParameter,"=")){
              CurrentParameter = CurrentParameter.split("=");
              ParameterName = Trim(CurrentParameter[0]);
              ParameterValue = Trim(CurrentParameter[1]);
              Parameters[0] = GetExternalData(ParameterName, ParameterValue)
            }
            SolutionInput = Parameters[0];
           }
    
      
        SolutionObjectType = BuildWebObj(g_dicColNames_obj.item(SolutionObjectName));
        SolutionObjectType = eval(SolutionObjectType);
        if (IsExists(SolutionObjectType)){
            SolutionObjectType = SolutionObjectType.ObjectType;
        }else{
            SolutionObjectType = "Select"; //If there is a error, consider it is a drop down.
        }
    
          var CompanyRegion = GetTestData("CompanyRegion");
          if (CompareText(CompanyRegion,"UCAN")){
              SugarName = "dextrose";//UCAN
          }else if (CompareText(CompanyRegion,"EMEA")){
            if (!CompareText(SolutionInput,"NA")){
              SolutionInput = GetSolutionInfo(SolutionInput,"DextroseConcentration");
            }
              SugarName = "glucose";//EMEA
          }else{
              SugarName = "unknown";   
          }
          
          if (CompareText(SolutionInput,"7.5")){
              SugarName = "icodextrin"; //If the concentration is 7.5, then sugarname is icodextrin
          }
              
        SolutionInput = Trim(SolutionInput);
        if (!CompareText(SolutionObjectType,"Select") && ContainsText(SolutionObjectName,"Regimen")){
            ReturnValue = SolutionInput;
            return ReturnValue; //If it is a textbox object, then return the Solution input itself.
        }else if (CompareText(SolutionInput,"NA")){
            return GetExternalData("LocalizationData",SugarName);//If it is a drop down and if the solution input is NA
        }        
        
        ReturnValue = GetExternalData("LocalizationData",SugarName) + " " + SolutionInput + GetExternalData("LocalizationData","percentage");
          
    }catch(e){
        Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    }
    return ReturnValue;    
}          


//*************************************************************************************************
// Function Name        : GetSolutionInfo
// Function Description : To get the Solution Informatio based on the concentration               
// Inputs               : Solution Concentration      
// Returns              : Requested Solution Information
//**************************************************************************************************
function GetSolutionInfo(SolutionConcentration, SolutionInfoType){
    var ReturnValue = null;
    var RegimenStringKey = null;
    var DextroseConcentration = null;
    try{
        SolutionConcentration = Trim(SolutionConcentration);
        SolutionInfoType = Trim(SolutionInfoType).toUpperCase();
        switch (SolutionConcentration){
            case "1.5":
            case "1.36":
                RegimenStringKey = "glucoseOnePointFivePercentValue";
                DextroseConcentration = "1.36";
                break;
            case "2.5":
            case "2.27":
                RegimenStringKey = "glucoseTwoPointFivePercentValue";
                DextroseConcentration = "2.27";
                break;              
            case "4.25":
            case "3.86":
                RegimenStringKey = "glucoseFourPointTwoFivePercentValue";
                DextroseConcentration = "3.86";
                break;   
            case "7.5":
                RegimenStringKey = "icodextrin";
                DextroseConcentration = "7.5";
                break;                          
        }
        
        if (CompareText(SolutionInfoType,"DEXTROSECONCENTRATION")){
            ReturnValue = DextroseConcentration;
        }else{
            ReturnValue = RegimenStringKey;
        }  
            
    }catch(e){
        Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    }
    return ReturnValue;    
}




//*************************************************************************************************
// Function Name        : GetDwellEndTime
// Function Description : To compute the End Time based on Dwell Time.               
// Inputs               : Duration of Dwell Time
// Returns              : Dwell End time
//**************************************************************************************************
function GetDwellEndTime(DwellDuration){
    
    var DwellStartTime = 0;
    var DwellEndTime = 0;
    var ReturnValue = "01:00 AM";
    try{
        
        //If the Last Dwell Field and Time is blank, then it is Last Fill so enter 00:00 AM
        if (CompareText(LastExchangeStartTime,"")){
            LastExchangeStartTime = ReturnValue;
            LastDwellDuration = DwellDuration;
            return ReturnValue;
        }
        
        //Get the Stat Time from the last Start Time set
        DwellStartTime = LastExchangeStartTime;
        
        StartPosition = aqConvert.StrToDateTime(DwellStartTime);
        EndPosition = aqDateTime.AddMinutes(StartPosition,LastDwellDuration);
        DwellEndTime = aqConvert.DateTimeToFormatStr(EndPosition,"%I:%M %p");
        ReturnValue = DwellEndTime;
        LastExchangeStartTime = ReturnValue;
        LastDwellDuration = DwellDuration;
        
        return ReturnValue;
    }catch(e){
        Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    }        
}


//*************************************************************************************************
// Function Name        : VerifyLastFillInputs
// Function Description : To verify the Last Fill fields in the Regimen Summary based on the values from Calculation File.                    
// Inputs               : N/A     
// Returns              : N/A
//*************************************************************************************************
function VerifyLastFillInputs(){
 
  var sObjectName = null;     //Variable for storing object name  
  var sInputValue = null;
  var sAction = null;
  var sInputValue1 = null;
  var sStepNo = null;           //Variable for storing step number

  var intIniRwNo = g_intStrtRow;        //Variable for storing initial row number
  var intEndRwNo = g_intEndRow;        //Variable for storing end row number  
  var intStepStatus = 1;      //Initialize the status to Fail
  var strData = null;
  var LastFillStatus = false;
     
  try{
      if (GetRegimenData("DayExchangeCount") > 0){
          LastFillStatus = true;    
      }
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      
          //Initialize the status to fail for each each step.
          intStepStatus = 1;  //Fail
      
          sObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
          sInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
          sAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
          sInputValue1 = ReadExcel(intIterCnt,c_intInputVal1Col);
          sStepNo = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
          
          if (sObjectName != null){
              objName = BuildWebObj(g_dicColNames_obj.item(sObjectName));  //Build object from the given object name
              if (!LastFillStatus){
                  sAction = "VerifyNotExist";
              }else if(ContainsText(sObjectName,"Cycle")){ 
                  sInputValue = "1";
              }else if(ContainsText(sObjectName,"Solution")){
                  sInputValue = GetRegimenData(sInputValue);
              }else if(ContainsText(sObjectName,"Volume")){
                  sInputValue = GetRegimenData(sInputValue);
                  sInputValue = ConvertVolume(sInputValue,"LITRE");
              }else if(ContainsText(sObjectName,"Time")){
                  sInputValue = GetRegimenData(sInputValue);
                  sInputValue = ConvertTime(sInputValue,"MINUTE");
              }
    
              //Perform the Action
              intStepStatus = DataEntry(sAction,objName,sInputValue,sObjectName,sStepNo);
              VerificationPoint(g_strFuncCall,sStepNo,intStepStatus,intIterCnt);
          }
          
      } 
      g_stepnum = sStepNo; 
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}


//*************************************************************************************************
// Function Name        : VerifyManualDayInputs
// Function Description : To verify the Manual Day Inputs in the Regimen Summary based on the values from Calculation File.                    
// Inputs               : N/A     
// Returns              : N/A
//*************************************************************************************************
function VerifyManualDayInputs(){
 
  var sObjectName = null;     //Variable for storing object name  
  var sInputValue = null;
  var sAction = null;
  var sInputValue1 = null;
  var sStepNo = null;           //Variable for storing step number

  var intIniRwNo = g_intStrtRow;        //Variable for storing initial row number
  var intEndRwNo = g_intEndRow;        //Variable for storing end row number  
  var intStepStatus = 1;      //Initialize the status to Fail
  var strData = null;
  var TotalDayExchange = 0;
  var iDayExchange = 1;
  var Solution = new Array();
  var Volume = new Array();
  var DwellTime = new Array();
  var CycleTime = new Array();
  var TotalDayExchange = 0;
  var ExpectedRecordCount = 0; 
  var RegimenType = null;
  
  try{
  
      TotalDayExchange = GetRegimenData("DayExchangeCount");
      ExpectedRecordCount = oRDRecordSet.RecordCount;
      RegimenType = GetCalculationData("RegimenType", false);

      if (CompareText(RegimenType,"CAPD")){
          //For CAPD, Move to the first day exchange
          oRDRecordSet.MoveFirst();
      }else{
          //For APD and TIDAL, Move to the second day exchange since first day exchange was processed for Last Fill
          oRDRecordSet.MoveFirst();   
          oRDRecordSet.MoveNext(); 
          if (TotalDayExchange > 0){
              TotalDayExchange = TotalDayExchange - 1;//Decrement 1, since first day exchange was processed for Last Fill
          }             
      }

      //Prepare to loop the record set and assign it to the arrays
      Solution[0] = "---";
      Volume[0] = 0;
      DwellTime[0] = 0;
      CycleTime[0] = TotalDayExchange;
      
      for (iDayExchange=1; iDayExchange<=TotalDayExchange; iDayExchange++){
          //Take the values from RecordSet to an Array
          Solution[iDayExchange] = Number(GetRegimenData("DayExchangeSolution"));
          Volume[iDayExchange] = Number(GetRegimenData("DayVolumeInfused"));
          DwellTime[iDayExchange] = Number(GetRegimenData("DayExchangeDwellTime"));
          CycleTime[iDayExchange] = iDayExchange + 1; //Since for APD, one record will be processed for Last Fill
          //Update the first element, with the sum of all other elements.
          Volume[0] = Number(Volume[0]) + Number(Volume[iDayExchange]);
          DwellTime[0] = Number(DwellTime[0]) + Number(DwellTime[iDayExchange]);
          if (iDayExchange < TotalDayExchange){
              oRDRecordSet.MoveNext();
          }
      }
      iDayExchange = 0;
      
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      
          //Initialize the status to fail for each each step.
          intStepStatus = 1;  //Fail
      
          sObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
          sInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
          sAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
          sInputValue1 = ReadExcel(intIterCnt,c_intInputVal1Col);
          sStepNo = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
          
          if (sObjectName != null){
              objName = BuildWebObj(g_dicColNames_obj.item(sObjectName));  //Build object from the given object name
              if (TotalDayExchange ==  0 || TotalDayExchange < iDayExchange){
                  sAction = "VerifyNotExist"; 
              }else if(ContainsText(sObjectName,"Solution")){
                  sInputValue = Solution[iDayExchange]; 
              }else if(ContainsText(sObjectName,"Volume")){
                  sInputValue = Volume[iDayExchange]; 
                  sInputValue = ConvertVolume(sInputValue,"LITRE");
              }else if(ContainsText(sObjectName,"Time")){
                  sInputValue = DwellTime[iDayExchange];
                  if (CompareText(RegimenType,"APD") || CompareText(RegimenType,"TIDAL")){
                      sInputValue = ConvertTime(sInputValue,"MINUTE");
                  }
                  iDayExchange++; //Time is the last column in a exchange, so increment it for the next loop
              }else if(ContainsText(sObjectName,"Cycle")){
                  if (iDayExchange == 0){
                      sInputValue = CycleTime[iDayExchange]; //Cycle
                  }
              } 
    
              //Perform the Action
              intStepStatus = DataEntry(sAction,objName,sInputValue,sObjectName,sStepNo);
              VerificationPoint(g_strFuncCall,sStepNo,intStepStatus,intIterCnt);
              
          }
      } 
      
      g_stepnum = sStepNo; 
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}

//*************************************************************************************************
// Function Name        : VerifyAutomatedDayInputs
// Function Description : To verify the Automated Day Inputs in the Regimen Summary based on the values from Calculation File.                    
// Inputs               : N/A     
// Returns              : N/A
//*************************************************************************************************
function VerifyAutomatedDayInputs(){
 
  var sObjectName = null;     //Variable for storing object name  
  var sInputValue = null;
  var sAction = null;
  var sInputValue1 = null;
  var sStepNo = null;           //Variable for storing step number

  var intIniRwNo = g_intStrtRow;        //Variable for storing initial row number
  var intEndRwNo = g_intEndRow;        //Variable for storing end row number  
  var intStepStatus = 1;      //Initialize the status to Fail
  var strData = null;
  var TotalDayExchange = 0;
  var iDayExchange = 1;
  var Solution = new Array();
  var Volume = new Array();
  var DwellTime = new Array();
  var CycleTime = new Array();
  try{
  
      var TotalDayExchange = GetRegimenData("DayExchangeCount") - 1; //Since One Exchange was used for Last Fill.
      if (TotalDayExchange <= 0){
          TotalDayExchange = 0;
      }
     
      //Prepare to loop the record set and assign it to the arrays
      oRDRecordSet.MoveNext();  //Move from the first day change (Last Fill) to the next row.
      Solution[0] = 0;
      Volume[0] = 0;
      DwellTime[0] = 0;
      CycleTime[0] = TotalDayExchange;
      
      for (iDayExchange=1; iDayExchange<=TotalDayExchange; iDayExchange++){
          //Take the values from RecordSet to an Array
          Solution[iDayExchange] = Number(GetRegimenData("DayExchangeSolution"));
          Volume[iDayExchange] = Number(GetRegimenData("DayVolumeInfused"));
          DwellTime[iDayExchange] = Number(GetRegimenData("DayExchangeDwellTime"));
          CycleTime[iDayExchange] = iDayExchange + 1; //Since one is already last fill.
          //Update the first element, with the sum of all other elements.
          Solution[0] = Number(Solution[0]) + Number(Solution[iDayExchange]);
          Volume[0] = Number(Volume[0]) + Number(Volume[iDayExchange]);
          DwellTime[0] = Number(DwellTime[0]) + Number(DwellTime[iDayExchange]);
          oRDRecordSet.MoveNext();
      }
      iDayExchange = 0;
      
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      
          //Initialize the status to fail for each each step.
          intStepStatus = 1;  //Fail
      
          sObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
          sInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
          sAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
          sInputValue1 = ReadExcel(intIterCnt,c_intInputVal1Col);
          sStepNo = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
          
          if (sObjectName != null){
              objName = BuildWebObj(g_dicColNames_obj.item(sObjectName));  //Build object from the given object name
              if (TotalDayExchange ==  0 || iDayExchange > 0){ //Since Automated Day Exchange will have only one row
                  sAction = "VerifyNotExist"; 
              }else if(ContainsText(sObjectName,"Solution")){
                  sInputValue = RoundOff(Solution[0] / CycleTime[0],2); 
              }else if(ContainsText(sObjectName,"Volume")){
                  sInputValue = Volume[iDayExchange]; 
                  sInputValue = ConvertVolume(sInputValue,"LITRE");
              }else if(ContainsText(sObjectName,"Time")){
                  sInputValue = DwellTime[iDayExchange];
                  sInputValue = ConvertTime(sInputValue,"MINUTE");
                  iDayExchange++; //Time is the last column in a exchange, so increment it for the next loop
              }else{
                  sInputValue = CycleTime[iDayExchange]; //Cycle
              } 
    
              //Perform the Action
              intStepStatus = DataEntry(sAction,objName,sInputValue,sObjectName,sStepNo);
              VerificationPoint(g_strFuncCall,sStepNo,intStepStatus,intIterCnt);
              
          }
      } 
      
      g_stepnum = sStepNo; 
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}


//*************************************************************************************************
// Function Name        : VerifyDryInputs
// Function Description : To verify the Dry Inputs in the Regimen Summary based on the values from Calculation File.                    
// Inputs               : N/A     
// Returns              : N/A
//*************************************************************************************************
function VerifyDryInputs(){
 
  var sObjectName = null;     //Variable for storing object name  
  var sInputValue = null;
  var sAction = null;
  var sInputValue1 = null;
  var sStepNo = null;           //Variable for storing step number

  var intIniRwNo = g_intStrtRow;        //Variable for storing initial row number
  var intEndRwNo = g_intEndRow;        //Variable for storing end row number  
  var intStepStatus = 1;      //Initialize the status to Fail
  var strData = null;
  var isDryTimeAvaiable = false;
  try{
  
      oRDRecordSet.MoveFirst();  //Since Day Exchange verification would have moved it to last.
      if (GetCalculationData("RegimenDryTime", true) > 0 ){
         isDryTimeAvaiable = true;
      }
            
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      
          //Initialize the status to fail for each each step.
          intStepStatus = 1;  //Fail
      
          sObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
          sInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
          sAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
          sInputValue1 = ReadExcel(intIterCnt,c_intInputVal1Col);
          sStepNo = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
          
          if (sObjectName != null){
              objName = BuildWebObj(g_dicColNames_obj.item(sObjectName));  //Build object from the given object name
              if (!isDryTimeAvaiable){
                  sAction = "VerifyNotExist";    
              }else if(ContainsText(sObjectName,"Cycle")){ 
                  sInputValue = sInputValue;
              }else if(ContainsText(sObjectName,"Solution")){
                  sInputValue = sInputValue; //No Change
              }else if(ContainsText(sObjectName,"Volume")){
                  sInputValue = sInputValue; //No Change 
              }else if(ContainsText(sObjectName,"Time")){
                  sInputValue = GetCalculationData(sInputValue);
              }
    
              //Perform the Action
              intStepStatus = DataEntry(sAction,objName,sInputValue,sObjectName,sStepNo);
              VerificationPoint(g_strFuncCall,sStepNo,intStepStatus,intIterCnt);
          }
      } 
      
      g_stepnum = sStepNo; 
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}


//*************************************************************************************************
// Function Name        : VerifyNightInputs
// Function Description : To verify the Night Inputs in the Regimen Summary based on the values from Calculation File.                    
// Inputs               : N/A     
// Returns              : N/A
//*************************************************************************************************
function VerifyNightInputs(){
 
  var sObjectName = null;     //Variable for storing object name  
  var sInputValue = null;
  var sAction = null;
  var sInputValue1 = null;
  var sStepNo = null;           //Variable for storing step number

  var intIniRwNo = g_intStrtRow;        //Variable for storing initial row number
  var intEndRwNo = g_intEndRow;        //Variable for storing end row number  
  var intStepStatus = 1;      //Initialize the status to Fail
  var strData = null;
  
  try{
  
      
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      
          //Initialize the status to fail for each each step.
          intStepStatus = 1;  //Fail
      
          sObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
          sInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
          sAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
          sInputValue1 = ReadExcel(intIterCnt,c_intInputVal1Col);
          sStepNo = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
          
          if (sObjectName != null){
              objName = BuildWebObj(g_dicColNames_obj.item(sObjectName));  //Build object from the given object name
              if(ContainsText(sObjectName,"Cycle")){ 
                  sInputValue = GetRegimenData(sInputValue);
              }else if(ContainsText(sObjectName,"Solution")){
                  sInputValue = "NightExchangeSolution";
                  if (CompareText(GetCalculationData("RegimenType", false),"TIDAL")){
                      sInputValue = "NightExchangeSolutionTidal";
                  }
                  sInputValue = GetRegimenData(sInputValue);
              }else if(ContainsText(sObjectName,"Volume")){
                      sInputValue = GetRegimenData(sInputValue) * GetRegimenData("NightExchangeCount");
                      sInputValue = ConvertVolume(sInputValue,"LITRE");
              }else if(ContainsText(sObjectName,"Time")){
                  sInputValue = GetRegimenData(sInputValue);
                  sInputValue = ConvertTime(sInputValue,"MINUTE");
              }
    
              //Perform the Action
              intStepStatus = DataEntry(sAction,objName,sInputValue,sObjectName,sStepNo);
              VerificationPoint(g_strFuncCall,sStepNo,intStepStatus,intIterCnt);
          }
      } 
      
      g_stepnum = sStepNo; 
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}

//*************************************************************************************************
// Function Name        : VerifyTotalInputsRegimenSummary
// Function Description : To verify the Total Inputs in the Regimen Summary based on the values from Calculation File.                    
// Inputs               : N/A     
// Returns              : N/A
//*************************************************************************************************
function VerifyTotalInputsRegimenSummary(){
 
  var sObjectName = null;     //Variable for storing object name  
  var sInputValue = null;
  var sAction = null;
  var sInputValue1 = null;
  var sStepNo = null;           //Variable for storing step number

  var intIniRwNo = g_intStrtRow;        //Variable for storing initial row number
  var intEndRwNo = g_intEndRow;        //Variable for storing end row number  
  var intStepStatus = 1;      //Initialize the status to Fail
  var strData = null;
  
  try{
  
      
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      
          //Initialize the status to fail for each each step.
          intStepStatus = 1;  //Fail
      
          sObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
          sInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
          sAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
          sInputValue1 = ReadExcel(intIterCnt,c_intInputVal1Col);
          sStepNo = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
          
          if (sObjectName != null){
              objName = BuildWebObj(g_dicColNames_obj.item(sObjectName));  //Build object from the given object name
              if(ContainsText(sObjectName,"Cycle")){ 
                  sInputValue = Number(GetRegimenData("DayExchangeCount")) + Number(GetRegimenData("NightExchangeCount"))
              }else if(ContainsText(sObjectName,"Solution")){
                  sInputValue = sInputValue;//No Change
              }else if(ContainsText(sObjectName,"Volume")){
                  sInputValue = GetExternalData("RunTimeData",sInputValue);
                  sInputValue = ConvertVolume(sInputValue,"LITRE");
              }else if(ContainsText(sObjectName,"Time")){
                  sInputValue = sInputValue //No Change
              }else if(ContainsText(sObjectName,"Effluent")){
                  sInputValue = GetExternalData("RunTimeData",sInputValue);
                  sInputValue = ConvertVolume(sInputValue,"LITRE");
                  sInputValue = Number(sInputValue) + Number(GetCalculationData("RegimenDailyUF", true));
              }              
    
              //Perform the Action
              intStepStatus = DataEntry(sAction,objName,sInputValue,sObjectName,sStepNo);
              VerificationPoint(g_strFuncCall,sStepNo,intStepStatus,intIterCnt);
          }
      } 
      
      g_stepnum = sStepNo; 
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}

//*************************************************************************************************
// Function Name        : ConvertVolume
// Function Description : To convert volumes from one unit to another unit                    
// Inputs               : Input Value for Volume, Required Unit type
// Returns              : Values in Required unit
//*************************************************************************************************
function ConvertVolume(InputValue,OutputType){
  var ReturnValue = InputValue;
  try{
      OutputType = Trim(OutputType).toUpperCase();
      if (CompareText(OutputType,"LITRE")){
          ReturnValue = InputValue / 1000;
          ReturnValue = RoundOff(ReturnValue,2);
      }else if(CompareText(OutputType,"MILLILITRE")){
          ReturnValue = InputValue * 1000;
      }
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
  return ReturnValue;
}

//*************************************************************************************************
// Function Name        : ConvertTime
// Function Description : To convert time from one format to another format 
// Inputs               : Input Value for time, Required time format    
// Returns              : Values in Required format
//*************************************************************************************************
function ConvertTime(InputValue,OutputType){
  var ReturnValue = InputValue;
  try{
      OutputType = Trim(OutputType).toUpperCase();
      if (CompareText(OutputType,"HOUR")){
          ReturnValue = InputValue / 60;
      }else if(CompareText(OutputType,"MINUTE")){
          ReturnValue = InputValue * 60;
      }else if (CompareText(OutputType,"HH:MM")){
          if (IsNumeric(InputValue) && aqConvert.VarToStr(InputValue).length > 0 ){
              Hour = Math.floor(InputValue / 60);
              Hour = (Hour<10) ? "0" + Hour : Hour;
              Minutes = (InputValue % 60);
              Minutes = (Minutes<10) ? "0" + Minutes : Minutes;
              ReturnValue = Hour + ":" + Minutes;
          }else{
              ReturnValue = "00:00";
          }  
      }else if (CompareText(OutputType,"H:MM")){
          if (IsNumeric(InputValue) && aqConvert.VarToStr(InputValue).length > 0 ){
              Hour = Math.floor(InputValue / 60);
              Minutes = (InputValue % 60);
              Minutes = (Minutes<10) ? "0" + Minutes : Minutes;
              ReturnValue = Hour + ":" + Minutes;
          }else{
              ReturnValue = "0:0";
          }              
      }
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
  return ReturnValue;
}



//*************************************************************************************************
// Function Name        : VerifyDayInputsInList
// Function Description : To verify the Day Inputs in the Regimen List based on the values from Calculation File.                    
// Inputs               : N/A     
// Returns              : N/A
//*************************************************************************************************
function VerifyDayInputsInList(){
 
  var sObjectName = null;     //Variable for storing object name  
  var sInputValue = null;
  var sAction = null;
  var sInputValue1 = null;
  var sStepNo = null;           //Variable for storing step number

  var intIniRwNo = g_intStrtRow;        //Variable for storing initial row number
  var intEndRwNo = g_intEndRow;        //Variable for storing end row number  
  var intStepStatus = 1;      //Initialize the status to Fail
  var strData = null;
  var TotalDayExchange = 0;
  var iDayExchange = 1;
  var Solution = new Array();
  var Volume = new Array();
  var DwellTime = new Array();
  var CycleTime = new Array();

  try{
  
      TotalDayExchange = GetRegimenData("DayExchangeCount");
      ExpectedRecordCount = oRDRecordSet.RecordCount;
      RegimenType = GetCalculationData("RegimenType", false);
      oRDRecordSet.MoveFirst();
     
      //Prepare to loop the record set and assign it to the arrays
      Solution[0] = 0;
      Volume[0] = 0;
      DwellTime[0] = 0;
      CycleTime[0] = TotalDayExchange;
      
      for (iDayExchange=1; iDayExchange<=TotalDayExchange; iDayExchange++){
          //Take the values from RecordSet to an Array
          Solution[iDayExchange] = Number(GetRegimenData("DayExchangeSolution"));
          Volume[iDayExchange] = Number(GetRegimenData("DayVolumeInfused"));
          DwellTime[iDayExchange] = Number(GetRegimenData("DayExchangeDwellTime"));
          CycleTime[iDayExchange] = iDayExchange + 1; //Since one is already last fill.
          //Update the first element, with the sum of all other elements.
          Solution[0] = Number(Solution[0]) + Number(Solution[iDayExchange]);
          Volume[0] = Number(Volume[0]) + Number(Volume[iDayExchange]);
          DwellTime[0] = Number(DwellTime[0]) + Number(DwellTime[iDayExchange]);
          oRDRecordSet.MoveNext();
      }
      
      
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      
          //Initialize the status to fail for each each step.
          intStepStatus = 1;  //Fail
      
          sObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
          sInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
          sAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
          sInputValue1 = ReadExcel(intIterCnt,c_intInputVal1Col);
          sStepNo = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
          
          if (sObjectName != null){
              objName = BuildWebObj(g_dicColNames_obj.item(sObjectName));  //Build object from the given object name
              if(ContainsText(sObjectName,"Cycle") || ContainsText(sObjectName,"Exchange")){
                  sInputValue = CycleTime[0]; //Cycle or Exchange   
              }else if(ContainsText(sObjectName,"Solution") || ContainsText(sObjectName,"Concentration")){
                  sInputValue = 0;
                  if (TotalDayExchange > 0){//Calcualtion avg concentration only if day exchange count is more than 0
                      sInputValue = RoundOff(Solution[0]/CycleTime[0],2); 
                  }
              }else if(ContainsText(sObjectName,"Volume")){
                  sInputValue = Volume[0];
                  sInputValue = ConvertVolume(sInputValue,"LITRE");
              }else if(ContainsText(sObjectName,"Time")){
                  sInputValue = DwellTime[0];
                  if (!CompareText(RegimenType,"CAPD")){
                      sInputValue = ConvertTime(sInputValue,"MINUTE");
                  }
                  sInputValue = ConvertTime(sInputValue,"H:MM");
              }else if (ContainsText(sObjectName,"TypicalOrActual")){
                  sInputValue = GetExternalData("CalculationData",sInputValue);
                  sInputValue = sInputValue.toUpperCase();
                  if (CompareText(sInputValue,"NO")){
                      sInputValue = "ACTUAL"
                  }else{
                      sInputValue = "TYPICAL"
                  }
              }
    
              //Perform the Action
              intStepStatus = DataEntry(sAction,objName,sInputValue,sObjectName,sStepNo);
              VerificationPoint(g_strFuncCall,sStepNo,intStepStatus,intIterCnt);
              
          }
      } 
      
      g_stepnum = sStepNo; 
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}

//*************************************************************************************************
// Function Name        : VerifyNightInputsInList
// Function Description : To verify the Night Inputs in the Regimen List based on the values from Calculation File.                    
// Inputs               : N/A     
// Returns              : N/A
//*************************************************************************************************
function VerifyNightInputsInList(){
 
  var sObjectName = null;     //Variable for storing object name  
  var sInputValue = null;
  var sAction = null;
  var sInputValue1 = null;
  var sStepNo = null;           //Variable for storing step number

  var intIniRwNo = g_intStrtRow;        //Variable for storing initial row number
  var intEndRwNo = g_intEndRow;        //Variable for storing end row number  
  var intStepStatus = 1;      //Initialize the status to Fail
  var strData = null;
  
  try{
  
      oRDRecordSet.MoveFirst();//Since the day exchange would have processed the last row.
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      
          //Initialize the status to fail for each each step.
          intStepStatus = 1;  //Fail
      
          sObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
          sInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
          sAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
          sInputValue1 = ReadExcel(intIterCnt,c_intInputVal1Col);
          sStepNo = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
          
          if (sObjectName != null){
              objName = BuildWebObj(g_dicColNames_obj.item(sObjectName));  //Build object from the given object name
              if(ContainsText(sObjectName,"Cycle") || ContainsText(sObjectName,"Exchange")){ 
                  sInputValue = GetRegimenData(sInputValue);
              }else if(ContainsText(sObjectName,"Solution") || ContainsText(sObjectName,"Concentration")){
                  sInputValue = "NightExchangeSolution";
                  if (CompareText(GetCalculationData("RegimenType", false),"TIDAL")){
                      sInputValue = "NightExchangeSolutionTidal";
                  }           
                  sInputValue = GetRegimenData(sInputValue);
              }else if(ContainsText(sObjectName,"Volume")){
                  sInputValue = GetRegimenData(sInputValue) * GetRegimenData("NightExchangeCount");
                  sInputValue = ConvertVolume(sInputValue,"LITRE");
              }else if(ContainsText(sObjectName,"NightDwellTime")){
                  sInputValue = GetRegimenData(sInputValue);
                  sInputValue = ConvertTime(sInputValue,"MINUTE");
                  sInputValue = ConvertTime(sInputValue,"H:MM");
              }else if(ContainsText(sObjectName,"DryDwellTime")){
                  sInputValue = GetCalculationData(sInputValue,false);
                  sInputValue = ConvertTime(sInputValue,"H:MM");
              }
    
              //Perform the Action
              intStepStatus = DataEntry(sAction,objName,sInputValue,sObjectName,sStepNo);
              VerificationPoint(g_strFuncCall,sStepNo,intStepStatus,intIterCnt);
          }
      } 
      
      g_stepnum = sStepNo; 
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}

//*************************************************************************************************
// Function Name        : UpdateTypicalAndActualFields
// Function Description : To modify the Typical and Actual fields in the Regimen page based on the values from Calculation File.                    
// Inputs               : N/A     
// Returns              : N/A
//*************************************************************************************************
function UpdateTypicalAndActualFields(){
 
  var sObjectName = null;     //Variable for storing object name  
  var sInputValue = null;
  var sAction = null;
  var sInputValue1 = null;
  var sStepNo = null;           //Variable for storing step number

  var intIniRwNo = g_intStrtRow;        //Variable for storing initial row number
  var intEndRwNo = g_intEndRow;        //Variable for storing end row number  
  var intStepStatus = 1;      //Initialize the status to Fail
  var strData = null;
  var TypicalStatus = null;
  
  try{
      TypicalStatus = GetTypicalStatus();
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      
          //Initialize the status to fail for each each step.
          intStepStatus = 1;  //Fail
      
          sObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
          sInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
          sAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
          sInputValue1 = ReadExcel(intIterCnt,c_intInputVal1Col);
          sStepNo = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
          sInputValue = GetExternalData("CalculationData",sInputValue);
          if (sObjectName != null){
              objName = BuildWebObj(g_dicColNames_obj.item(sObjectName));  //Build object from the given object name
              sInputValue = sInputValue.toUpperCase();
              
              if (!TypicalStatus){ //User can change only when it is not typical.
                  if(CompareText(sInputValue,"YES")){
                      sAction = "Click"
                  }else{
                      sAction = "VerifyRadioUnSelected";
                  }              
              }else{
                  sAction = "VerifyRadioSelected"   //If it is typical then, typical radio button will be selected by default
              }

              //Perform the Action
              intStepStatus = DataEntry(sAction,objName,sInputValue,sObjectName,sStepNo);
              VerificationPoint(g_strFuncCall,sStepNo,intStepStatus,intIterCnt);
          }
          
      } 
      g_stepnum = sStepNo; 
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}


//*************************************************************************************************
// Function Name        : VerifyPagination
// Function Description : To verify that the pagination for the Footer section                    
// Inputs               : N/A     
// Returns              : N/A
//*************************************************************************************************

 function VerifyPagination(){
 
  var sObjectName= "";     //Variable for storing object name  
  var sStepNo = "";           //Variable for storing step number
  var iStepStatus = 1;      //Initialize the status to Fail
  var iInputValue;
  var Total_No_Of_Pages;
  var Start_Record_Value;
  var End_Record_Value;  
  var Expected_Record_Value=null;   
  var Actual_Output_Value=null;
  var sObjectName1 = null;
  var RowsCount = 0;
  
   try{
     
    sObjectName = ReadExcel(g_intStrtRow,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
    sStepNo = ReadExcel(g_intStrtRow,c_intStepCol);  //Reads the step from c_intStepCol
    strInputValue = ReadExcel(g_intStrtRow,c_intInputValCol);  //Reads the input value from c_intInputValCol
    g_OtherInputValue = ReadExcel(g_intStrtRow,c_intInputVal1Col);  //Reads the input value from c_intInputValCol1i.e., having total Number of Records
    strAction = ReadExcel(intIterCnt, c_intActionCol);  //Reads the action from c_intActionCol
    if (sObjectName != null){
        objName = BuildWebObj(g_dicColNames_obj.item(sObjectName));
        ObjDesc = eval(objName);
        ActualText = GetText(ObjDesc);//Gets the value of the object from screen
    }
   
   
        switch (trim(sObjectName)){
          case "PatientDashboard_PDAPatientsPagination_Label": 
              sObjectName1="PatientList_AdequestPatients_Count";
              break; 
          case "PatientDashboard_InactivePatientsPagination_Label": 
              sObjectName1="PatientList_InactivePatients_Count";
              break; 
          case "PatientDashboard_SharesourcePatientsPagination_Label": 
              sObjectName1="PatientList_SharesourcePatients_Count";
              break; 
        }    
   
        if (!IsNullorUndefined(sObjectName1)){
            objName = GetChildObjectsByXPath(g_dicColNames_obj.item(sObjectName1));
            ScrollIntoView(objName);
            RowsCount =  objName.length; 
        }
     
        ExpectedText = GetRecordInfoText(strInputValue,g_OtherInputValue,RowsCount);
        ExpectedMessage = "Text of "+sObjectName+" shall be '"+ExpectedText + "'";
        ActualMessage= "Text of "+sObjectName+" is '"+ActualText + "'";        
        
        if (CompareText(ExpectedText,ActualText)){
          iStepStatus = 0;
          Log.Message(sStepNo + " - " + ActualMessage);
        }else{
          Log.Error(sStepNo + " - " + ActualMessage);  
        } 
       g_stepnum = sStepNo;     
   }catch(e){
        Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
   }finally{
        VerificationPoint(g_strFuncCall,sStepNo,iStepStatus,g_intStrtRow);
   }
 }
 
//*************************************************************************************************
// Function Name        : AddPatientsToAdequest
// Function Description : To Add the patients from sharesource to Active Adequest Patients                    
// Inputs               : N/A     
// Returns              : N/A
//*************************************************************************************************

 function AddPatientsToAdequest(){
 
  var sObjectName= "";     //Variable for storing object name  
  var sStepNo = "";           //Variable for storing step number
  var iStepStatus = 1;      //Initialize the status to Fail
  var iInputValue;
  var count=0;
  var PDAutoPatientCount = 25;
   try{
     
    sObjectName = ReadExcel(g_intStrtRow,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
    sStepNo = ReadExcel(g_intStrtRow,c_intStepCol);  //Reads the step from c_intStepCol
    iInputValue = ReadExcel(g_intStrtRow,c_intInputValCol);  //Reads the input value from c_intInputValCol
 if (!IsNullorUndefined(sObjectName)){
  for(var i=1;i<=PDAutoPatientCount;i++){
    //Add Patient button
    objName_AddPatient = BuildWebObj(g_dicColNames_obj.item(sObjectName));
    //SecondarMenu_AdequestDashBoard
    objName_AdequestDashBoard = BuildWebObj(g_dicColNames_obj.item("SecondaryMenu_AdequestDashboard"));
    //Search Button
    objName_SearchButton = BuildWebObj(g_dicColNames_obj.item("PatientList_SearchButton"));
   
          objName_AddPatient = eval(objName_AddPatient);
          ObjDesc_AdequestDashBoard = eval(objName_AdequestDashBoard);
          ObjDesc_SearchButton = eval(objName_SearchButton);
            if(IsExists(objName_AddPatient)){

                objName_AddPatient.Click();
                WaitForPageSync();//Wait for page to load

                //Submit object
                objName_Submit = BuildWebObj(g_dicColNames_obj.item("AddAdequestPatient_Submit_Button"));
                ObjDesc_Submit = eval(objName_Submit);
                ObjDesc_Submit.Click();
                WaitForPageSync();
            
                ObjDesc_AdequestDashBoard.Click();
                WaitForPageSync();
                WaitForLoadingSpinner();
            
                ObjDesc_SearchButton.Click();
                WaitForPageSync();
                WaitForLoadingSpinner();
                count++;
            }
        }
        ExpectedResult = count + " Patients are added to Adequest.";
        ActualResult = PDAutoPatientCount + " Patients shall be added to Adequest.";
        if (CompareText(count,PDAutoPatientCount)){
          iStepStatus = 0;
          Log.Message(sStepNo + " - " + ActualResult);
        }else{
          Log.Error(sStepNo + " - " + ActualResult);  
        }                 
   }  
     g_stepnum = sStepNo;     
   }catch(e){
        Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
   }finally{
        VerificationPoint(g_strFuncCall,sStepNo,iStepStatus,g_intStrtRow);
   }
 }
 
//*************************************************************************************************
// Function Name        : GetRecordInfoText
// Function Description : To get the footer Section Expected Result                    
// Inputs               : Total Record Count, Page Number,Total Row Count for Each Page    
// Returns              : Expected Footer to be displayed
//*************************************************************************************************
 
function GetRecordInfoText(PageInfo,PageNumber,RecordsDisplayedInThePage){
    var ReturnValue = null;
    var TotalRecordCount = null;
    var CurrentPageNumber = null;
    var Min = null;
    var Max = null;
    var RecordsPerPage = 10;
    
    try{
        TotalRecordCount = Number(PageInfo);
        CurrentPageNumber = Number(PageNumber);
        
        Min = ((CurrentPageNumber -1) * RecordsPerPage ) + 1;
        Max = Number(Min + RecordsDisplayedInThePage)- 1;

         //Records {MIN}-{MAX} of {SIZE}_en_GB  
         ReturnValue = GetExternalData("LocalizationData","paginationTableFooter");
         ReturnValue = ReturnValue.replace("{MIN}",Min);
         ReturnValue = ReturnValue.replace("{MAX}",Max);
         ReturnValue = ReturnValue.replace("{SIZE}",TotalRecordCount);
            
    }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    } 
    return ReturnValue;
}



//*************************************************************************************************
// Function Name        : VerifyTypicalAndActualValues
// Function Description : To verify the Typical and Actual values of PET in the Add Regimen Page.                    
// Inputs               : N/A     
// Returns              : N/A
//*************************************************************************************************
function VerifyTypicalAndActualValues(){
 
  var sObjectName = null;     //Variable for storing object name  
  var sInputValue = null;
  var sAction = null;
  var sInputValue1 = null;
  var sStepNo = null;           //Variable for storing step number

  var intIniRwNo = g_intStrtRow;        //Variable for storing initial row number
  var intEndRwNo = g_intEndRow;        //Variable for storing end row number  
  var intStepStatus = 1;      //Initialize the status to Fail
  var strData = null;
  var isTypical = null;
  try{
      isTypical = GetTypicalStatus();
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      
          //Initialize the status to fail for each each step.
          intStepStatus = 1;  //Fail
      
          sObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
          sInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
          sAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
          sInputValue1 = ReadExcel(intIterCnt,c_intInputVal1Col);
          sStepNo = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
          
          if (sObjectName != null){
              objName = BuildWebObj(g_dicColNames_obj.item(sObjectName));  //Build object from the given object name
                  //Typical Conditions
              if(ContainsText(sObjectName,"Typical") && ContainsText(sInputValue,"Selected")){
                  sAction = isTypical ? "VerifyRadioSelected" : "VerifyRadioUnSelected"   
              }else if(ContainsText(sObjectName,"Typical") && ContainsText(sInputValue,"Enabled")){
                  sAction = isTypical ? "Verifydisable" : "Verifyenable"                           
                  
                  //Actual Conditions
              }else if(ContainsText(sObjectName,"Actual") && ContainsText(sInputValue,"Selected")){
                  sAction = isTypical ? "VerifyRadioUnSelected" : "VerifyRadioSelected"    
              }else if(ContainsText(sObjectName,"Actual") && ContainsText(sInputValue,"Enabled")){
                  sAction = isTypical ? "Verifydisable" : "Verifyenable"                                   
              }
              //Perform the Action
              intStepStatus = DataEntry(sAction,objName,sInputValue,sObjectName,sStepNo);
              VerificationPoint(g_strFuncCall,sStepNo,intStepStatus,intIterCnt);
          }
      } 
      g_stepnum = sStepNo; 
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}


//*************************************************************************************************
// Function Name        : VerifyTypicalAndActualCalculations
// Function Description : To verify the Typical and Actual calculations of PET in the Add Regimen Page.                    
// Inputs               : N/A     
// Returns              : N/A
//*************************************************************************************************
function VerifyTypicalAndActualCalculations(){
 
  var sObjectName = null;     //Variable for storing object name  
  var sInputValue = null;
  var sAction = null;
  var sInputValue1 = null;
  var sStepNo = null;           //Variable for storing step number

  var intIniRwNo = g_intStrtRow;        //Variable for storing initial row number
  var intEndRwNo = g_intEndRow;        //Variable for storing end row number  
  var intStepStatus = 1;      //Initialize the status to Fail
  var strData = null;
  var isTypical = null;
  var oApplicationObject = null;
  var objName = null;
  try{
      isTypical = GetTypicalStatus();
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      
          //Initialize the status to fail for each each step.
          intStepStatus = 1;  //Fail
      
          sObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
          sInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
          sAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
          sInputValue1 = ReadExcel(intIterCnt,c_intInputVal1Col);
          sStepNo = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
          
          if (sObjectName != null){
          
              sInputValue = GetExternalData(sInputValue1,sInputValue);
              objName = BuildWebObj(g_dicColNames_obj.item(sObjectName));  //Build object from the given object name
              oApplicationObject = WaitForObject(objName);
    
             //If the Object does not exist, then log the error and exit the function
             if (!IsExists(oApplicationObject)){
                 ActualMessage = "'" + ObjectName + "' Object does not exist so failed to perform " + action;
                 intStepStatus = 1; //Fail
                 Log.Error (StepNumber+" -"+ActualMessage) ;
             }else{
             
                  if(ContainsText(sObjectName,"Typical") && ContainsText(sObjectName,"ResidualDialysate")){
                      sInputValue = (10 * Number(sInputValue)) / 100; //10% of Four Hour Volume Infused
                  }             
                  var ApplicationText = GetText(oApplicationObject);
                  ApplicationText = Trim(ApplicationText.split(":")[1])
                  intStepStatus = VerifyCalculatedValues(sStepNo,sObjectName, ApplicationText, sInputValue);
             }      
                    
             VerificationPoint(g_strFuncCall,sStepNo,intStepStatus,intIterCnt);
          }
      } 
      g_stepnum = sStepNo; 
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}


//*************************************************************************************************
// Function Name        : GetTypicalStatus
// Function Description : To check whether the current record holds data for Typical values or not                    
// Inputs               : N/A     
// Returns              : true if Typical and false otherwise
//*************************************************************************************************
function GetTypicalStatus(){
    var ReturnValue = false;//standard PET
    var ConditionValue = "";
    var PETType = "";
    var TypicalReason = "";
    try{
    
        //Typical Check by Four Hour Solution Concentration.
        ConditionValue = Trim(GetCalculationData("FourHourSolution",false));
        if (CompareText(ConditionValue,"1.5")){
            ReturnValue = true;
            TypicalReason = "Typical due to Four Hour Solution: " + ConditionValue;
            Log.Message(TypicalReason);
            return ReturnValue;
        }
    
        //Typical Check by Fluid Absorption
        ConditionValue = Number(GetCalculationData("FluidAbsorption",true));
        if ( ConditionValue < 0.1 || ConditionValue > 3.0 ){
            ReturnValue = true;
            TypicalReason = "Typical due to Fluid Absorption (QL): " + ConditionValue;
            Log.Message(TypicalReason);
            return ReturnValue;
        }
        
        //Simulated Check by Residual Dialysate Volume
        ConditionValue = Number(GetCalculationData("ResidualDialysateVolume",true));
        if ( ConditionValue <= 1 ){
            ReturnValue = true;
            TypicalReason = "Typical due to Residual Dialysate Volume " + ConditionValue;
            Log.Message(TypicalReason);
            return ReturnValue;
        }
        
        //Simulated Check by Hydraulic Permeability
        ConditionValue = Number(GetCalculationData("HydraulicPermeability",true));
        if ( ConditionValue < 0 ){
            ReturnValue = true;
            TypicalReason = "Typical due to Hydraulic Permeability (LPA): " + ConditionValue;
            Log.Message(TypicalReason);
            return ReturnValue;
        }
        
    }catch(e){
        Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    }
    return ReturnValue;
}




//*************************************************************************************************
// Function Name        : CloseConfirmPopup
// Function Description : To click on link,button and image                 
// Inputs               : None  
// Returns              : None     
//************************************************************************************************
function CloseConfirmPopup(){
  try{
    var intIniRwNo ;       //Variable for storing initial row number
    var strObjectName;     //Variable for storing object name
    var objName;           //Variable for storing object 
    var strStep = "";           //Variable for storing step number
    var strInputValue ;    //Variable for storing input value 
    var strAction;         //Variable for storing strAction 
    var intStepCounter = 1;  //Variable for storing result for each step in the keyword (0 denotes step is pass/1 denotes step is fail)
    var objnotexist = 0;   //Variable for storing whether the object exists or not
    var strStepType;       //Variable for storing step type
    intIniRwNo = g_intStrtRow;  //Searches the g_intStrtRow in testdata sheet and reads the row number of a keyword       
    strObjectName = ReadExcel(intIniRwNo,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
    if (strObjectName != null){
      objName = BuildWebObj(g_dicColNames_obj.item(strObjectName));  //Build object from the given object name    
      strInputValue = ReadExcel(intIniRwNo,c_intInputValCol);  //Reads the input value from c_intInputValCol
      strAction = ReadExcel(intIniRwNo,c_intActionCol);  //Reads the action from c_intActionCol   
      strStep = ReadExcel(intIniRwNo,c_intStepCol);  //Reads the step from c_intStepCol
      //This function performs the strActions specified in the excel 
      strStepType = ReadExcel (intIniRwNo,c_intStepType); //Reads the step type from c_intStepType
      strInputValue=strInputValue.toUpperCase();
      if (IsNullorUndefined(oPageObject)){
          oPageObject = eval(g_PageObject);
      }
      if(CompareText(strInputValue, "OK")){
          intStepCounter = ConfirmWindow(oPageObject, true);//To click on Confirm window ok button
          strObjectName = "OK";
      }else{
          intStepCounter = ConfirmWindow(oPageObject, false);//To click on Confirm window ok button
          strObjectName = "Cancel";
      }
        
    
      if(intStepCounter) {
        Log.Message("CloseConfirm function is passed");
        fnInsertResult ("CloseConfirm",strStep,strObjectName+" shall be clicked",strObjectName+" is clicked","PASS",strStepType);
      } 
      else{
        Log.Message("CloseConfirm function is failed");
        fnInsertResult ("CloseConfirm ",strStep,strObjectName +" shall be clicked",strObjectName+" is not clicked","FAIL",strStepType);
        g_intFailCnt = g_intFailCnt + 1;
      }  
    }     
      g_stepnum = strStep;
     }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}



//*************************************************************************************************
// Function Name        : VerifyCalculationMethods
// Function Description : To verify the calculation methods (Body Volume and nPCR methods)                    
// Inputs               : N/A     
// Returns              : N/A
//*************************************************************************************************
function VerifyCalculationMethods(){
 
  var sObjectName = null;     //Variable for storing object name  
  var sInputValue = null;
  var sAction = null;
  var sInputValue1 = null;
  var sStepNo = null;           //Variable for storing step number

  var intIniRwNo = g_intStrtRow;        //Variable for storing initial row number
  var intEndRwNo = g_intEndRow;        //Variable for storing end row number  
  var intStepStatus = 1;      //Initialize the status to Fail
  var strData = null;
  try{
  
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      
          //Initialize the status to fail for each each step.
          intStepStatus = 1;  //Fail
      
          sObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
          sInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
          sAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
          sInputValue1 = ReadExcel(intIterCnt,c_intInputVal1Col);
          sStepNo = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
          
          if (sObjectName != null){
              objName = BuildWebObj(g_dicColNames_obj.item(sObjectName));  //Build object from the given object name
              sInputValue = GetExternalData("CalculationData",sInputValue);
              if (sInputValue == 'W'){
                  sInputValue = GetLocalizationData("watsonAndWatsonLabel"); 
                  if (GetCalculationData("PediatricStatus", false) == 'Y'){
                      sInputValue = GetLocalizationData("morgensternEtAlLabel"); 
                  }  
              }else if (sInputValue == 'H'){
                  sInputValue = GetLocalizationData("humeAndWeyersLabel");   
                  if (GetCalculationData("PediatricStatus", false) == 'Y'){
                      sInputValue = GetLocalizationData("morgensternEtAlLabel"); 
                  }                                 
              }else if (sInputValue == 'R'){
                  sInputValue = GetLocalizationData("randersonLabel");
              }else if (sInputValue == 'B'){
                  sInputValue = GetLocalizationData("bergstromLabel");
              }
    
              //Perform the Action
              intStepStatus = DataEntry(sAction,objName,sInputValue,sObjectName,sStepNo);
              VerificationPoint(g_strFuncCall,sStepNo,intStepStatus,intIterCnt);
          }
          
      } 
      g_stepnum = sStepNo; 
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}


//*************************************************************************************************
// Function Name        : UpdateTidalFields
// Function Description : To modify the Dry fields in the Regimen page based on the values from Calculation File.                    
// Inputs               : N/A     
// Returns              : N/A
//*************************************************************************************************
function UpdateTidalFields(){
 
  var sObjectName = null;     //Variable for storing object name  
  var sInputValue = null;
  var sAction = null;
  var sInputValue1 = null;
  var sStepNo = null;           //Variable for storing step number

  var intIniRwNo = g_intStrtRow;        //Variable for storing initial row number
  var intEndRwNo = g_intEndRow;        //Variable for storing end row number  
  var intStepStatus = 1;      //Initialize the status to Fail
  var strData = null;
  var isTidalAvaiable = false;
  try{
  
      
      if (CompareText(GetCalculationData("RegimenType", false),"TIDAL")){
         isTidalAvaiable = true;
      }
      
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      
          //Initialize the status to fail for each each step.
          intStepStatus = 1;  //Fail
      
          sObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
          sInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
          sAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
          sInputValue1 = ReadExcel(intIterCnt,c_intInputVal1Col);
          sStepNo = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
          
          if (sObjectName != null){
              objName = BuildWebObj(g_dicColNames_obj.item(sObjectName));  //Build object from the given object name
              if (!isTidalAvaiable){
                  sAction = "VerifyExist";    
              }
    
              //Perform the Action
              intStepStatus = DataEntry(sAction,objName,sInputValue,sObjectName,sStepNo);
              VerificationPoint(g_strFuncCall,sStepNo,intStepStatus,intIterCnt);
              
              //If Dry Time exchange is not present, then process only the first row.
              if (!isTidalAvaiable){
                  intIterCnt = intEndRwNo +1;
              }
          }
          
      } 
      g_stepnum = sStepNo; 
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}


//*************************************************************************************************
// Function Name        : VerifyRegimenType
// Function Description : To verify regimen type                    
// Inputs               : N/A     
// Returns              : N/A
//*************************************************************************************************
function VerifyRegimenType(){
 
  var sObjectName = null;     //Variable for storing object name  
  var sInputValue = null;
  var sAction = null;
  var sInputValue1 = null;
  var sStepNo = null;           //Variable for storing step number

  var intIniRwNo = g_intStrtRow;        //Variable for storing initial row number
  var intEndRwNo = g_intEndRow;        //Variable for storing end row number  
  var intStepStatus = 1;      //Initialize the status to Fail
  var strData = null;
  var RegimenType = null;
  try{
  
      RegimenType = GetCalculationData("RegimenType", false);
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      
          //Initialize the status to fail for each each step.
          intStepStatus = 1;  //Fail
      
          sObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
          sInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
          sAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
          sInputValue1 = ReadExcel(intIterCnt,c_intInputVal1Col);
          sStepNo = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
          
          if (sObjectName != null){
              objName = BuildWebObj(g_dicColNames_obj.item(sObjectName));  //Build object from the given object name
              if (ContainsText(sObjectName,"Modality")){
                  if (CompareText(RegimenType,"APD")){
                     sInputValue = GetLocalizationData("APDLabel");
                  }else if (CompareText(RegimenType,"TIDAL")){
                     sInputValue = GetLocalizationData("TIDALLabel");
                  }else if (CompareText(RegimenType,"CAPD")){
                     sInputValue = GetLocalizationData("CAPDLabel");
                  }
              }else if (ContainsText(sObjectName,"RegimenType")){
                  if (CompareText(RegimenType,"APD")){
                     sInputValue = GetLocalizationData("APDLabel");
                  }else if (CompareText(RegimenType,"TIDAL")){
                     sInputValue = GetLocalizationData("TIDAL");
                  }else if (CompareText(RegimenType,"CAPD")){
                     sInputValue = GetLocalizationData("CAPDLabel");
                  }                  
              }else if (ContainsText(sObjectName,"TypicalStatus")){
                  if (!GetTypicalStatus() && CompareText(GetCalculationData("TypicalValues",false),"No")){
                      sAction = "VerifyNotExist"
                  }
              }else if (ContainsText(sObjectName,"TidalPercentage")){
                  if (CompareText(RegimenType,"TIDAL")){
                     sInputValue = GetRegimenData(sInputValue);
                     sInputValue = GetLocalizationData("tidalPercentLabel") + " : " + sInputValue;
                  }else{
                      sAction = "VerifyNotExist";
                  }
              }else if (ContainsText(sObjectName,"TidalFrequency")){
                  if (CompareText(RegimenType,"TIDAL")){
                     sInputValue = GetRegimenData(sInputValue);
                     sInputValue = GetLocalizationData("tidalFullDrainLabel") + " : " + sInputValue;
                  }else{
                      sAction = "VerifyNotExist";
                  }                  
              } 
    
              //Perform the Action
              intStepStatus = DataEntry(sAction,objName,sInputValue,sObjectName,sStepNo);
              VerificationPoint(g_strFuncCall,sStepNo,intStepStatus,intIterCnt);
          }
          
      } 
      g_stepnum = sStepNo; 
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}


//*************************************************************************************************
// Function Name        : UpdateOvernightExchangeFields
// Function Description : To modify the Overnight fields in the CAPD Regimen page based on the values from Calculation File.                    
// Inputs               : N/A     
// Returns              : N/A
//*************************************************************************************************
function UpdateOvernightExchangeFields(){
 
  var sObjectName = null;     //Variable for storing object name  
  var sInputValue = null;
  var sAction = null;
  var sInputValue1 = null;
  var sStepNo = null;           //Variable for storing step number

  var intIniRwNo = g_intStrtRow;        //Variable for storing initial row number
  var intEndRwNo = g_intEndRow;        //Variable for storing end row number  
  var intStepStatus = 1;      //Initialize the status to Fail
  var strData = null;
  var OverNightStatus = false;
     
  try{
      if (GetRegimenData("NightExchangeCount") > 0){
          OverNightStatus = true;    
          oRDRecordSet.MoveLast();  //Move to the last day exchange to process for Overnight
      }
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      
          //Initialize the status to fail for each each step.
          intStepStatus = 1;  //Fail
      
          sObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
          sInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
          sAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
          sInputValue1 = ReadExcel(intIterCnt,c_intInputVal1Col);
          sStepNo = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
          
          if (sObjectName != null){
              objName = BuildWebObj(g_dicColNames_obj.item(sObjectName));  //Build object from the given object name
              if(CompareText(sObjectName,"Regimen_OverNightExchange_CheckBox")){
                  //Select Yes, if the record has Last Fill
                  if (OverNightStatus){
                      sAction = "Checked";
                  }else{
                      sAction = "Unchecked";
                  }
              }else if(ContainsText(sObjectName,"Solution")){
                  sInputValue = GetSolutionValue(sInputValue,sObjectName);
              }else if(ContainsText(sObjectName,"StartTime")){
                  sInputValue = GetRegimenData(sInputValue);
                  sInputValue = ConvertTime(sInputValue, "MINUTE")
                  sInputValue = GetDwellEndTime(sInputValue);
              }
    
              //Perform the Action
              intStepStatus = DataEntry(sAction,objName,sInputValue,sObjectName,sStepNo);
              VerificationPoint(g_strFuncCall,sStepNo,intStepStatus,intIterCnt);
              
              //If OverNight exchnage is not present, then process only the first row to set the checkbox box status.
              if (!OverNightStatus){
                  intIterCnt = intEndRwNo +1;
              }
          }
          
      } 
      g_stepnum = sStepNo; 
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}

//*************************************************************************************************
// Function Name        : VerifyOverNightInputs
// Function Description : To verify the Over Night Inputs in the Regimen Summary based on the values from Calculation File.                    
// Inputs               : N/A     
// Returns              : N/A
//*************************************************************************************************
function VerifyOverNightInputs(){
 
  var sObjectName = null;     //Variable for storing object name  
  var sInputValue = null;
  var sAction = null;
  var sInputValue1 = null;
  var sStepNo = null;           //Variable for storing step number

  var intIniRwNo = g_intStrtRow;        //Variable for storing initial row number
  var intEndRwNo = g_intEndRow;        //Variable for storing end row number  
  var intStepStatus = 1;      //Initialize the status to Fail
  var strData = null;
  
  try{
  
      oRDRecordSet.MoveLast(); //Move to the last record to get the night inputs
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      
          //Initialize the status to fail for each each step.
          intStepStatus = 1;  //Fail
      
          sObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
          sInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
          sAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
          sInputValue1 = ReadExcel(intIterCnt,c_intInputVal1Col);
          sStepNo = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
          
          if (sObjectName != null){
              objName = BuildWebObj(g_dicColNames_obj.item(sObjectName));  //Build object from the given object name
              if(ContainsText(sObjectName,"Cycle")){ 
                  sInputValue = GetRegimenData(sInputValue);
              }else if(ContainsText(sObjectName,"Solution")){
                  sInputValue = GetRegimenData(sInputValue);
              }else if(ContainsText(sObjectName,"Volume")){
                      sInputValue = GetRegimenData(sInputValue);
                      sInputValue = ConvertVolume(sInputValue,"LITRE");
              }else if(ContainsText(sObjectName,"Time")){
                  sInputValue = GetRegimenData(sInputValue);
              }
    
              //Perform the Action
              intStepStatus = DataEntry(sAction,objName,sInputValue,sObjectName,sStepNo);
              VerificationPoint(g_strFuncCall,sStepNo,intStepStatus,intIterCnt);
          }
      } 
      
      g_stepnum = sStepNo; 
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}


//*************************************************************************************************
// Function Name        : VerifyOverNightInputsInList
// Function Description : To verify the Night Inputs in the Regimen List based on the values from Calculation File.                    
// Inputs               : N/A     
// Returns              : N/A
//*************************************************************************************************
function VerifyOverNightInputsInList(){
 
  var sObjectName = null;     //Variable for storing object name  
  var sInputValue = null;
  var sAction = null;
  var sInputValue1 = null;
  var sStepNo = null;           //Variable for storing step number

  var intIniRwNo = g_intStrtRow;        //Variable for storing initial row number
  var intEndRwNo = g_intEndRow;        //Variable for storing end row number  
  var intStepStatus = 1;      //Initialize the status to Fail
  var strData = null;
  
  try{
  
      oRDRecordSet.MoveLast(); //Move to the last record to get the night 
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      
          //Initialize the status to fail for each each step.
          intStepStatus = 1;  //Fail
      
          sObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
          sInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
          sAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
          sInputValue1 = ReadExcel(intIterCnt,c_intInputVal1Col);
          sStepNo = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
          
          if (sObjectName != null){
              objName = BuildWebObj(g_dicColNames_obj.item(sObjectName));  //Build object from the given object name
              if(ContainsText(sObjectName,"Cycle") || ContainsText(sObjectName,"Exchange")){ 
                  sInputValue = GetRegimenData(sInputValue);
              }else if(ContainsText(sObjectName,"Solution") || ContainsText(sObjectName,"Concentration")){
                  sInputValue = GetRegimenData(sInputValue);
              }else if(ContainsText(sObjectName,"Volume")){
                  sInputValue = GetRegimenData(sInputValue);
                  sInputValue = ConvertVolume(sInputValue,"LITRE");
              }else if(ContainsText(sObjectName,"NightDwellTime")){
                  sInputValue = GetRegimenData(sInputValue);
                  sInputValue = ConvertTime(sInputValue,"H:MM");
              }else if(ContainsText(sObjectName,"DryDwellTime")){
                  sInputValue = GetCalculationData(sInputValue,false);
                  sInputValue = ConvertTime(sInputValue,"H:MM");
              }
    
              //Perform the Action
              intStepStatus = DataEntry(sAction,objName,sInputValue,sObjectName,sStepNo);
              VerificationPoint(g_strFuncCall,sStepNo,intStepStatus,intIterCnt);
          }
      } 
      
      g_stepnum = sStepNo; 
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}


//*************************************************************************************************
// Function Name        : VerifyUpdatedDate
// Function Description : To verify the updated date which is based on Server Time                    
// Inputs               : N/A     
// Returns              : N/A
//*************************************************************************************************
function VerifyUpdatedDate(){
 
  var sObjectName = null;     //Variable for storing object name  
  var sInputValue = null;
  var sAction = null;
  var sInputValue1 = null;
  var sStepNo = null;           //Variable for storing step number

  var intIniRwNo = g_intStrtRow;        //Variable for storing initial row number
  var intEndRwNo = g_intEndRow;        //Variable for storing end row number  
  var intStepStatus = 1;      //Initialize the status to Fail
  var strData = null;
  
  try{
  
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      
          //Initialize the status to fail for each each step.
          intStepStatus = 1;  //Fail
      
          sObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
          sInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
          sAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
          sInputValue1 = ReadExcel(intIterCnt,c_intInputVal1Col);
          sStepNo = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
          
          if (sObjectName != null){
              objName = BuildWebObj(g_dicColNames_obj.item(sObjectName));  //Build object from the given object name
              if(ContainsText(sObjectName.toUpperCase(),"UPDATED")){ 
                  //To get the Server date which is UTC
                  var CurrentDate = new Date();
                  var ServerCurrentDate = new Date(CurrentDate.getTime() + CurrentDate.getTimezoneOffset() * 60000); //Conver to UTC
                  //ServerCurrentDate = new Date(ServerCurrentDate.getTime() + 120 * 60000); //Convert to UTC + 2
                  ServerCurrentDate = aqDateTime.SetDateElements(ServerCurrentDate.getFullYear(), ServerCurrentDate.getMonth() + 1, ServerCurrentDate.getDate());
                  sInputValue = aqConvert.DateTimeToFormatStr(ServerCurrentDate,"%d %b %Y");
              }
    
              //Perform the Action
              intStepStatus = DataEntry(sAction,objName,sInputValue,sObjectName,sStepNo);
              VerificationPoint(g_strFuncCall,sStepNo,intStepStatus,intIterCnt);
          }
      } 
      
      g_stepnum = sStepNo; 
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}

/*************************************************************************************************
   Function Name        : VerifyRegimenTimeError
   Function Description : To replace ini file property text for the regimen error message                     
   Inputs               : N/A     
   Returns              : N/A 
  *************************************************************************************************/
function VerifyRegimenTimeError(){
   var sObjectName = null;     //Variable for storing object name  
  var sInputValue = null;
  var sAction = null;
  var sInputValue1 = null;
  var sStepNo = null;           //Variable for storing step number

  var intIniRwNo = g_intStrtRow;        //Variable for storing initial row number
  var intEndRwNo = g_intEndRow;        //Variable for storing end row number  
  var intStepStatus = 1;      //Initialize the status to Fail
  var strData = null;
  
  try{
  
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      
          //Initialize the status to fail for each each step.
          intStepStatus = 1;  //Fail
      
          sObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
          sInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
          sAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
          sInputValue1 = ReadExcel(intIterCnt,c_intInputVal1Col);
          sStepNo = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
          
          if (sObjectName != null){
              objName = BuildWebObj(g_dicColNames_obj.item(sObjectName));  //Build object from the given object name
              sInputValue = GetRunTimeData(sInputValue);//StoreText of total regimen time
              var ErrorString = GetLocalizationData("totalTimeRegimen24HourError");
              sInputValue = ErrorString.replace(Trim("{TIME}"),Trim(sInputValue));
    
              //Perform the Action
              intStepStatus = DataEntry(sAction,objName,sInputValue,sObjectName,sStepNo);
              VerificationPoint(g_strFuncCall,sStepNo,intStepStatus,intIterCnt);
          }
      } 
      
    g_stepnum = sStepNo; 
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}


/*************************************************************************************************
   Function Name        : VerifyVolume
   Function Description : To verify the value converted in Litre                     
   Inputs               : N/A     
   Returns              : String
  *************************************************************************************************/
function VerifyVolume(){
   var sObjectName = null;     //Variable for storing object name  
  var sInputValue = null;
  var sAction = null;
  var sInputValue1 = null;
  var sStepNo = null;           //Variable for storing step number

  var intIniRwNo = g_intStrtRow;        //Variable for storing initial row number
  var intEndRwNo = g_intEndRow;        //Variable for storing end row number  
  var intStepStatus = 1;      //Initialize the status to Fail
  var strData = null;
  
  try{
  
      for (intIterCnt = intIniRwNo ; intIterCnt <= intEndRwNo ; intIterCnt++){
      
          //Initialize the status to fail for each each step.
          intStepStatus = 1;  //Fail
      
          sObjectName = ReadExcel(intIterCnt,c_intObjNameCol);  //Reads the object name from c_intObjNameCol
          sInputValue = ReadExcel(intIterCnt,c_intInputValCol);  //Reads the input value from c_intInputValCol
          sAction = ReadExcel(intIterCnt,c_intActionCol);  //Reads the action from c_intActionCol
          sInputValue1 = ReadExcel(intIterCnt,c_intInputVal1Col);
          sStepNo = ReadExcel(intIterCnt,c_intStepCol);  //Reads the step from c_intStepCol
          
          if (sObjectName != null){
              objName = BuildWebObj(g_dicColNames_obj.item(sObjectName));  //Build object from the given object name
              sInputValue = GetRunTimeData(sInputValue);//StoreText of total regimen time
              sInputValue=ConvertVolume(sInputValue,"LITRE");     
              //Perform the Action
              intStepStatus = DataEntry(sAction,objName,sInputValue,sObjectName,sStepNo);
              VerificationPoint(g_strFuncCall,sStepNo,intStepStatus,intIterCnt);
          }
      } 
      
    g_stepnum = sStepNo; 
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}
