//USEUNIT GlobalVariables

//*************************************************************************************************
// Function Name        : DataEntry
// Function Description : To enter data into different web controls
// Inputs               : action->Specifies the action to be performed on the object
//                      : ObjectProperty->Array of test data required to perform any action
//                      : strData->Input to perform the action on the object
//                      : ObjectName->Name of the object on which action has to be performed
//                      : StepNumber->Specifies the step number in the keyword
// Returns              : intStepCount (returns 0 or 1)
//************************************************************************************************
function DataEntry(action, ObjectProperty, strData, ObjectName, StepNumber){
  try{
    
    //Initialize the required variables
    var intStepCount = 0; //Initializing to PASS
    var i = 0;
    var strLogMessage = "";
    
    var objDesc = null;
    ExpectedMessage = action+" shall be performed on "+ObjectName;
    ActualMessage = action+" is not performed on "+ObjectName;
    
    //Read External data only for Adequest
    if (IsAdequest){
        g_OtherInputValue = ReadExcel(intIterCnt,c_intInputVal1Col);
        strData = GetExternalData(g_OtherInputValue, strData);
        //To click on Extend button when time out alert notice is displayed
        ExtendSession();
    }
    
    if (!ContainsText(action,"Exist")){
        //Wait for the Object and evalutate it
        objDesc = WaitForObject(ObjectProperty);
    
         //If the Object does not exist, then log the error and exit the function
         if (!IsExists(objDesc)){
           ActualMessage = "'" + ObjectName + "' Object does not exist so failed to perform " + action;
           intStepCount = 1; //Fail
           Log.Error (StepNumber+" -"+ActualMessage) ;
           return intStepCount;
         }
    }else{//action contains the "Exist"              
        objDesc = eval(ObjectProperty);//evalutate the object       
    }
    
    //Scroll the object to view
    ScrollIntoView(objDesc);
   
      //Perform a Switch to execute the Action
      action = trim(action);
      switch(action){
        case "NavigateBack" :
          objDesc.Keys ("[BS]");
          strLogMessage = StepNumber +" - "+ObjectName+" is navigated back";
          Log.Message(strLogMessage);
          break;
        case "Enter" :      
          objDesc.SetText(strData);
          strLogMessage = StepNumber +" - "+strData+" is "+action+"ed in "+ObjectName+"";
          ActualMessage=strData+" is entered in the "+ObjectName;
          ExpectedMessage=strData+" shall be entered in the "+ObjectName;
          if (objDesc.Type=="password"){
            ActualMessage="Password is entered in the "+ObjectName;
            ExpectedMessage="Password shall be entered in the "+ObjectName;
          }        
          Log.Message(strLogMessage);
          break;  
        case "ReplaceDate":
          var objvalue=objDesc.Value
          var strFirst = objvalue.toString();  //Variable to convert the file path to string type
          var date1= aqConvert.DateTimeToFormatStr(aqDateTime.Today(),"%Y");
          var strsplit1 = strFirst.split(" ");
          strFirst= strsplit1[0]+" "+strsplit1[1]+" "+strsplit1[2]
          var fileText1=aqString.Replace(objvalue,strFirst,strData);
          var fileText1=aqString.Replace(fileText1,strsplit1[5],date1);
          strData =fileText1;
          objDesc.SetText(strData);
          strLogMessage = StepNumber +" - "+strData+" is entered in "+ObjectName+"";
          ActualMessage=strData+" is entered in the "+ObjectName;
          ExpectedMessage=strData+" shall be entered in the "+ObjectName;
          Log.Message(strLogMessage);
          break;
        case "Wait" : 
          Delay(strData)         
          break; 
         case "EnterKeyVal" :   
          objDesc.Click()
          objDesc.Click()      
          objDesc.SetText(strData);
          ActualMessage=strData+" is entered in the "+ObjectName;
          ExpectedMessage=strData+" shall be entered in the "+ObjectName;
          strLogMessage = StepNumber +" - "+strData+" is entered in "+ObjectName+"";
          Log.Message(strLogMessage);
          break;      
        case "KeyInData":
          objDesc.DblClick();
          if (GetText(objDesc).length > 0 ){//If there is any value in the field, then clear it.
            objDesc.Keys("^a") //Ctrl + a
            objDesc.Keys ("[Del]"); //Press Delete
          }
          objDesc.Keys(strData);
          ActualMessage = "'" + strData + "' is entered in "+ObjectName;
          ExpectedMessage = "'" + strData + "' shall be entered in "+ObjectName;
          if (objDesc.Type=="password"){
            ActualMessage="Password is entered in the "+ObjectName;
            ExpectedMessage="Password shall be entered in the "+ObjectName;
          }        
          strLogMessage = StepNumber +" - "+ActualMessage;
          Log.Message(strLogMessage);
          if(ObjectName=="FileName"){
            for(var x=0;x<=3;x++){ 
              if(objDesc.wText!=strData){ 
                Delay(3000);
                objDesc.Refresh();
                objDesc.Keys(strData); 
              }else{ 
                x=5; 
              } 
            }
          }
          break;
        case "KeyinValue" : 
          objDesc.Keys (strData);
          objDesc.Keys ("[Enter]");
          strLogMessage = StepNumber +" - "+strData+" is entered in "+ObjectName+"";
          ActualMessage=strData+" is entered in the "+ObjectName;
          ExpectedMessage=strData+" shall be entered in the "+ObjectName;
          break;
        case "EnterValue" : 
          objDesc.Keys (strData);
          ActualMessage=strData+" is entered in the "+ObjectName;
          ExpectedMessage=strData+" shall be entered in the "+ObjectName;
          break;
        case "Close": 
          objDesc.Close();
          ActualMessage=ObjectName+" is  closed the current tab"
          ExpectedMessage=ObjectName+" shall be closed the current tab";
          break;
        case "ClickButton":        
          objDesc.ClickButton();
          strLogMessage = StepNumber +" - "+ObjectName+" is Clicked";
          ActualMessage=ObjectName+" is clicked"
          ExpectedMessage=ObjectName+" shall be Clicked";
          Log.Message(strLogMessage);
          break;  
        case "Click":        
          objDesc.Click();
          Wait(1);
          WaitForPageSync(); //To Wait for the page to load completely.
          WaitForLoadingSpinner();
          strLogMessage = StepNumber +" - "+ObjectName+" is Clicked";
          ActualMessage=ObjectName+" is clicked"
          ExpectedMessage=ObjectName+" shall be Clicked";
          Log.Message(strLogMessage);
          break;  
	      case "RightClick":        
          objDesc.ClickR();
          strLogMessage = StepNumber +" - "+ObjectName+" is Right Clicked";
          ActualMessage=ObjectName+" is Right clicked"
          ExpectedMessage=ObjectName+" shall be Right Clicked";
          Log.Message(strLogMessage);
          break;
        case "ScrollAndClick": 
          ScrollIntoView(objDesc);       
          objDesc.Click();
          WaitForPageSync(); //To Wait for the page to load completely.
          strLogMessage = StepNumber +" - "+ObjectName+" is Clicked";
           ActualMessage=ObjectName+" is clicked"
          ExpectedMessage=ObjectName+" shall be Clicked";
          Log.Message(strLogMessage);
          break;    
        case "DblClick":
          objDesc.DblClick;
          strLogMessage = StepNumber +" - "+ObjectName+" is double clicked";
           ActualMessage=ObjectName+" is clicked"
          ExpectedMessage=ObjectName+" shall be Clicked";
          Log.Message(strLogMessage);
          break;
        case "Select":
          intStepCount = 1;
          ActualMessage = strData+" is not selected in "+ObjectName;
          ExpectedMessage = strData+" shall be selected in "+ObjectName;        
          WaitForPageSync();
          objDesc.ClickItem(strData);
          WaitForPageSync();
          if(ObjectName == "SqlSelect_ConnectionName"){
            if (CompareText(strData,objDesc.wText)){
              ActualMessage = strData+" is selected in "+ObjectName;
              intStepCount = 0;
              Log.Message(StepNumber +" - "+ActualMessage);
            }else{
              Log.Error(StepNumber +" - "+ActualMessage);
            }
          }else{
            if (CompareText(strData,GetText(objDesc))){
              ActualMessage = strData+" is selected in "+ObjectName;
              intStepCount = 0;
              Log.Message(StepNumber +" - "+ActualMessage);
            }else{
              Log.Error(StepNumber +" - "+ActualMessage);
            }            
          }
          break;
        case "Value":
          objDesc.Value;
          strLogMessage = StepNumber +" - "+ObjectName+" is "+action+"ed";
          Log.Message(strLogMessage);
          break;
        case "SelectFirstItem":
          objDesc.ClickItem(1);
          strLogMessage = StepNumber +" - "+ObjectName+" is selected ";
          ActualMessage=ObjectName+" is selected"
          ExpectedMessage=ObjectName+" shall be selected";
          Log.Message(strLogMessage);
          break;  
        case "VerifyContentText":
              intStepCount = VerifyTextContains(StepNumber,ObjectName,objDesc,strData);
              break;
        case "VerifyContentTextIgnoreCase":
              ScrollIntoView(objDesc); 
              intStepCount = VerifyTextContainsIgnorecase(StepNumber,ObjectName,objDesc,strData);
              break;
        case "VerifyTextNotContains":
              intStepCount = VerifyTextNotContains(StepNumber,ObjectName,objDesc,strData);
              break;              
         case "Verifypatientname":
          ExpectedMessage=strData+" shall be matched with the text present in the Screen";
          if (aqString.Compare(trim(strData),trim(objDesc.ContentText), false) == 0){
            searchpatient =0;          
            strLogMessage = StepNumber +" - "+ObjectName+" value is verified as "+strData+"";
             ActualMessage=strData+" is matched with the "+objDesc.ContentText;
            Log.Message(strLogMessage);
          } 
          else{
            searchpatient =1;
            strLogMessage = StepNumber +" - "+ObjectName+" value is expected to be ["+strData+"] and actual is ["+objDesc.ContentText+"]";
            ActualMessage=strData+" is not matched with the "+objDesc.ContentText;
            Log.Error(strLogMessage);
          }    
          break; 
         case "VerifyClassNameDisable":
            ExpectedMessage=ObjectName+" shall be disabled";
            var objData = objDesc.className;
            if(aqString.Find(trim(objData),(trim(strData)))!=-1){
              strLogMessage = StepNumber +" - "+ObjectName+" is disabled";
              Log.Message(strLogMessage);   
              ActualMessage=ObjectName+" is disabled";         
            }   
            else{
              intStepCount = 1; //Fail
              strLogMessage = StepNumber +" - "+ObjectName+" is not disabled";
              Log.Error(strLogMessage);
              ActualMessage=ObjectName+" is not disabled";
            } 
            break;  
        case "VerifyText":
              intStepCount = VerifyTextEquals(StepNumber,ObjectName,objDesc,strData);
              break;        
        case "Verifywtext":
          ExpectedMessage = "The value for "+ObjectName+" shall be matched with "+strData;
          if(Trim(strData) == Trim(objDesc.wText)){            
            strLogMessage = StepNumber +" - "+ObjectName+" value is matched with "+strData+"";
            ActualMessage="The value for "+ObjectName+" is matched with "+strData;             
            Log.Message(strLogMessage);
          }   
          else{
           intStepCount = 1; //Fail
            strLogMessage = StepNumber +" - "+ObjectName+" is expected to be ["+strData+"] and actual is ["+objDesc.wText+"]";
            ActualMessage="The value for "+ObjectName+" is not matched with "+strData;
            Log.Error(strLogMessage);
          }
          break;
        case "Verifyexist":
        case "VerifyExist":
          ExpectedMessage=ObjectName+" shall exist on the Screen";
          ActualMessage=ObjectName+" exists on the Screen";         
          if(IsExists(objDesc)){
          	strLogMessage = StepNumber +" - "+ActualMessage;
          	Log.Message(strLogMessage);
		      }else{
            intStepCount = 1; //Fail
            ActualMessage=ObjectName+" does not exist on the screen";
            strLogMessage = StepNumber +" - "+ActualMessage;
            Log.Error(strLogMessage);
          }
          break;
        case "VerifyInVisible":
           ExpectedMessage=ObjectName+" shall not be visible on the Page";
          if(objDesc.Visible){
            intStepCount = 1; //Fail
            strLogMessage = StepNumber +" - "+ObjectName+" exists on the webpage";
            ActualMessage=ObjectName+" is visible on the Page";
            Log.Error(strLogMessage);
          }   
          else{           
            strLogMessage = StepNumber +" - "+ObjectName+" does not exist on the webpage";
            ActualMessage=ObjectName+" is not visible on the Page";
            Log.Message(strLogMessage);
          }  
          break;         
        case "VerifyNotEditable":
          ExpectedMessage=ObjectName+" shall be uneditable";
          if(objDesc.isContentEditable == false){            
            strLogMessage = StepNumber +" - "+ObjectName+" is not editable";
            ActualMessage=ObjectName+" is uneditable";
            Log.Message(strLogMessage);
          }  
          else{
            intStepCount = 1; //Fail
            strLogMessage = StepNumber +" - "+ObjectName+" is editable";            
            ActualMessage=ObjectName+" is editable";
            Log.Error(strLogMessage);
          }   
          break;
        case "VerifyEditable":
          ExpectedMessage=ObjectName+" shall be editable";
          if(objDesc.isContentEditable){            
            strLogMessage = StepNumber +" - "+ObjectName+" is editable";
            ActualMessage=ObjectName+" is editable";
            Log.Message(strLogMessage);
          }  
          else{
            intStepCount = 1; //Fail
            strLogMessage = StepNumber +" - "+ObjectName+" is not editable";
            ActualMessage=ObjectName+" is not editable";
            Log.Error(strLogMessage);             
          }   
          break;
        case "VerifyVisible":
          ExpectedMessage=ObjectName+" shall be visible on the application";
          if(objDesc.Visible){  
            ActualMessage=ObjectName+" is visible on the application";          
            strLogMessage = StepNumber +" - "+ActualMessage;
            Log.Message(strLogMessage);
           
          }  
          else{
            intStepCount = 1; //Fail
            ActualMessage=ObjectName+" is not visible on the application";
            strLogMessage = StepNumber +" - "+ActualMessage;            
            Log.Error(strLogMessage);
          }
          break;
        case "Verifyenable":
          ExpectedMessage=ObjectName+" shall be enabled";
          if(objDesc.Enabled){
            strLogMessage = StepNumber +" - "+ObjectName+" is enabled";
            Log.Message(strLogMessage);   
            ActualMessage=ObjectName+" is enabled";         
          }   
          else{
            intStepCount = 1; //Fail
            ActualMessage=ObjectName+" is not enabled";
            strLogMessage = StepNumber +" - "+ActualMessage;
            Log.Error(strLogMessage);
            
          } 
          break;           
        case "Verifydisable":
          ExpectedMessage=ObjectName+" shall be disabled";
          if(objDesc.disabled){            
            strLogMessage = StepNumber +" - "+ObjectName+" is disabled";
            Log.Message(strLogMessage);
            ActualMessage=ObjectName+" is disabled";  
          }  
          else{
            intStepCount = 1; //Fail
            ActualMessage=ObjectName+" is enabled";
            strLogMessage = StepNumber +" - "+ActualMessage;
            Log.Error(strLogMessage);
          }   
          break;
        case "VerifyChecked":
          ExpectedMessage=ObjectName+" shall be Checked";
          if(objDesc.checked == true){
             strLogMessage = StepNumber +" - "+ObjectName+" is Checked" ;
             Log.Message(strLogMessage);
             ActualMessage=ObjectName+" is Checked";
          }             
          else{
           intStepCount = 1; //Fail
		   ActualMessage=ObjectName+" is not Checked";
           strLogMessage = StepNumber +" - "+ActualMessage;
           Log.Error(strLogMessage);
          }   
          break;

        case "ItemList":
          ExpectedMessage= "The listbox options of " +ObjectName+" shall be "+strData;
          if(Trim(strData) == Trim(objDesc.wItemList)){
            strLogMessage = StepNumber +" - "+ObjectName+" listbox options are "+strData+"";
            ActualMessage= "The listbox options of " +ObjectName+" is "+strData;
            Log.Message(strLogMessage);            
          }   
          else{
            intStepCount = 1; //Fail
            ActualMessage= "The listbox options of " +ObjectName+" is not "+strData;
            strLogMessage = StepNumber +" - "+ObjectName+" listbox options are expected to be ["+strData+"] but actual is ["+objDesc.wItemList+"]";
            Log.Error(strLogMessage);
          }  
          break;    
        case "VerifyItemNotExist":
        objDesc = eval(ObjectProperty);
        ScrollIntoView(objDesc);        
        ExpectedMessage="The option "+ strData + " shall not be available in "+ObjectName;
          if(aqString.Find(trim(objDesc.wItemList),trim(strData)) == -1){
            strLogMessage = StepNumber +" - The option "+ strData + " is not available in "+ObjectName;
            Log.Message(strLogMessage);      
            ActualMessage="The option "+ strData + " is not available in "+ObjectName;      
          }   
          else{
            intStepCount = intStepCount+ 1;
            ActualMessage="The option "+ strData + " is available in "+ObjectName;
            strLogMessage = StepNumber +" - The option "+ strData + " is available in "+ObjectName;
            Log.Error(strLogMessage);
          }  
          break;
        case "VerifyClassName":
          ExpectedMessage= "The classname of " +ObjectName+" shall be "+strData;
          if(Trim(strData) == Trim(objDesc.className)){
            strLogMessage = StepNumber +" - The class name of "+ObjectName+" shall be "+strData;
            ActualMessage= "The classname of " +ObjectName+" is "+strData;
            Log.Message(strLogMessage);            
          }   
          else{
            intStepCount = 1; //Fail
            ActualMessage= "The classname of " +ObjectName+" is not "+strData;
            strLogMessage = StepNumber +" - The class name of "+ObjectName+" shall be "+strData+" but actual is "+ objDesc.className;
            Log.Error(strLogMessage);
          }  
          break;          
        case "ContentTextIsEmpty":
          if(objDesc.ContentText.length == 0 ){
            strLogMessage = StepNumber +" - "+ObjectName+" is empty";
            Log.Message(strLogMessage);            
          }  
          else{
            intStepCount = 1; //Fail
            strLogMessage = StepNumber +" - "+ObjectName+" is not empty";
            Log.Error(strLogMessage);
          }   
          break;         
        case "TextIsEmpty":
          ExpectedMessage= ObjectName+" field shall be empty";
          if((GetText(objDesc)).length == 0 ){
            ActualMessage= ObjectName+" field is empty";
            strLogMessage = StepNumber +" - "+ObjectName+" is empty";
            Log.Message(strLogMessage);
          }
          else{
            intStepCount = 1; //Fail
            ActualMessage= ObjectName+" field is not empty";
            strLogMessage = StepNumber +" - "+ObjectName+" is not empty";
            Log.Error(strLogMessage);
          }
          break;
        case "wTextIsEmpty":
          if(objDesc.wText.length == 0){
            strLogMessage = StepNumber +" - "+ObjectName+" is empty";
            Log.Message(strLogMessage);            
          }  
          else{
            intStepCount = 1; //Fail
            strLogMessage = StepNumber +" - "+ObjectName+" is not empty";
            Log.Error(strLogMessage);
          } 
          break;          
        case "VerifyRadioSelected":
          ExpectedMessage = ObjectName+" radiobutton shall be selected";
          if(objDesc.wChecked){
            ActualMessage = ObjectName+" radiobutton is selected";
            strLogMessage = StepNumber +" - "+ObjectName+" radio button is selected";
            Log.Message(strLogMessage);            
          }  
          else{
            intStepCount = 1; //Fail
            ActualMessage = ObjectName+" radiobutton is not selected";
            strLogMessage = StepNumber +" - "+ActualMessage;
            Log.Error(strLogMessage);
          }
          break;
        case "VerifyRadioUnSelected":
          if(!objDesc.wChecked){
            strLogMessage = StepNumber +" - "+ObjectName+" radio button is not selected";
            Log.Message(strLogMessage);          
          }
          else{ 
            intStepCount = 1; //Fail           
            strLogMessage = StepNumber +" - "+ObjectName+" radio button is selected";
            Log.Error(strLogMessage);
            
          }   
          break;      
        case "VerifyType":
        ExpectedMessage=ObjectName+" type shall be verified as "+strData;
          if(Trim(strData) == Trim(objDesc.Type)){
            strLogMessage = StepNumber +" - "+ObjectName+" type is verified as "+strData;
            Log.Message(strLogMessage);    
            ActualMessage=ObjectName+" type is verified as "+strData;        
          }
          else{
           
            intStepCount = 1; //Fail
            ActualMessage=ObjectName+" type is not verified as "+strData;
            strLogMessage = StepNumber +" - "+ActualMessage;
            Log.Error(strLogMessage);
          } 
          break;        
        case "Checked":
          ExpectedMessage=ObjectName+" shall be checked";
          if(!objDesc.checked){
            objDesc.click();
           }
           if(objDesc.checked){
            strLogMessage = StepNumber +" - "+ObjectName+" is Checked"
            Log.Message (strLogMessage);
            ActualMessage=ObjectName+" is checked";
          }else{
            intStepCount = 1; //Fail
            ActualMessage=ObjectName+" is not checked";
            strLogMessage = StepNumber +" - "+ObjectName+" is not Checked";
            Log.Error(strLogMessage);
            
          } 
          break;          
        case "Unchecked":
          ExpectedMessage=ObjectName+" shall be unchecked";
          if(objDesc.checked){
            objDesc.click();
          }
          if(!objDesc.checked){
            strLogMessage = StepNumber +" - "+ObjectName+" is Unchecked" ;
             Log.Message(strLogMessage);
             ActualMessage=ObjectName+" is unchecked";
          }else{
            intStepCount = 1; //Fail
            ActualMessage=ObjectName+" is checked";
           strLogMessage = StepNumber +" - "+ObjectName+" is Checked";
           Log.Error(strLogMessage);
           
          }   
          break;    
        case "StringCompare": 
          var strouterhtml = objDesc.outerHTML
          var strfind =  aqString.Remove(strouterhtml, 0, 203);
          var strval =   aqString.Remove(strfind, 6, 13)
          if (trim(strData) == trim(strval)){
           strLogMessage = StepNumber +" - "+ObjectName+" value is verified as "+strData+"";
           Log.Message(strLogMessage);
           }
           else{
           intStepCount = 1; //Fail
            strLogMessage = StepNumber +" - "+ObjectName+" is expected to be ["+strData+"] and actual is ["+strval+"]";
            Log.Error(strLogMessage);
           }
           break;
        case "VerifyInnerHTML":   
           var Res = aqString.Find(objDesc.innerHTML,"red-cross-error" );
           if ( Res != -1 ){  
             strLogMessage = StepNumber +" Inner html contains red cross error";
             Log.Message(strLogMessage);         
           }
           else{
            intStepCount = 1; //Fail
            strLogMessage = StepNumber +" Inner html does not contains red cross error";
            Log.Error(strLogMessage);
           }
           break;
        case "DeleteKey" :
        case "MakeEmpty":        
          objDesc.Keys ("[Home]");
          objDesc.Keys ("![End]");
          objDesc.Keys ("[Del]");
          ExpectedMessage=ObjectName+" shall be cleared and made empty.";
          ActualMessage=ObjectName+" is cleared and empty.";
          if (GetText(objDesc).length > 0 ){//If there is any value in the field, then mark as fail.
              ActualMessage=ObjectName+" is not cleared.";
              intStepCount = 1; //Fail
          }
          Log.Message(StepNumber + " - " + ActualMessage);
          break;
        case "VerifyMaxLength":
          if(Trim(objDesc.maxLength) == Trim(strData)){
            strLogMessage = StepNumber +" - Maximum length of "+ObjectName+" is verified as ["+strData+"]";
            Log.Message(strLogMessage);
          }else{
            strLogMessage = StepNumber +" - Maximum length of "+ObjectName+" is expected to be ["+strData+"] and actual is ["+objDesc.maxLength+"]";
          }
          break;		  
          case "SelectFlag":
          ScrollIntoView(objDesc);
          objDesc.Click()
          objDesc.FindChild("contentText",strData,1000).scrollIntoView()
          objDesc.FindChild("contentText",strData,1000).Click()
          ExpectedMessage=strData+" shall be selected in the "+ObjectName+" dropdown";
          if(aqString.Find(objDesc.FindChild("className","selected-flag",1000).title,strData) != -1){
            strLogMessage = StepNumber +" - "+strData+" is selected in the "+ObjectName+" dropdown";
            ActualMessage=strData+" is selected in the "+ObjectName+" dropdown";            
          }
          else{
            intStepCount = intStepCount+ 1; 
            strLogMessage = StepNumber +" - "+strData+" is not selected in the "+ObjectName+" dropdown";
            ActualMessage=strData+" is not selected in the "+ObjectName+" dropdown"; 
          }
          Log.Message(strLogMessage);
          break;
          case "CtrlClick":
            ExpectedMessage=ObjectName+" shall be Clicked";
            objDesc.Click (-1,-1,skCtrl);
            strLogMessage = StepNumber + " - " + ObjectName + " is Clicked";
    				Log.Message(strLogMessage);
            ActualMessage=ObjectName+" is clicked"
            break;

          case "VerifyObjectLabel":
    				ExpectedMessage=ObjectName+" value shall be matched with " + strData ;
            if (aqString.Compare(trim(strData), trim(objDesc.ObjectLabel), false) == 0) {
    					strLogMessage = StepNumber + " - " + ObjectName + " value is verified as " + strData + "";
    					Log.Message(strLogMessage);
              ActualMessage=ObjectName+" value is matched with " + strData ;
    				} else {
    					intStepCount = intStepCount + 1;
    					strLogMessage = StepNumber + " - " + ObjectName + " value is expected to be [" + strData + "] and actual is [" + objDesc.ObjectLabel + "]";
    					Log.Message(strLogMessage);
              ActualMessage=ObjectName+" value is not matched with " + strData ;
    				}
    				break;
          case "VerifyToday":
      				if (Trim(objDesc.ObjectType) == "Cell"){
      				  TargetDate = aqConvert.DateTimeToFormatStr(aqConvert.StrToDate(objDesc.contentText), "%Y-%m-%d");
      				} else {
      				  TargetDate = aqConvert.StrToDate(objDesc.wMinDate);
      				}
      				Today = aqConvert.DateTimeToFormatStr(aqDateTime.Today(), "%Y-%m-%d");
              ExpectedMessage=ObjectName+" shall be verified as " + Today ;
      						if (aqDateTime.Compare(Today, TargetDate) == 0) {
      							strLogMessage = StepNumber + " - " + ObjectName + " date is verified as " + Today + "";
      							Log.Message(strLogMessage);
                    ActualMessage=ObjectName+" is verified as " + Today ;
      						} else {
      							strLogMessage = StepNumber + " - " + ObjectName + " is expected to be " + Today + " and actual is " + TargetDate + "";
      							Log.Message(strLogMessage);
                    ActualMessage=ObjectName+" is not verified as " + Today ;
      							intStepCount = intStepCount + 1;
  						}
  						break;
            case "VerifyUnChecked":
            ExpectedMessage=ObjectName+" shall be Unchecked";
        				if(!objDesc.checked){
        					strLogMessage = StepNumber + " - " + ObjectName + " is Unchecked"
        					Log.Message(strLogMessage);
                  ActualMessage=ObjectName+" is Unchecked";
        				} else {
                  intStepCount = 1;
        					strLogMessage = StepNumber + " - " + ObjectName + " is Checked";
        					Log.Message(strLogMessage);
                  ActualMessage=ObjectName+" is Checked";
        				}
        				break;
                    
            case "VerifyObjectType":
              ExpectedMessage=ObjectName+" shall be verified as " + strData ;
        				if (Trim(strData) == Trim(objDesc.ObjectType)) {
        					strLogMessage = StepNumber + " - " + ObjectName + " type is verified as " + strData + "";
        					Log.Message(strLogMessage);
                  ActualMessage=ObjectName+" is verified as " + strData ;
        				} else {
        					strLogMessage = StepNumber + " - " + ObjectName + " is expected to be " + strData + " and actual is " + objDesc.ObjectType + "";
        					Log.Message(strLogMessage);
        					intStepCount = intStepCount + 1;
                  ActualMessage=ObjectName+" is not verified as " + strData ;
        				}
        				break;
                
            case "VerifyRequired":
            ExpectedMessage=ObjectName+" shall be labeled as Required" ;
              if (Trim(objDesc.className) == "mandatorylabel") {
      					strLogMessage = StepNumber + " - " + ObjectName + " is Required";
      					Log.Message(strLogMessage);
               ActualMessage=ObjectName+" is labeled as Required"; 
      				} else {
                intStepCount = intStepCount + 1;
      					strLogMessage = StepNumber + " - " + ObjectName + " is expected to be Required";
      				  Log.Message(strLogMessage);
                ActualMessage=ObjectName+" is not labeled as Required";
              }
      				break;
               
            case "VerifyNonRequired":
              ExpectedMessage=ObjectName+" shall be labeled as Non-Required" ;
              if (Trim(objDesc.className) != "mandatorylabel") {
      					strLogMessage = StepNumber + " - " + ObjectName + " is Non-Required";
      					Log.Message(strLogMessage);
                ActualMessage=ObjectName+" is labeled as Non-Required"; 
      				} else {
                intStepCount = intStepCount + 1;
      					strLogMessage = StepNumber + " - " + ObjectName + " is expected to be Non-Required";
      				  Log.Message(strLogMessage);
                ActualMessage=ObjectName+" is labeled as Required"; 
              }
      				break;
              case "Remove":
        				ExpectedMessage=ObjectName+" shall be removed" ;
                objDesc.value = "";
        				strLogMessage = StepNumber + " - " + ObjectName + " is made empty";
        				Log.Message(strLogMessage);
                ActualMessage=ObjectName+" is removed" ;
      				break;
          case "VerifyNotExist":        
          case "VerifyNotexist":
            	  ExpectedMessage=ObjectName+" shall not exist on the Screen";
            	  ActualMessage=ObjectName+" does not exists on the Screen";          
            	  if(IsExists(objDesc)){
            		intStepCount = 1; //Fail
            		ActualMessage=ObjectName+" exists on the screen";
            		strLogMessage = StepNumber +" - "+ActualMessage;
            		Log.Error(strLogMessage);
            	  }else{
            		strLogMessage = StepNumber +" - "+ActualMessage;
            		Log.Message(strLogMessage);
            	  }          
	            break; 
                 
         case "VerifyTheDayOneYearLater":
              YearLater = aqConvert.DateTimeToFormatStr(aqDateTime.AddDays(aqDateTime.AddMonths(aqDateTime.Today(), 12), -1), "%Y-%m-%d");
      				if (Trim(objDesc.ObjectType) == "Cell"){
      				  TargetDate = aqConvert.DateTimeToFormatStr(aqConvert.StrToDate(objDesc.contentText), "%Y-%m-%d");
      				} else {
      				  TargetDate = aqConvert.DateTimeToFormatStr(aqConvert.StrToDate(objDesc.Text), "%Y-%m-%d");
      				}
  				//if (aqString.Compare(trim(aqConvert.DateTimeToStr(YearLater)), trim(aqConvert.DateTimeToStr(TargetDate)), false) == 0) {
  						ExpectedMessage=ObjectName+" shall be verified as " + YearLater ;
              if (aqDateTime.Compare(YearLater, TargetDate) == 0) {
  					strLogMessage = StepNumber + " - " + ObjectName + " date is verified as " + YearLater + "";
  							Log.Message(strLogMessage);
                ActualMessage=ObjectName+" is verified as " + YearLater ;
  						} else {
  							strLogMessage = StepNumber + " - " + ObjectName + " is expected to be " + YearLater + " and actual is " + TargetDate + "";
  							Log.Message(strLogMessage);
  							intStepCount = intStepCount + 1;
                ActualMessage=ObjectName+" not verified as " + YearLater ;
  						}
  					break;
                    
            case "ChangeBrowserTab":
              //Mock the keyboard input
              objDesc.Keys("^2");
              ExpectedMessage="Control shall be switched to another browser tab";
              ActualMessage="Control is switched to another browser tab";
              break;
                      
          case "CloseCurrentTab":
            //Mock the keyboard input
            objDesc.Keys("^w");
            ExpectedMessage="Current tab shall be closed";
            ActualMessage="Current tab is closed";
            break;
                 
          case "ClickDayLnk":
            var i ;
            // Specify the sought-for property names 
            var PropArray = new Array ("ObjectType", "contentText");
            // Specify the sought-for property values
            var ValuesArray = new Array ("Link", Trim(strData));
            var links = objDesc.FindAllChildren(PropArray,ValuesArray,2);
            links = (new VBArray(links)).toArray();
            for (i = 0; i < links.length; i++){
              Log.Message(links[i].FullName);
              links[i].Click();
            }
    				strLogMessage = StepNumber + " - " + ObjectName + " is Clicked";
    				Log.Message(strLogMessage);
            ExpectedMessage= ObjectName + "shall be clicked";
            ActualMessage= ObjectName + "is clicked";
            break;
      
//regular expression on dd Month yyyy
            case "ValidateDateMatchPattern":
              var compareText = objDesc.contentText;
              aqString.ListSeparator = ":";
              var dateText = aqString.GetListItem(compareText, 1);
              var re1='((?:(?:[0-2]?\\d{1})|(?:[3][01]{1})))(?![\\d])';	// Day 
              var re2='(\\s+)';	// White Space 1
              var re3='((?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Sept|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?))';	// Month 
              var re4='(\\s+)';	// White Space 2
              var re5='((?:(?:[1]{1}\\d{1}\\d{1}\\d{1})|(?:[2]{1}\\d{3})))(?![\\d])';	// Year 

              var p = new RegExp(re1+re2+re3+re4+re5,["i"]);
              var m = p.exec(dateText);
              ExpectedMessage=strdata + "shall be properly matched with the Date";  
              if(m!= null){
                strLogMessage = StepNumber + " - " + strData + " properly match the Date";
      					Log.Message(strLogMessage);
                ActualMessage=strdata + "is properly matched with the Date";
              }else{
                intStepCount = intStepCount + 1;
                strLogMessage = StepNumber + " - " + strData + " is not match!";
      					Log.Message(strLogMessage);
                ActualMessage=strdata + "is not matched with the Date";
              }
              break;
                   
            case "VerifySolutionProduct":
              ChildObj = objDesc.FindAllChildren("ObjectType", "Select", 500); //Find all the child objects of the type Link
              ChildObj = new VBArray(ChildObj).toArray();

              aqString.ListSeparator = ";";
              var strTargetProduct = Trim(strData);
              // true means have product parameter in the drop down, false means didn't find product parameter in the drop down
              var flag = true;

              for (var i = 0; (i < aqString.GetListLength(strTargetProduct)) && flag; i++){
                var productValue = aqString.GetListItem(strTargetProduct, i)
              	for (var j = 0; j < aqString.GetListLength(ChildObj[3-i].wItemList); j++){
                  ChildObj[3-i].ClickItem("Select");
              		var selectValue = aqString.GetListItem(ChildObj[3-i].wItemList, j)
              		if (aqString.Compare(productValue, selectValue, true) == 0) {
                    ChildObj[3-i].ClickItem(selectValue);
      	        		flag = true;
      	        		break;
              		}
      	        	flag = false;
              	}
              }
              ExpectedMessage= "Product shall be exist";  
              if (flag){
                intStepCount = intStepCount + 1;
      	        strLogMessage = StepNumber + " - Exsisted product";
      					Log.Message(strLogMessage);
                ActualMessage= "Product exists"
              } else {
      	        strLogMessage = StepNumber + " - Didn't find product";
      	        Log.Message(strLogMessage);
                ActualMessage="Product does not exist";
              }
              break;
                      
              case "ModifyMaxLength":
                objDesc.setAttribute("maxLength",Trim(strData),1);
                ExpectedMessage= "Maximum length of " + ObjectName + " shall be verified as [" + strData + "]";
                if (Trim(objDesc.maxLength) == Trim(strData)) {
        					strLogMessage = StepNumber + " - Maximum length of " + ObjectName + " is verified as [" + strData + "]";
        					Log.Message(strLogMessage);
                  ActualMessage= "Maximum length of " + ObjectName + " is verified as [" + strData + "]";
        				} else {
                  intStepCount = intStepCount + 1;
        					strLogMessage = StepNumber + " - Maximum length of " + ObjectName + " is expected to be [" + strData + "] and actual is [" + objDesc.maxLength + "]";
        				  Log.Message(strLogMessage);
                  ActualMessage= "Maximum length of " + ObjectName + " is not verified as [" + strData + "]";
                }
        				break;
                        
                case "VerifyObjectIdentifier":
                  ExpectedMessage= ObjectName + " value shall be verified as " + strData + "";
          				if (Trim(strData) == Trim(objDesc.ObjectIdentifier)) {
          					strLogMessage = StepNumber + " - " + ObjectName + " value is verified as " + strData + "";
          					Log.Message(strLogMessage);
                    ActualMessage= ObjectName + " value is verified as " + strData + "";
          				} else {
          					intStepCount = intStepCount + 1;
          					strLogMessage = StepNumber + " - " + ObjectName + " is expected to be [" + strData + "] and actual is [" + objDesc.ObjectIdentifier + "]";
          					Log.Message(strLogMessage);
                    ActualMessage= ObjectName + " value is not verified as " + strData + "";
          				}
          				break;
                          
                  case "VerifyVisibleOnscreen":
                    ExpectedMessage= ObjectName + " shall be visible on the webpage";
            				if (objDesc.VisibleOnScreen) {
            					strLogMessage = StepNumber + " - " + ObjectName + " is visible on the webpage";
            					Log.Message(strLogMessage);
                      ActualMessage= ObjectName + " is visible on the webpage";
            				} else {
            					intStepCount = intStepCount + 1;
            					strLogMessage = StepNumber + " - " + ObjectName + " is not visible on the webpage";
            					Log.Message(strLogMessage);
                      ActualMessage= ObjectName + " is not visible on the webpage";
            				}
            				break;
            			case "VerifyInVisibleOnscreen":
                    ExpectedMessage= ObjectName + " shall be hidden on the webpage";
            				if (objDesc.VisibleOnScreen == false) {
            					strLogMessage = StepNumber + " - " + ObjectName + " is hidden on the webpage";
            					Log.Message(strLogMessage);
                      ActualMessage= ObjectName + " is hidden on the webpage";
            				} else {
            					intStepCount = intStepCount + 1;
            					strLogMessage = StepNumber + " - " + ObjectName + " is visible on the webpage";
            					Log.Message(strLogMessage);
                      ActualMessage= ObjectName + " is visible on the webpage";
            				}
            				break;
                    	case "VerifyTop":
                        ExpectedMessage=ObjectName + " value shall be verified as " + strData ;
                				if (Trim(strData) == Trim(objDesc.Top)) {
                					strLogMessage = StepNumber + " - " + ObjectName + " value is verified as " + strData + ",The step is passed";
                					Log.Message(strLogMessage);
                          ActualMessage=ObjectName + " value is verified as " + strData ;
                				} else {
                					intStepCount = intStepCount + 1;
                					strLogMessage = StepNumber + " - " + ObjectName + " is expected to be [" + strData + "] and actual is [" + objDesc.Top + "],The step is failed";
                					Log.Message(strLogMessage);
                          ActualMessage=ObjectName + " value is not verified as " + strData ;
                				}
                				break;
                    case "Verifyenable_Style":
                      ExpectedMessage=ObjectName + " shall be enabled";
              				if (aqString.Find(trim(objDesc.outerHTML), "ui-state-disabled", 0, false) = -1) {
              					strLogMessage = StepNumber + " - " + ObjectName + " style is enabled";
              					Log.Message(strLogMessage);
                        ActualMessage=ObjectName + " is enabled" ;
              				} else {
              					intStepCount = intStepCount + 1;
              					strLogMessage = StepNumber + " - " + ObjectName + " style is not enabled";
              					Log.Message(strLogMessage);
                        ActualMessage=ObjectName + " is disabled" ;
              				}
              				break;

            			case "Verifydisable_Style":
                    ExpectedMessage=ObjectName + " shall be disabled";
            				if (aqString.Find(trim(objDesc.outerHTML), "ui-state-disabled", 0, false) != -1) {
            					strLogMessage = StepNumber + " - " + ObjectName + " style is disabled";
            					Log.Message(strLogMessage);
                      ActualMessage=ObjectName + " is disabled" ;
            				} else {
            					intStepCount = intStepCount + 1;
            					strLogMessage = StepNumber + " - " + ObjectName + " style is not disabled";
            					Log.Message(strLogMessage);
                      ActualMessage=ObjectName + " is enabled" ;
            				}
            				break;

            			case "VerifyOuterText":
                    ExpectedMessage=ObjectName + " value shall be verified as " + strData ;
            				if (aqString.Compare(trim(strData), trim(objDesc.OuterText), false) == 0) {
            					strLogMessage = StepNumber + " - " + ObjectName + " value is verified as " + strData + "";
            					Log.Message(strLogMessage);
                      ActualMessage= ObjectName + " value is verified as " + strData ;
            				} else {
            					intStepCount = intStepCount + 1;
            					strLogMessage = StepNumber + " - " + ObjectName + " value is expected to be [" + strData + "] and actual is [" + objDesc.OuterText + "]";
            					Log.Message(strLogMessage);
                      ActualMessage= ObjectName + " value is not verified as " + strData ;
            				}
            				break;

            			case "ItemListContain":
                    ExpectedMessage= ObjectName + " listbox options shall contain " + strData ;
            				if (aqString.Find(";" + trim(objDesc.wItemList) + ";", ";" + trim(strData) + ";", 0, false) != -1) {
            					strLogMessage = StepNumber + " - " + ObjectName + " listbox options contains " + strData + "";
            					Log.Message(strLogMessage);
                      ActualMessage= ObjectName + " listbox options contains " + strData ;
            				} else {
            					intStepCount = intStepCount + 1;
            					strLogMessage = StepNumber + " - " + ObjectName + " listbox options does not contain [" + strData + "] and actual is [" + objDesc.wItemList + "]";
            					Log.Message(strLogMessage);
                      ActualMessage= ObjectName + " listbox options does not contain " + strData ;
            				}
            				break;

            			case "ContentTextContain":
                    ExpectedMessage= ObjectName + " ContentText shall contain " + strData ;
            				if (aqString.Find(trim(objDesc.ContentText), trim(strData), 0, false) != -1) {
            					strLogMessage = StepNumber + " - " + ObjectName + " ContentText contains " + strData + "";
            					Log.Message(strLogMessage);
                      ActualMessage= ObjectName + " ContentText contains " + strData ;
            				} else {
            					intStepCount = intStepCount + 1;
            					strLogMessage = StepNumber + " - " + ObjectName + " ContentText does not contain [" + strData + "] and actual is [" + objDesc.ContentText + "]";
            					Log.Message(strLogMessage);
                      ActualMessage= ObjectName + " ContentText does not contain " + strData ;
            				}
            				break;
                    case "VerifyMandatoryIndicator":
                     ExpectedMessage= ObjectName + " shall exist with Mandatory Indicator * ";
            				if (aqString.Find(trim(objDesc.outerHTML), "mandatory", 0, false) != -1) {
            					strLogMessage = StepNumber + " - " + ObjectName + " exists with Mandatory Indicator *";
            					Log.Message(strLogMessage);
                      ActualMessage= ObjectName + " exists with Mandatory Indicator * ";
            				} else {
            					intStepCount = intStepCount + 1;
            					strLogMessage = StepNumber + " - " + ObjectName + " does not exist with Mandatory Indicator *";
            					Log.Message(strLogMessage);
                      ActualMessage= ObjectName + " does not exist with Mandatory Indicator * ";
            				}
            				break;
            			case "VerifyCloseCross":
                    ExpectedMessage= ObjectName + " shall exist as a close cross";
            				if (aqString.Find(trim(objDesc.outerHTML), "ui-icon-closethick", 0, false) != -1) {
            					strLogMessage = StepNumber + " - " + ObjectName + " exists as a close cross";
            					Log.Message(strLogMessage);
                      ActualMessage= ObjectName + " exists as a close cross";
            				} else {
            					intStepCount = intStepCount + 1;
            					strLogMessage = StepNumber + " - " + ObjectName + " does not exist as a close cross";
            					Log.Message(strLogMessage);
                      ActualMessage= ObjectName + " does not exist as a close cross";
            				}
            				break;
                    	case "VerifyDate": //Date Format: DD Month YYYY
            				strDate = aqConvert.DateTimeToFormatStr(strData, "%d %B %Y");
            				ExpectedMessage= ObjectName + " shall be verified as " + strDate ;
                    if (Trim(strDate) == Trim(objDesc.contentText)) {
            					strLogMessage = StepNumber + " - " + ObjectName + " date is verified as " + strDate + "";
            					Log.Message(strLogMessage);
                      ActualMessage= ObjectName + " is verified as " + strDate ;
            				} else {
            					intStepCount = intStepCount + 1;
            					strLogMessage = StepNumber + " - " + ObjectName + " date is expected to be [" + strDate + "] and actual is [" + objDesc.contentText + "]";
            					Log.Message(strLogMessage);
                      ActualMessage= ObjectName + " is not verified as " + strDate ;
            				}
            				break;
        
                    case "Input":
            				objDesc.Click();
            				objDesc.Keys("^a");
            				objDesc.Keys("[Del]");				
            				objDesc.Keys(strData);
            				strLogMessage = StepNumber + " - " + strData + " is " + action + "ed in " + ObjectName + "";
            				Log.Message(strLogMessage);
                    ExpectedMessage= strData + " shall " + action + "ed in " + ObjectName + "";
                    ActualMessage= strData + " is " + action + "ed in " + ObjectName + "";
            				break;
            			case "MakeBlank":
            				objDesc.Click()
            				objDesc.SetText("");
            				strLogMessage = StepNumber + " - " + ObjectName + " is made blank";
            				Log.Message(strLogMessage);
                    ExpectedMessage= ObjectName + " shall be made blank";
                    ActualMessage= ObjectName + " is made blank";
            				break;		  
        case "StoreText":
          ExpectedMessage = "Text of " + ObjectName + " shall be stored in the variable '" + strData + "'";
          intStepCount = StoreRunTimeData(strData, GetText(objDesc));
          break;
        case "StoreURL":
          ExpectedMessage = "Current URL shall be stored in the variable '" + strData + "'";
          intStepCount = StoreRunTimeData(strData, GetCurrentURL());
          break;  
        case "ScrollObject":
          ExpectedMessage = "Object shall be scrolled into view";
          ActualMessage = "Object is not scrolled into view";
          if (ScrollIntoView(objDesc)){
               ActualMessage = "Object is scrolled into view";
               intStepCount = 0;
          }
          break;
        case "GetExternalData":
          var sKey = ReadExcel(intIterCnt,c_intObjNameCol); //Read the Key from objectName.
          var sValue = GetExternalData(g_OtherInputValue, sKey);
          WriteExcel(intIniRwNo,c_intInputValCol,g_TestDataSheet,sValue); //Write the value to ObjectName column
          break; 
        case "IsErrorField":
          ExpectedMessage = "Error shall be displayed for " + ObjectName;
          ActualMessage = "Error is displayed for " + ObjectName;
          var AttributeValue = GetAttribute(objDesc, "class");
          if((ContainsText(AttributeValue,"error"))||(CompareText(AttributeValue,"out-of-max-range"))){
            strLogMessage = StepNumber +" - "+ ActualMessage;
            Log.Message(strLogMessage);
          }else{
            intStepCount = 1; //Fail
            ActualMessage = "Error is not displayed for " + ObjectName;
            strLogMessage = StepNumber +" - "+ActualMessage; 
            Log.Error(strLogMessage);            
          }         
          break;
        case "IsNotErrorAndWarning":
          ExpectedMessage = "Error shall be not displayed for " + ObjectName;
          ActualMessage = "Error is not displayed for " + ObjectName;
          var AttributeValue = GetAttribute(objDesc, "class"); 
          if((ContainsText(AttributeValue,"error"))||CompareText(AttributeValue,"outside-recommended-range")){
            intStepCount = 1; //Fail
            ActualMessage = "Error is displayed for " + ObjectName;
            strLogMessage = StepNumber +" - "+ActualMessage; 
            Log.Error(strLogMessage);
          }else{
            strLogMessage = StepNumber +" - "+ ActualMessage;
            Log.Message(strLogMessage);
          }                         
          break;
         case "PressKey":
            PressKey(objDesc,ObjectName,strData);// press key based on input value in specified object
            strLogMessage = StepNumber +" - "+ ActualMessage;
            Log.Message(strLogMessage);                              
            break;
         case "VerifyUnchecked":
            Expectedmessage=ObjectName+" shall be Unchecked";
            if(!objDesc.checked){
               strLogMessage = StepNumber +" - "+ObjectName+" is Unchecked" ;
               Log.Message(strLogMessage);
               ActualMessage=ObjectName+" is Unchecked";
          }else{
             intStepCount = 1; //Fail
  		       ActualMessage=ObjectName+" is Checked";
             strLogMessage = StepNumber +" - "+ActualMessage;
             Log.Error(strLogMessage);
            }   
            break; 
          case "PDASelectDate":
            intStepCount = Runner.CallMethod("PDAUtil.PDASelectDate", ObjectName, strData); //Using Runner.CallMethod to avoid cross script reference error
            strLogMessage = StepNumber +" - "+ActualMessage;
            if (intStepCount == 0){
              Log.Message(strLogMessage);  
            }else{
              Log.Error(strLogMessage);
            }
            break;
          case "VerifyReadOnly":
            Expectedmessage=ObjectName+" shall be Read only";
            if(objDesc.readOnly){
               strLogMessage = StepNumber +" - "+ObjectName+" is Read only" ;
               Log.Message(strLogMessage);
               ActualMessage=ObjectName+" is Read only";
            }else{
               intStepCount = 1; //Fail
    		        ActualMessage=ObjectName+" is not Read only";
               strLogMessage = StepNumber +" - "+ActualMessage;
               Log.Error(strLogMessage);
            }   
            break;
          case "VerifyDropDownValue"://To verify the availability of drop down value in the list.
              ExpectedMessage = ObjectName+" shall have the Drop Down value '"+strData+"'";
              ActualMessage=ObjectName+" does not have the Drop Down value '"+strData+"'"; 
              var ListValueFlag=VerifyDropDownValue(objDesc,strData);
              if(ListValueFlag){
                ActualMessage=ObjectName+" does have the Drop Down value '"+strData+"'";
                strLogMessage = StepNumber +" - "+ActualMessage;
                Log.Message(strLogMessage);
              }else{
                intStepCount = 1; //Fail    		        
                strLogMessage = StepNumber +" - "+ActualMessage;
                Log.Error(strLogMessage);
              }
              break;
        case "VerifyDropDownValuesCount"://To verify the count of drop down values in the list.
              ExpectedMessage = ObjectName+" shall have the Drop Down value count'"+strData+"'";
              ActualMessage=ObjectName+" does not have the Drop Down value count'"+strData+"'"; 
              var ListCount= objDesc.wItemCount
              if(ListCount == strData){
                ActualMessage=ObjectName+" does have the Drop Down value count'"+strData+"'";
                strLogMessage = StepNumber +" - "+ActualMessage;
                Log.Message(strLogMessage);
              }else{
                intStepCount = 1; //Fail    		        
                strLogMessage = StepNumber +" - "+ActualMessage;
                Log.Error(strLogMessage);
              }
              break;              
        case "VerifyWarning":                        
          intStepCount=VerifyWarningMessage(StepNumber,ObjectName,strData);                  
          break;        
        case "VerifyMandatoryField":
          intStepCount=Runner.CallMethod("PDAUtil.VerifyMandatoryOptionalField",StepNumber,ObjectName,objDesc,"mandatory");       
          break;
        case "VerifyOptionalField":
          intStepCount=Runner.CallMethod("PDAUtil.VerifyMandatoryOptionalField",StepNumber,ObjectName,objDesc,"optional");         
          break;
        case "KeyInDataTabOut":
          objDesc.DblClick();
          if (GetText(objDesc).length > 0 ){//If there is any value in the field, then clear it.
            objDesc.Keys("^a") //Ctrl + a
            objDesc.Keys("[P999]") //Pause for 999 second
            objDesc.Keys ("[Del]"); //Press Delete
          }
          objDesc.Keys(strData);
          objDesc.Keys ("[Tab]"); //Press Tab
          ActualMessage = "'"+strData+"' is entered in "+ObjectName;
          ExpectedMessage = "'"+strData+"' shall be entered in "+ObjectName;
          if (objDesc.Type=="password"){
            ActualMessage="Password is entered in the "+ObjectName;
            ExpectedMessage="Password shall be entered in the "+ObjectName;
          }        
          strLogMessage = StepNumber +" - "+ActualMessage;
          Log.Message(strLogMessage);          
          break;      
        case "VerifyCalculatedValues":
              var ApplicationText = GetText(objDesc);
              intStepCount = VerifyCalculatedValues(StepNumber,ObjectName,ApplicationText,strData);
              break; 
        case "VerifyNumericDifference":
              var ApplicationText = GetText(objDesc);
              intStepCount = VerifyNumericDifference(StepNumber,ObjectName,ApplicationText,strData);
              break;                 
        case "VerifyDropDownValueNotPresent"://To verify the availability of drop down value in the list.
              ExpectedMessage = ObjectName+" shall not have the Drop Down value '"+strData+"'";
              ActualMessage=ObjectName+" does have the Drop Down value '"+strData+"'"; 
              var ListValueFlag=VerifyDropDownValue(objDesc,strData);
              if(!(ListValueFlag)){
                ActualMessage=ObjectName+" does not have the Drop Down value '"+strData+"'";
                strLogMessage = StepNumber +" - "+ActualMessage;
                Log.Message(strLogMessage);
              }else{
                intStepCount = 1; //Fail    		        
                strLogMessage = StepNumber +" - "+ActualMessage;
                Log.Error(strLogMessage);
              }
              break;  
        
        case "VerifyPlaceHolder":
          ExpectedMessage = "Place holder of '" + ObjectName + "' shall be " + strData
          ActualMessage = "Place holder of '" + ObjectName + "' is not " + strData
          var AttributeValue = GetAttribute(objDesc, "placeholder");
          if (CompareText(strData,AttributeValue)){
            ActualMessage = "Place holder of '" + ObjectName + "' is " + strData
            strLogMessage = StepNumber +" - "+ActualMessage; 
            Log.Message(strLogMessage);
          }else{            
            intStepCount = 1; //Fail            
            strLogMessage = StepNumber +" - "+ActualMessage; 
            Log.Error(strLogMessage);
          }         
          break;
        case "VerifyConvertedValue":         
            strConveretdData = Runner.CallMethod("PDAUtil.ConvertToPDAFormat", strData, ":", ObjectName);
            intStepCount = VerifyTextEquals(StepNumber,ObjectName,objDesc,strConveretdData);
            break;  
        default:
          intStepCount = 1; //Fail  
          Log.Error("Action '" + action + "' was not performed since it is invalid");
          break;

     }//Select Case Ends

  }catch(e){
     intStepCount = 1;
     Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
  return intStepCount;
}

//*************************************************************************************************
// Function Name         : GetBrowserName
// Function Description  : To get the TestComplete specific browser name
// Inputs                : g_strBrowser ->Browser Name
// Returns               : TestComplete specific browser name
//************************************************************************************************
function GetBrowserName(BrowserName){
     var BrowserProcessName = "Invalid";
try{
     BrowserName = Trim(BrowserName).toUpperCase();
       switch (BrowserName){
         case "IE":
           BrowserProcessName = "iexplore";
           break;
         case "CHROME":
           BrowserProcessName = "chrome";
           break;
         case "FIREFOX":
           BrowserProcessName = "firefox";
           break;
         case "SAFARI":
           BrowserProcessName = "btSafari";
           break;
         case "OPERA":
           BrowserProcessName = "btOpera";
           break;
         case "EDGE":
           BrowserProcessName = "edge";
           break;
         default :
           BrowserProcessName = "Invalid"
           break;
       }
  
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
  
  if (BrowserProcessName == "Invalid"){
        throw{//throw exception if browser name is invalid
          name: "Invalid Browser Name",
          message: "Browser Name '"+BrowserName+"' is invalid."
        }
  }
  return BrowserProcessName;
}
//*************************************************************************************************
// Function Name        : FormatURL
// Function Description : To format URL of the web application
// Inputs               : strUrl->URL of the web application
// Returns              : Formatted URL
//************************************************************************************************
function FormatURL(strUrl){
  try{
     //Variable Declaration
     var arrUrl;
     var strNewUrl; 
     var strNewUrl  = "";
     intcounter = 0;
     arrUrl = strUrl.split("/");
     for (var intCnt in arrUrl) {
     strNewUrl = arrUrl[intCnt]+"//";
      break;
     }
    return strNewUrl + "*";
  }
 catch(e){
   Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }
}
////*************************************************************************************************
//// Function Name        : Dictionary
//// Function Description : To get the data from Excel using Excel object
//// Inputs               : 
////                        
//// Returns              : Entire content of excel
////************************************************************************************************
function Dictionary(){
  try{
    var objExcel;  //Variable to store excel object 
    var arrExcelData = new Array();
    var sObjectPath = g_sProjPath+"Stores\\Files\\Object.xlsx" ;  //object sheet File Path
    strExcelSheet = "object";  //Variable to store excell sheet name
    var ObjectKey = "";
    var ObjectValue = "";
    
    //open the specified file
    var objWorkbook = g_objXLApp.Workbooks.Open(sObjectPath);  //Create object for excel workbook
    var objWorksheet = objWorkbook.Worksheets(strExcelSheet);  //Create object for excel worksheet
    var intRowCnt = objWorksheet.UsedRange.Rows.Count; //Get row count 
    for (var intRow = 1; intRow <= intRowCnt; intRow++){
    ObjectKey = objWorksheet.Cells(intRow, 1).Value;
    ObjectValue = objWorksheet.Cells(intRow, 2).Value;
    if( !IsNullorUndefined(ObjectKey) && !IsNullorUndefined(ObjectValue) ){  //((ObjectKey)&&(ObjectValue))!=null
      if(!g_dicColNames_obj.Exists(ObjectKey)){
        g_dicColNames_obj.Add(ObjectKey, ObjectValue);   
      }else{
        g_dicColNames_obj.Item(ObjectKey) = ObjectValue;
        Log.Error("Duplicate object with name "+ObjectKey+" found.")
      }
    } 
    } 
    objWorkbook.Close();
 }
 catch(e){
   Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }
}

//*************************************************************************************************
// Function Name        : BuildWebObj
// Function Description : To concatenate parent object with object repository//s child object
// Inputs               : ChildObjectProperties->array of child objects to be constucted
// Returns              : Newly constructed web objects
//************************************************************************************************
function BuildWebObj(ChildObjectProperties){
  try{
      var OutputValue = null;
      var ChildObject = null;
      g_IsLastObjectWeb = true;
      var PageProperty = null;
      
      //If the ObjectProperty is invalid, just return it. So exception is raised while evaluating it.
      if (IsNullorUndefined(ChildObjectProperties)){
        return ChildObjectProperties;
      } 
      
      //Set the Browser Identification Properties if it was not set.
      var BrowserName =  GetBrowserName(g_strBrowserName); //Get Test Complete specific Browser name
      ChildObjectProperties = Trim(ChildObjectProperties);
      if (IsNullorUndefined(g_PageObject)){
          g_PageObject = "Sys.Browser("+chr(34)+BrowserName+chr(34)+").Page("+chr(34)+g_strFormattedUrl+chr(34)+")";
          oPageObject = eval(g_PageObject);
      }
      PageProperty = g_PageObject;
      //Wait for page to load
      WaitForPageSync();

      if (aqString.Find(g_strObjectName,"&&")!= -1){
        OutputValue = BuildFindChild(PageProperty,g_strObjectName);
        Log.Warning("Check for this condition");
      }else if (aqString.Find(ChildObjectProperties,"&&")!= -1){
        //Object Identification using FindChild (Descriptive Programming)
        OutputValue = BuildFindChild(PageProperty,ChildObjectProperties);
      }else if (aqString.Find(ChildObjectProperties.toUpperCase(),"CSS:")!= -1){
        //Object Identification using QuerySelector (CSS Selectors)           
        ChildObjectProperties = ChildObjectProperties.substr(4,ChildObjectProperties.length);
        ChildObject = aqString.Format("QuerySelector(%s)",aqString.Quote(ChildObjectProperties));
        ChildObject = "QuerySelector(" + aqString.Quote(ChildObjectProperties) + ")";
        OutputValue = PageProperty+"."+ChildObject;
      }else if (aqString.Find(ChildObjectProperties.toUpperCase(),"XPATH:")!= -1){
        //Object Identification using QuerySelector (XPATH)           
        ChildObjectProperties = ChildObjectProperties.substr(6,ChildObjectProperties.length);
        ChildObject = aqString.Format("FindChildByXPath(%s,true)",aqString.Quote(ChildObjectProperties)); 
        OutputValue = PageProperty+"."+ChildObject; 
      }else{
        ChildObject = ChildObjectProperties;
        OutputValue = PageProperty+"."+ChildObject;
      }
      
      return OutputValue;
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}

//*******************************************************************************************************************************************
// Name: ReadObjData
// Description: 	To read config data from text file
// Inputs:	   strFilePath -> Path of the file
// Output:	 Dictionary object containing child items with appropriate keys
//*******************************************************************************************************************************************

function ReadConfigData(strFilePath){
  try{    
    var ForReading = 1;
    var objfs;
    var objf;    
    //Creates a new file object
    objfs = Sys.OleObject("Scripting.FileSystemObject");
    if(aqFileSystem.Exists(strFilePath)){
      objf = objfs.OpenTextFile(strFilePath, ForReading)
      while(! objf.AtEndOfStream){
        var strText =  objf.ReadLine();
        var strFileLine = Trim(strText)           
        var strObj1 = strFileLine.split("=") //Split the line using =
        var strKey = Trim(strObj1[0])
        var strVal = Trim(strObj1[1])
        g_dicParams.Add(strKey,strVal);
      }      
    }
     objf.Close()
     objf = null;
     objfs = null;
     

        
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 } 
}

  
//*******************************************************************************************************************************************
// Function Name        : GetValueFromDictionary
// Function Description : Retrieves the value in the key stored in the passed Dictionary object.
// Inputs               : oDictionaryObject -> Dictionary object where the key is stored.
// Returns              : Value of the key in Dictionary object
//*******************************************************************************************************************************************
function GetValueFromDictionary(oDictionaryObject, sKey){
  var sValue = "NULL";
  var ParameterValue = "";
  try{
       if(oDictionaryObject.Exists(sKey)){
          ParameterValue = oDictionaryObject.item(sKey);
          ParameterValue = Trim(ParameterValue);
        }
   }catch(e){
     Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
   }  
   return ParameterValue;
}
  
//*******************************************************************************************************************************************
// Function Name        : GetDicParam
// Function Description : Retrieves the config value stored in g_dicParams
// Inputs               : ParameterName -> Config Name whose value needs to be searched.
// Returns              : Item of the key in Dictionary object
//*******************************************************************************************************************************************
function GetDicParam(ParameterName){
  var ParameterValue = "";
  try{
     ParameterValue = GetValueFromDictionary(g_dicParams,ParameterName);    
   }catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
   }  
   return ParameterValue;
}

//*******************************************************************************************************************************************
// Function Name        : GetRunTimeData
// Function Description : Retrieves the value of the run time variable stored in g_RunTimeData
// Inputs               : ParameterName -> Variable Name whose value needs to be searched.
// Returns              : Item of the key in Dictionary object
//*******************************************************************************************************************************************
function GetRunTimeData(sKey){
  var ParameterValue = "";
  try{
     ParameterValue = GetValueFromDictionary(g_RunTimeData,sKey);    
   }catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
   }  
   return ParameterValue;
}


//*************************************************************************************************
// Function Name        : ReadExcel
// Function Description : To read excel file
// Inputs               : strFileName -> Excel path ,sheetname ,column number
// Returns              : data read from the given cell
//************************************************************************************************
function ReadExcel(intRow,intCol){
  try{  
    var data;
    g_objXLSht1 = g_objXLBook.Worksheets(g_TestDataSheet);
    data = g_objXLSht1.Cells(intRow,intCol).Value;
    return trim(data);
  }
 catch(e){
   Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }
}

//*************************************************************************************************
// Function Name        : WriteExcel
// Function Description : To write data to excel file
// Inputs               : strFileName -> Row,Column,Sheet name,data to be written
// Returns              : None
//************************************************************************************************
function WriteExcel(intRow,intCol,SheetName,g_strCellData){  
  try{
    //open the specified file 
    var g_objXLSht1 = g_objXLBook.Sheets(SheetName);                                                        
    g_objXLSht1.Cells(intRow,intCol).Value = g_strCellData;
    g_objXLBook.Save()    
  }
 catch(e){
   Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }
}

//*************************************************************************************************
// Function Name        : VerifyObject
// Function Description : To verify whether the object exists
// Inputs               : Object
// Returns              : None
//************************************************************************************************
function VerifyObject(Object){
  try{
    var BuildObj = eval(Object);
    if(!BuildObj.Exists){
      StopCurrentTest();
      } 
  }
 catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 }
}

//*************************************************************************************************
// Function Name        : KillExcel
// Function Description : To terminate Excel process
// Inputs               : None
// Returns              : None
//************************************************************************************************
function KillExcel(){
  try{
  var ExcelProcess = Sys.WaitProcess("Excel",0);
  if(ExcelProcess.Exists){
    ExcelProcess.Close();
    ExcelProcess = Sys.WaitProcess("Excel",0);
    if(ExcelProcess.Exists){
       ExcelProcess.Terminate();
    }    
  }
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }     
}

//*************************************************************************************************
// Function Name        : CloseTestComplete
// Function Description : To terminate Testcomplete process
// Inputs               : None
// Returns              : None
//************************************************************************************************
function CloseTestComplete(){
  try{
    var TestCompleteProcess = Sys.Process("TestComplete");
    var TestExecuteProcess = Sys.Process("TestExecute");  
    if(TestCompleteProcess.Exists){
       TestCompleteProcess.Terminate();
    }
    if(TestExecuteProcess.Exists){
       TestExecuteProcess.Terminate();
    } 
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }    
}
//*************************************************************************************************
// Function Name        : getbrowser_OSversion
// Function Description : To get the current browser name and OS version
// Inputs               : None
// Returns              : None
//************************************************************************************************

function getbrowser_OSversion(){  
  try{
    var TestExecutionEngine = Sys.WaitProcess("TestComplete",1000);    
    if(TestExecutionEngine.Exists){  
      g_TCVersion = "TestComplete "+TestExecutionEngine.FileVersionInfo;  // Test complete version
    }else{
      TestExecutionEngine = Sys.WaitProcess("TestExecute",1000);
      g_TCVersion = "TestExecute "+TestExecutionEngine.FileVersionInfo;  // Test Execute version
    }        
    g_OSInfo = Sys.OSInfo.FullName
    g_browserinfo = Browsers.Item(g_strBrowser).Description
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}

//*************************************************************************************************
// Function Name        : ScrollIntoView
// Function Description : Scroll the page to the object location                 
// Inputs               : Object      
// Returns              : None  
//************************************************************************************************
function ScrollIntoView(objDesc){
     var ReturnValue = false;
 try{
     if (IsExists(objDesc)){
           var objFullName = objDesc.FullName;
           //Perform the scroll into view only for Web Objects.
           if (ContainsText(objFullName,"Sys.Browser")){
                try{
                    objDesc.scrollIntoView();
                    g_IsLastObjectWeb = true; 
                    ReturnValue = true; 
                }catch(Ignore){
                    g_IsLastObjectWeb = false; //If the exception is thrown, then it is not a web Object. 
                } 
           }
      }
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
  return ReturnValue;
}


//*************************************************************************************************
// Function Name        : CloseAllBrowsers
// Function Description : This function closes all the opened browsers  
// Inputs               : None
// Returns              : None 
//************************************************************************************************
function CloseAllBrowsers(){
  try{  
    //Get all the browser windows which are currently open in the desktop
    var AllOpenTabs = null;
    CloseProcess("iexplore");   
    //Handling for IE to exempt QC
    var AllOpenBrowsers = Sys.FindAllChildren("ObjectType","Browser",1);
    AllOpenBrowsers = (new VBArray(AllOpenBrowsers)).toArray();  
    //Iterate each browser Window
    for (BrowserIndex = 0; BrowserIndex < AllOpenBrowsers.length; BrowserIndex++){    
          //Find all tabs (pages) in the current window
          AllOpenTabs = AllOpenBrowsers[BrowserIndex].FindAllChildren("ObjectType","Page",2);
          AllOpenTabs = (new VBArray(AllOpenTabs)).toArray();          
          //Iterate tab and close it one by one if it is not QC or WebEx       
          for (TabIndex = 0; TabIndex < AllOpenTabs.length; TabIndex++){
            if (AllOpenTabs[TabIndex].Exists && AllOpenTabs[TabIndex].Name.indexOf("UIPage") == -1){         
              AllOpenTabs[TabIndex].Close();//Close each tab one by one
              ConfirmWindow(AllOpenTabs[TabIndex], true);//If Confirm message is displayed then close it.              
            }
          }//Loop for all Tabs in the current browser window 
         CloseProcess("iexplore");
         CloseProcess("chrome");
         CloseProcess("firefox");
         CloseProcess("Edge") ;
         Wait(3);  //Wait for all process / tabs / windows to close  
    }//Loop for All Browser Windows      
  }catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest); 
  }
}

//*************************************************************************************************
// Function Name        : BuildFindChild
// Function Description : This function is to find the child object 
// Inputs               : Parent Object, Object Name
// Returns              : Object 
//************************************************************************************************
function BuildFindChild(ParentProperty,ChildProperties){
     
    //Initialize the variables to initial value
    var CombinedObject = null;
    var PropertyName = "new Array(";
    var PropertyValue = "new Array(";
    
  try{
     
     //Split the properties and loop through them
     var Properties = ChildProperties.split("&&")
      for (i=0; i<Properties.length; i=i+2){
         PropertyName = PropertyName + aqString.Quote(Properties[i]) + " ,";
         PropertyValue = PropertyValue + aqString.Quote(Properties[i+1]) + " ,";
      }
    
    //Truncate the extra comma
    PropertyName = PropertyName.substr(0,PropertyName.length-1);
    PropertyValue = PropertyValue.substr(0,PropertyValue.length-1);
    
    //Add the close brackets for the array
    PropertyName = Trim(PropertyName) + ")";
    PropertyValue = Trim(PropertyValue) + ")"; 
    
    //Construct the Child Property with hard coded depth of 2000
    var ChildProperty = "FindChild("+PropertyName+","+PropertyValue+",2000)";
    CombinedObject = ParentProperty + "." + ChildProperty;
  }catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
  return CombinedObject;
}

//*************************************************************************************************
// Function Name        : CloseAdobeAcrobat
// Function Description : To terminate Adobe Acrobat process if its running
// Inputs               : None
// Returns              : None
//************************************************************************************************
function CloseAdobeAcrobat(){
  try{
  //Close Acrobat process if it is running
    var AcrobatProcess = Sys.Process("AcroRd32");  
    if(AcrobatProcess.Exists){
       AcrobatProcess.Terminate();
    }    
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }      
}

//*************************************************************************************************
// Function Name        : CloseCommandPrompt
// Function Description : To terminate CommandPrompt process if its running
// Inputs               : None
// Returns              : None
//************************************************************************************************
function CloseCommandPrompt(){
  try{
   //Close CMD process if it is running
   var CMDProcess = Sys.Process("cmd");  
    if(CMDProcess.Exists){
       CMDProcess.Terminate();
    }   
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }      
}

//*************************************************************************************************
// Function Name        : ParseRecSetToArray
// Function Description : Parse RecSet to an array
// Inputs               : RecSet
// Returns              : recordArray
//************************************************************************************************

function ParseRecSetToArray(RecSet) {
	try {
		var recordArray = new Array();
		recordArray[0] = new Array();
		for (var i = 0; i < RecSet.Fields.Count; i++) {
			recordArray[0][i] = RecSet.Fields.Item(i).Name;
		}

		if(!RecSet.BOF && !RecSet.EOF){
		RecSet.MoveFirst();
		var intRowIndex = 1;
		while (!RecSet.EOF) {
			recordArray[intRowIndex] = new Array();
			for (i = 0; i < RecSet.Fields.Count; i++) {
				recordArray[intRowIndex][i] = RecSet.Fields.Item(i).Value;
			}
			RecSet.MoveNext();
			intRowIndex = intRowIndex + 1;
		}
		}
		return recordArray;
	} catch (e) {
		Log.Error("ParseRecSetToArray:" + e.description);
	}
}

//*************************************************************************************************
// Function Name        : GetCompareResult
// Function Description : To get compare result bwtween Compare Value Column and DB result 
// Inputs               : Two array
// Returns              : intStepCounter
//************************************************************************************************

function GetCompareResult(recordArray, compareArray) {
	//  var resultArray = new Array();
	var arr1Len = recordArray.length;
	var arr2Len = compareArray.length;
	var maxRowNum = recordArray.length;
	var minRowNum = compareArray.length;
	var columnArray = new Array();
	var intStepCounter = 0
		var i = 0;
	// resultArray[0] = recordArray[0];
	//resultArray[0] = columnArr;
	//  resultArray[1] = new Array();
	columnArray[0] = recordArray[0];
	var maxColNum = columnArray[0].length;

	if (arr1Len < arr2Len) {
		maxRowNum = arr2Len;
		minRowNum = arr1Len;
	}

	for (var j = 0; j < maxColNum; j++) {
		if (recordArray[0][j] == compareArray[0]) {
			for (var i = 1; i < maxRowNum; i++) {
				if (i >= minRowNum) {
					intStepCounter = intStepCounter + 1;
					continue;
				}
				if (recordArray[i][j] != compareArray[i]) {
					intStepCounter = intStepCounter + 1;
				}
			}
		}
	}
	if (i == 0) {
		intStepCounter = intStepCounter + 1;
	}
	return intStepCounter;
}

//*************************************************************************************************
// Function Name        : GetContainsResult
// Function Description : To get compare result contains the data in Compare Value Column and compare with DB result
// Inputs               : Two array
// Returns              : intStepCounter
//************************************************************************************************

function GetContainsResult(recordArray, compareArray) {
	//  var resultArray = new Array();
	var arr1Len = recordArray.length;
	var arr2Len = compareArray.length;
	var maxRowNum = recordArray.length;
	var minRowNum = compareArray.length;
	var columnArray = new Array();
	var intStepCounter = 0;
	var intStepCount = 0;
	var intCompareCount = 0;
	var k = 0;

	// resultArray[0] = recordArray[0];
	//resultArray[0] = columnArr;
	//  resultArray[1] = new Array();
	columnArray[0] = recordArray[0];
	var maxColNum = columnArray[0].length;

	for (var j = 0; j < maxColNum; j++) {
		if (recordArray[0][j] == compareArray[0]) {
			for (var i = 1; i < minRowNum; i++) {
				for (var k = 1; k < maxRowNum; k++) {
					if (recordArray[k][j] == compareArray[i]) {
						k = maxRowNum;
						intStepCount = 0;
					} else {
						intStepCount = intStepCount + 1;
					}
				}
				intCompareCount = intCompareCount + intStepCount;
			}
			intStepCounter = intCompareCount;
		}
		if (k == 0) {
			intStepCounter = intStepCounter + 1;
		}
	}
	return intStepCounter;
}

//*************************************************************************************************
// Function Name        : WriteSQLResultToExcel
// Function Description : To write SQL result to excel file
// Inputs               : SQL Result from DB
// Returns              : None
//************************************************************************************************
function WriteSQLResultToExcel(RecSet) {
	try {
		g_objXLWB = g_objXLApp.Workbooks.Open(g_sTempSQLResultPath); //Create object for excel workbook
		g_objXLApp.Visible = true; //Making excel visible/invisible

		g_objXLApp.DisplayAlerts = false; //Excel will not display alerts
		var objWorksheet = g_objXLWB.Worksheets("Sheet1"); //Create object for excel worksheet
		g_objXLApp.DisplayFullScreen = true;
		g_objXLApp.ActiveWindow.Activate();
		g_objXLApp.ActiveWindow.Zoom = 90;
		objWorksheet.UsedRange.Clear();
		// Save table name
		for (var i = 0; i < RecSet.Fields.Count; i++)
			objWorksheet.Cells(1, i + 1).value = RecSet.Fields.Item(i).Name;
		// Save table value
		RecSet.MoveFirst();
		var intRowIndex = 2;
		while (!RecSet.EOF) {
			for (i = 0; i < RecSet.Fields.Count; i++)
				objWorksheet.Cells(intRowIndex, i + 1).Value = RecSet.Fields.Item(i).Value;
			RecSet.MoveNext();
			intRowIndex = intRowIndex + 1;
		}
		objWorksheet.UsedRange.Columns.AutoFit;
		g_objXLWB.Save();
	} catch (e) {
		Log.Error(e.description);
	}
}


//*************************************************************************************************
// Function Name        : GetCompareResultArray
// Function Description : To get two excel data compare result
// Inputs               : Two array
// Returns              : Array
//************************************************************************************************

function GetCompareResultArray(recordArray, compareData) {
	try {
		var resultArray = new Array();
		var columnArray = new Array();
		var arr1Len = recordArray.length;
		var arr2Len = compareData.length;
		var maxRowNum = recordArray.length;
		var minRowNum = compareData.length;

		resultArray[0] = new Array();
		resultArray[0][0] = "Test Result";
		columnArray[0] = recordArray[0];
		var maxColNum = columnArray[0].length;

		if (arr1Len < arr2Len) {
			maxRowNum = arr2Len;
			minRowNum = arr1Len;
		}

		for (var i = 1; i < maxRowNum; i++) {
			resultArray[i] = new Array();
			intStepCount = 0
				for (var j = 0; j < maxColNum; j++) {
					if (i >= minRowNum) {
						intStepCount = intStepCount + 1
							continue;
					}
					if ('' + recordArray[i][j] != '' + compareData[i][j]) { //compare if cell Excel values are same as DB
						if (!(compareData[i][j] == undefined && recordArray[i][j] == null) || !(compareData[i][j] == null && recordArray[i][j] == undefined)) {
							intStepCount = intStepCount + 1;
						}
					}
				}
				if (intStepCount == 0) {
					resultArray[i][0] = 'passed';
				} else {
					resultArray[i][0] = 'failed';
				}
		}
		return resultArray;
	} catch (e) {
		Log.Error("GetCompareResultArray:" + e.description);
	}
}

//*************************************************************************************************
//  Function Name        : GetExceptionInfo
//  Function Description : To collect error information from the Exception 
//  Inputs               : None
//  Returns              : None 
//**************************************************************************************************/
function GetExceptionInfo(ex){

  //Write the Exception datails to the log.
  var ExceptionInfo = "";
  var Seperator = "  \r\n";
  try{
    
    ExceptionInfo = ExceptionInfo + "Name: " + ex.name + Seperator;
    ExceptionInfo = ExceptionInfo + "Number: " + ex.number + Seperator;
    ExceptionInfo = ExceptionInfo + "Description: " + ex.description + Seperator;
    ExceptionInfo = ExceptionInfo + "Message: " + ex.message + Seperator;
    
  }catch(e){
    ExceptionInfo = "Unable to collect exception information";
    Log.Error("Exception occured in GetExceptionInfo itself." + e.description ,pmHighest);
  }
  return ExceptionInfo;
} 


//*************************************************************************************************
//  Function Name        : GetExceptionLocation
//  Function Description : To get the method name in which the exception occured.
//  Inputs               : None
//  Returns              : Method name as string 
//**************************************************************************************************/
function GetExceptionLocation()
{
  var CurrentCaller = "";
  var MethodName = "Exception occured.";
  try{
     CurrentCaller = GetExceptionLocation.caller;
     if (!IsNullorUndefined(CurrentCaller)){
       MethodName = CurrentCaller.toString().match(/function (\w*)/)[1];
       MethodName = "Exception occured in " + MethodName ;
     }
  }catch(e){
    Log.Error("Exception occured in GetExceptionLocation.", GetExceptionInfo(e), pmHighest);
  }
  return MethodName; 
}

//*************************************************************************************************
//   Function Name        : GetStepNumber
//   Function Description : To return the sequential number of the step.
//   Inputs               : Actual Step Number "Step 43 NH.SyRs.4992 VP"  
//   Returns              : Returns the sequential number of the step.
//**************************************************************************************************/
function GetStepNumber(StepNo){
     var ReturnValue = "0";
     
     try {
          //Clean up the string. 
          StepNo = Trim(StepNo.toUpperCase());
          StepNo = StepNo.replace("-"," ");
          
          //Remove the Step and trim the string, so that the step numerb comes first.
          StepNo = StepNo.replace("STEP","");
          StepNo = Trim(StepNo);
          
          //Split by space and trim the first array.
          ReturnValue = StepNo.split(" ")[0];
    }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    }          
    return StepNo;
}

//*************************************************************************************************
// Function Name        : CloseProcess
// Function Description : To close all instances of the proces, by the name passed as argument.
// Inputs               : None
// Returns              : None
//************************************************************************************************
function CloseProcess(ProcessName){
  try{
        var ProcessObject = Sys.WaitProcess(ProcessName,0);
        if(ProcessObject.Exists){
          ProcessObject.Close();
          
          //Wait for the process to close.
          Wait(5);
          
          //If the process is still present then terminate it.
          if(ProcessObject.Exists){
             ProcessObject.Terminate();
          }  
            
        }
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }      
}


//************************************************************************************************
// Function Name        : IsNullorUndefined
// Function Description : To check whether the variable is Null or Undefined
// Inputs               : VariableToCheck  
// Returns              : True/False
//************************************************************************************************ 
function IsNullorUndefined(VariableToCheck){

  var ReturnValue = false;

  try {
    if (VariableToCheck === null || typeof(VariableToCheck) == 'undefined'){
        ReturnValue = true;
    }
  }catch(e){
     Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }  
   
  return ReturnValue;
}

//************************************************************************************************
//  Function Name        : IsEmpty 
//  Description  	 	 : To check whether the variable is Empty
//  Inputs               : VariableToCheck  
//  Returns              : True/False
//************************************************************************************************/
function IsEmpty(VariableToCheck){

  var ReturnValue = false;

  try {
    if (VariableToCheck === '' || VariableToCheck.length<=0 ){
        ReturnValue = true;
    }
  }catch(e){
     Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }  
   
  return ReturnValue;
}

//************************************************************************************************
// Function Name        : Wait
// Function Description : Function to wait for the specific time        
// Inputs               : Time in Seconds  
// Returns              : None      
//************************************************************************************************ 
function Wait(TimeInSeconds){
  try{
    if (IsNullorUndefined(TimeInSeconds)){
        TimeInSeconds = 3; //Default Value is 3 Seconds
    }
    aqUtils.Delay(TimeInSeconds * 1000);
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }    
}


//************************************************************************************************
// Function Name        : WaitForObject
// Function Description : Function to wait until an object exists            
// Inputs               : ObjectIdentification String  
// Returns              : If the object is found then true is returned, else false is returned.      
//************************************************************************************************
function WaitForObject(ObjectIdentification){

  var iLoopIndex = 0;
  var objDesc = null;
  var IsObjectFound = false;

  try { 
     
     //Check if object is not null or undefined
     if(!IsNullorUndefined(ObjectIdentification)){
        for(iLoopIndex=0;iLoopIndex<=10;iLoopIndex=iLoopIndex+1) {
          objDesc = eval(ObjectIdentification);
          if(IsExists(objDesc)){
              IsObjectFound = true;
              break;
          }else{
              Sys.Refresh();
              Wait(1);
          }
        }
     }else{
          Log.Warning(StepNumber+" - Value of Object is " + ObjectIdentification);
     }
  
    //Hike the Global Counter variables by 1000 to skip the next iterations
    if(!IsObjectFound){
      g_RowNumObjNotFound = intIterCnt;
      StopCurrentTest();
     }
    
  }catch(e){
     Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
  
  return objDesc;
}


//************************************************************************************************
// Function Name        : WaitForPageSync
// Function Description : Function to wait until the Webpage is completely loaded.         
// Inputs               : ObjectIdentification String of the WebPage
// Returns              : True if the page loaded within timeout, else false.      
//************************************************************************************************
function WaitForPageSync(){

  var ReturnValue = false;
  var PageObject = null;
  var WaitResult = null; 
  var MaxTimeout = 60;  //60 Seconds
  
  try {
     oPageObject = eval(g_PageObject);
     WaitResult = oPageObject.Wait(MaxTimeout * 1000);
     if (WaitResult === null || WaitResult.length == 0 ){
          Log.Warning("The page did not load completely within " + MaxTimeout + "seconds.")
     }else{
          ReturnValue = true;
     }
  }catch(e){
     Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }

  return ReturnValue;
}



//*************************************************************************************************
//  Function Name        : CompareText
//  Function Description : To Compare two strings if they are same.
//  Inputs               : String1 the first string and String2 the second string
//  Returns              : Boolean, true if both the string are same and false otherwise.
//  Date                 : 09-Sep-2016
//**************************************************************************************************/
function CompareText(String1,String2){
     var ReturnValue = false;
     try{
          //Check if the arguments are valid.
          if (IsNullorUndefined(String1)){
              String1 = "NULL1";
          }
          if (IsNullorUndefined(String2)){
              String2 = "NULL2";
          }
          if (IsEmpty(String1)){
              String1 = "EMPTY";
          }
          if (IsEmpty(String2)){
              String2 = "EMPTY";
          } 
          //Convert the arguments of type string, incase if the type is different.
          String1 = CleanUpText(String1);
          String2 = CleanUpText(String2);
    
          //Compare the strings and update the result
          if (String1 == String2){
               ReturnValue = true;
          }
     }catch(e){
          Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
     }
     return ReturnValue;   
  } 

//*************************************************************************************************
//  Function Name        : ContainsText
//  Function Description : To check if the second string is contained in the first string.
//  Inputs               : BiggerString the first string and SmallerString the second string
//  Returns              : Boolean, true if both the string are same and false otherwise.
//  Date                 : 09-Sep-2016
//**************************************************************************************************/  
function ContainsText(BiggerString,SmallerString){
     var ReturnValue = false;
     try{
          //Check if the arguments are valid.
          if (IsNullorUndefined(BiggerString)){
              BiggerString = "NULL1";
          }
          if (IsNullorUndefined(SmallerString)){
              SmallerString = "NULL2";
          }

          if (IsEmpty(BiggerString)){
              BiggerString = "EMPTY";
          }
          if (IsEmpty(SmallerString)){
              SmallerString = "EMPTY";
          }     
          //Convert the arguments of type string, incase if the type is different.
          BiggerString = CleanUpText(BiggerString);
          SmallerString = CleanUpText(SmallerString);
    
          //Compare the strings and update the result
          if (aqString.Find(BiggerString, SmallerString) != -1 ) {
               ReturnValue = true;
          }
     }catch(e){
          Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
     }
     return ReturnValue;   
  } 
  
//*************************************************************************************************
//  Function Name        : CloseConfirmWindow
//  Function Description : To click OK or cancel on the confirm window.
//  Inputs               : ConfirmObject - Object of the confirm window
//                       : AllowNavigation - true to click ok. False to click cancel.
//  Returns              : None
//  Date                 : 16-Sep-2016
//**************************************************************************************************/    
function CloseConfirmWindow(ConfirmObject, AllowNavigation){
    if (ConfirmObject.Exists){
      if (AllowNavigation){
        ConfirmObject.Button("OK").Click();
      }else{
        ConfirmObject.Button("Cancel").Click();
      }
    }
}  

//*************************************************************************************************
// Function Name        : IsObject
// Function Description : To verify object or not
// Inputs               : object name 
// Returns              : true or false
//*************************************************************************************************
  function IsObject(InputArgument){
    var ReturnValue = false;
    try{
      if (!(IsNullorUndefined(InputArgument) && typeof(InputArgument)=='object')){
        ReturnValue = true;
      }
    }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    }
    return ReturnValue;
  }
  
//*************************************************************************************************
// Function Name        : IsExists
// Function Description : To verify object existance or not
// Inputs               : object name 
// Returns              : true or false
//*************************************************************************************************
  function IsExists(obj){
    var ReturnValue = false;
    try{
      if (!IsNullorUndefined(obj)){
        if (obj.Exists){
          ReturnValue = true;
        }
      }
    }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    }
    return ReturnValue;
  }
  
//*************************************************************************************************
//   Function Name        : GetText
//   Function Description : To Get the text of an object.
//   Inputs               : action object(Current Object)
//   Returns              : content Text
//**************************************************************************************************/
  function GetText(ActionObject){
  
    var ObjectText = null;
    var ObjectType = null;  
    try{
      var ObjectType = ActionObject.ObjectType

        switch(ObjectType.toUpperCase()){
          case "BUTTON":
          case "LINK":
          case "PANEL":
          case "TEXTNODE":
          case "LABEL":
          case "CELL":       
            ObjectText = ActionObject.contentText;
            break;
          case "TEXTBOX":
          case "PASSWORDBOX":
          case "EDIT":
            ObjectText = ActionObject.Text;
            break;
          case "SELECT":
            ObjectText = ActionObject.wText;
          break;   
        case "FORM":         
          ObjectText = ActionObject.contentText;
          break;
        case "TABLE":         
          ObjectText = ActionObject.contentText;
            break;            
          default :
             ObjectText = "ObjectType Is Not Matched";
             break;
          }
      }catch(e){
        Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
      }   
      return ObjectText;   
  }
  
//*************************************************************************************************
//   Function Name        : StoreVariable
//   Function Description : To Get the value from the page AND store it in the gloabl dictionary variable
//   Inputs               : Key to store in dictionary global variable
//   Returns              : NA
//*************************************************************************************************
function StoreVariable(){
  var strKey = ReadExcel(g_intStrtRow,c_intInputValCol);  //Reads the key from c_intInputValCol 
  var strAction = ReadExcel(g_intStrtRow, c_intActionCol); 
  try{
    switch(strAction.toUpperCase()){
        case "URL":
         strVal=GetCurrentURL();
         g_storeVariables.Add(strKey,strVal);
         break;
        case "TEXT":
         break;
        
    }
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
  
}
//*************************************************************************************************
//   Function Name        : GetCurrentURL
//   Function Description : To get the current page URL
//   Inputs               : 
//   Returns              : current page url
//*************************************************************************************************
function GetCurrentURL(){
  
  var PageURL=null;
  try{  
    // Obtains the page object
    if (IsNullorUndefined(oPageObject)){
        oPageObject = eval(g_PageObject);
    }
    PageURL = oPageObject.URL;//to get the URL of the web page to which the Page object corresponds
  }catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
  return PageURL;
}

//*************************************************************************************************
//   Function Name        : RoundOff
//   Function Description : To round of the given number to some decimal precision                  
//   Inputs               : any float number,decimalprecision number     
//   Returns              : Returns round off value else returns the same input value    
//*************************************************************************************************
  function RoundOff(InputValue,decimalPrecision){
   try{
    var ReturnValue=InputValue;//To set value is numeric integer or not    
      if(IsNumeric(InputValue)){    
        var base=Math.pow(10,decimalPrecision);      
        ReturnValue=Math.round(InputValue*base)/base;        
      }   
     }catch(e){
       Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
     } 
    return ReturnValue;
  }

//*************************************************************************************************
//   Function Name        : IsFloat
//   Function Description : To check whether the number is float or not                 
//   Inputs               : any numeric    
//   Returns              : boolean flag    
//*************************************************************************************************
   function IsFloat(InputValue){
     var ReturnValue=false;
     try{
        InputValue = VarToStr(InputValue);      
        var arr=InputValue.split(".");
        if((arr.length>1)){//To check whether number has dot or not 
          ReturnValue=true;          
        }
     }catch(e){
        Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
      } 
     return ReturnValue;
  }

/*************************************************************************************************
//   Function Name        : IsNumeric
//   Function Description : To check whether the value is number or not                 
//   Inputs               : any string    
//   Returns              : boolean flag    
*************************************************************************************************/
  function IsNumeric(InputValue) {
  
   try{
      var ReturnValue=false;//To set value is numeric or not
      if(!IsNullorUndefined(InputValue)){
        InputValue = Trim(InputValue);
        if (!isNaN(InputValue)){   
         ReturnValue=true; 
         } 
      }
    }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    } 
    return ReturnValue;
   }

    
//*************************************************************************************************
// Function Name        : IsDate
// Function Description : To Verify whether Input value is a date or not.
// Inputs               : Date or string
// Returns              : True if input was a date or else False.
//*************************************************************************************************
  function IsDate(InputDate){
    var ReturnValue=false;
    var MonthValue;
    try{
      if (!IsNullorUndefined(InputDate)){  //Checking parameters are not empty
        InputDate = Trim(InputDate.toString());
        InputDate = InputDate.replace(" ","-");
        InputDate = InputDate.replace("/","-");

        if (InputDate.indexOf("-")>0){
          try{
          var MonthValue = aqDateTime.GetMonth(InputDate);
          }catch(Exception){
            MonthValue = "Error";
          }
          if (!isNaN(MonthValue)){ 
            ReturnValue = true;
          }
        }
      }
    }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    }
    return ReturnValue;
  }   

//*************************************************************************************************
//   Function Name        : VerifyChildExists
//   Function Description : To Verify whether Input value is present in an array or not.
//                          Array of data will come through the object we have provided
//   Inputs               : object which has list of strings
//   Returns              : True if input was there in array or else False.
//*************************************************************************************************
    function VerifyChildExists(ParentObject,ChildObjectType,ChildItemText){
    try{
        var ChildObj = ParentObject.FindAllChildren("objectType",ChildObjectType,50).toArray();  //Find all the child objects of the type Link
        var ResultFlag=false;
        var intChildCnt;        
        for (intChildCnt=0 ; intChildCnt < ChildObj.length ; intChildCnt++){ 
           if ((Trim(GetText(ChildObj[intChildCnt]))==Trim(ChildItemText))){
              ResultFlag = true;    
              break;
           }
        } 
    }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    }
    return ResultFlag;
  }


//*************************************************************************************************
// Function Name        : GetChildObjectsByXPath
// Function Description : To get all the child objects which matches the XPath
// Inputs               : XPath     
// Returns              : Child Objects in array    
//*************************************************************************************************
 
 function GetChildObjectsByXPath(XPath){
 
  var ReturnValue = null;  //Intialize Return Value
  var ChildObjects = null;
  var ChildObjectsArray = null;
  
  try{
  
  XPath = XPath.substr(6,XPath.length);  //Removing "XPath:" string from XPath
  ParentObject = "Sys.Browser("+chr(34)+g_strBrowser+chr(34)+").Page("+chr(34)+g_strFormattedUrl+chr(34)+").";
  ChildObjects = aqString.Format("EvaluateXPath(%s,true)",aqString.Quote(XPath));
  ChildObjects = eval(ParentObject+ChildObjects)
  if (!IsNullorUndefined(ChildObjects)){
    ChildObjectsArray = (new VBArray(ChildObjects)).toArray();  
    if (ChildObjectsArray.length > 0){
      ReturnValue = ChildObjectsArray;
    }
  }
  
  }catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
  return ReturnValue;
 }

//*************************************************************************************************
// Function Name        : GetExternalData
// Function Description : Method to fetch data from external files (LocalizationData, CalculationData, TestData)                    
// Inputs               : g_OtherInputValue - Type of Data (LocalizationData, CalculationData, TestData)
//                      : InputData - Key of the data to be fetched.
// Returns              : Returns the value from external file.
//************************************************************************************************* 
 function GetExternalData(g_OtherInputValue, InputData){
  var ReturnValue = null;
  
  try{
  
    //Read External data only for Adequest
    if (!IsAdequest){
        return InputData;
    }  
  
    if (IsNullorUndefined(g_OtherInputValue) || IsNullorUndefined(InputData)){
        return InputData;
    }
    
    g_OtherInputValue = g_OtherInputValue.toUpperCase();
    InputData = Trim(InputData);
    switch(g_OtherInputValue){
     case "TESTDATA":
      ReturnValue = Runner.CallMethod("ExcelUtil.GetTestData",InputData); //Runner.CallMethod is used to avoid cross refernce issue of ScriptUnits
      break;
     case "LOCALIZATIONDATA":
      ReturnValue = Runner.CallMethod("ExcelUtil.GetLocalizationData",InputData);
      break;
     case "CALCULATIONDATA":
      ReturnValue = Runner.CallMethod("ExcelUtil.GetCalculationData",InputData, true);
      break;
     case "REGIMENDATA":
      ReturnValue = Runner.CallMethod("ExcelUtil.GetRegimenData",InputData, true);
      break;      
     case "RUNTIMEDATA":
      ReturnValue = GetRunTimeData(InputData); 
      break;
     default:
      ReturnValue = InputData;
      break;
    }
  }catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
  return ReturnValue;
 }
 
 
//*************************************************************************************************
// Function Name        : StoreRunTimeData
// Function Description : DataEntry case to store runtime text from the application to a variable.                    
// Inputs               : sKey - Dictionary Key to which the value should be stored.
//                      : sValue - Value to be stored in the key.
// Returns              : Returns 0 for Pass and 1 for Fail.
//*************************************************************************************************    
function StoreRunTimeData(sKey, sValue){
     var ReturnValue = 1; //Fail
     try{
     
         if (IsNullorUndefined(sKey) || IsEmpty(sKey)){
          ActualMessage = "Value is not stored, since the key ' "+sKey+" ' is not valid.";
          Log.Error(ActualMessage);
          return ReturnValue;
         }
     
         if(!g_RunTimeData.Exists(sKey)){
               g_RunTimeData.Add(sKey,sValue); //Add a key with the value.
               ActualMessage = "'" + sValue + "' is stored in the variable " + sKey;
               Log.Message(ActualMessage);
               ReturnValue = 0;
         }else{
               g_RunTimeData.item(sKey) = sValue; //Update the existing key with the value.
               ActualMessage = "'" + sValue + "' is stored in the variable " + sKey;
               Log.Message(ActualMessage);
               ReturnValue = 0;
         }
         
         if (IsNullorUndefined(sValue) || IsEmpty(sValue)){
          ReturnValue = 1;//Allowing null and empty values to be stored in dictionary, but let the step fail.
         }
                  
     }catch(e){
          Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
     }
      return ReturnValue;
}

/*************************************************************************************************
//   Function Name        : GetAttribute
   Function Description : To get attribute from a HTML tag                 
   Inputs               : Object,attribute name as input  
   Returns              : attribute value    
*************************************************************************************************/

  function GetAttribute(objDesc, strData){
    var ReturnValue="null";
    try{
        ReturnValue = Trim(objDesc.getAttribute(strData));
  
      }catch(e){
        Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    }  
    return ReturnValue;
  }


/*************************************************************************************************
//   Function Name        : PressKey
   Function Description : To enter or press any key from keyboard             
   Inputs               : object,object name,key name  
   Returns              : none    
*************************************************************************************************/
  function PressKey(objDesc,ObjectName,strData){
    var ReturnValue="null";
    ExpectedMessage = "'"+strData+"' key shall be pressed on " + ObjectName;
    ActualMessage = "'"+strData+"' key is not pressed on " + ObjectName;
   
    try{
      switch(strData.toUpperCase()){
          case "ENTER":
            ReturnValue="[Enter]";
            break;
          case "SPACE":
            ReturnValue="     "
            break;
          case "ESC":
            ReturnValue="[Esc]";
            break;
          case "CLEAR":
            ReturnValue="[Clear]";
            break;  
          case "CAPS LOCK":
            ReturnValue="[Caps]"; 
            break;
          case "HOME":
            ReturnValue="[Home]"; 
            break;  
          case "SHIFT END":
            ReturnValue="![End]"; 
            break; 
          case "SHIFT HOME":
            ReturnValue="![Home]"; 
            break; 
          case "SELECT ALL":
            ReturnValue="^a"; 
            break; 
          case "DELETE":
            ReturnValue="[Del]"; 
            break;
          case "TAB":
            ReturnValue="[Tab]"; 
            break;  
          default:
             ReturnValue = strData;
            break;      
        } 
         if((ReturnValue!=strData)){
            objDesc.Keys (ReturnValue);
            ActualMessage = "'"+strData+"' key is pressed on " + ObjectName;
          } 
      }catch(e){
        Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    }
    
  }


/*************************************************************************************************
//   Function Name        : ReplaceAll
   Function Description : To replace all matching substrings with a new value             
   Inputs               : InputString - String to bo updated.
                        : FindString - String to bo searched.
                        : ReplaceString - String to bo replaced.                        
   Returns              : Updated string with the values replaced with new values    
*************************************************************************************************/ 
  function ReplaceAll(InputString, FindString, ReplaceString) {
    var ReturnValue = InputString;
    var RegExpMatch = new RegExp(FindString, 'g');
    try {
      InputString =  InputString.replace(RegExpMatch, ReplaceString);
      ReturnValue = InputString;
    }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    }
    return ReturnValue;
}

/*************************************************************************************************
//   Function Name        : Extend Session
   Function Description : To Click on Extend button when time out alert notice is displayed             
   Inputs               : None                        
   Returns              : None    
*************************************************************************************************/ 

function ExtendSession(){
    
    var ExtendObject = null;
    var StartTime = g_tLastSessionExtensionTime;
    var CurrentTime = aqDateTime.Now();
    var ElapsedTime = 0;
    
    try{
        
        if (StartTime==0){
            StartTime = g_tStart_Time;
        }
        ElapsedTime = (CurrentTime - StartTime)/60000;  //Elapsed Time in minutes after starting the current testscript or the last session.
        if (ElapsedTime < 20){
            return ; //Stop processing if the elapsed time is less than 20 minutes.
        }    
  
        //If extend Session button is displayed then click on it.
        if (IsNullorUndefined(oPageObject)){
            oPageObject = eval(g_PageObject);
        }
        
        if (oPageObject.Exists){
          if(oPageObject.FindChild("className","submit popup-alert-close",1).Exists){
            ExtendObject = oPageObject.FindChild("className","submit popup-alert-close",1);
            if (ExtendObject.VisibleOnScreen){
              ExtendObject.Click();
              g_tLastSessionExtensionTime = aqDateTime.Now();
              Log.Warning("Session Inactivity occured. Clicking on Extend button.");
            }
          }
        }
        
    }catch(e){
        Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    }
    
}

/*************************************************************************************************
// Function Name        : VerifyDropDownValue
   Function Description : To verify the availability of drop down value in the list.             
   Inputs               : objDesc,strDta,ObjectName                       
   Returns              : boolean  
*************************************************************************************************/ 

  function VerifyDropDownValue(objDesc,strData){
    var ReturnValue=false;  
    var ListValue="null";
    strData=  Trim(strData);
    try{
      var ListCount= objDesc.wItemCount;//To get total items count from drop down list
      for(var ListIndex=0;ListIndex<ListCount;ListIndex++){
        ListValue=Trim(objDesc.wItem(ListIndex));
        if(CompareText(ListValue,strData)){ //check value
            ReturnValue = true; 
            break; 
        }    
      }
     }catch(e){
          Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
      }  
      return ReturnValue;
  }

/*************************************************************************************************
// Function Name        : VerifyWarningMessage
   Function Description : To verify warning icon,tool tip text             
   Inputs               : StepNumber,ObjectName,strData                                                
   Returns              : ReturnValue- Returns 0 for Pass and 1 for Fail
*************************************************************************************************/
  function VerifyWarningMessage(StepNumber,ObjectName,strData){
   
    var ReturnValue = 1;
    var ActualText = "";
    var AttributeValue="";
    var MainObject="";
    var MainObjectPath="";
    var WarningIcon="/..//div[@class='warning-icon']";//warning icon path 
    var WarningIcon_Obj="";//warning icon object
    var WarningIconPath="";//warning icon full path 
    var ToolTip="/..//span[contains(@class,'tooltiptext')]"; //Tool Tip path  
    var ToolTip_Obj="";//Tool Tip object
    var ToolTipPath="";//Tool Tip full path
    var iLoopIndex = 0;
    var IsObjectFound = false;
    ExpectedMessage = "Warning icon and tool tip text shall be displayed for the " + ObjectName;
    ActualMessage = "Warning icon and tool tip text is not displayed for " + ObjectName;
    try{
          MainObjectPath=g_dicColNames_obj.item(ObjectName);
          MainObject=eval(BuildWebObj(MainObjectPath));           
          AttributeValue = GetAttribute(MainObject, "class");
          if((CompareText("outside-recommended-range",AttributeValue))){//To verify class attribute value is outside range
            WarningIconPath = MainObjectPath.concat(WarningIcon);//concat of main path ,Warning Icon 
            WarningIcon_Obj = eval(BuildWebObj(WarningIconPath));         
            if ((IsExists(WarningIcon_Obj))){  //To check warning object presence             
                ToolTipPath = MainObjectPath.concat(ToolTip); //concat of main path ,tool tip 
                MainObject.Click();
                ToolTip_Obj=eval(BuildWebObj(ToolTipPath)); 
                ScrollIntoView(ToolTip_Obj);
                
                //Retry to get the object.
                for(iLoopIndex=0;iLoopIndex<=10;iLoopIndex=iLoopIndex+1) {
                  if(IsExists(ToolTip_Obj) && ToolTip_Obj.Visible){
                      IsObjectFound = true;
                      break;
                  }                   
                  MainObject.Click();
                  Wait(2);
                  Sys.Refresh();
                  ToolTip_Obj=eval(BuildWebObj(ToolTipPath));
                  ScrollIntoView(ToolTip_Obj); 
                }
                
                if (IsObjectFound){                                                                    
                  ScrollIntoView(ToolTip_Obj);//Scroll the object to view
                  ActualText = GetText(ToolTip_Obj);
                  if(CompareText(strData, ActualText)){
                    ReturnValue = 0;//Pass 
                    ActualMessage = "Warning icon and tool tip text is displayed for " + ObjectName;
                    strLogMessage = StepNumber +" - "+ObjectName+" value is verified as '"+ActualText+ "'";
                    Log.Message(strLogMessage);
                  }else{                    
                    ActualMessage = "Warning Icon and Tool Tip text is not displayed for " + ObjectName;
                    strLogMessage = StepNumber +" - "+ObjectName+" text is expected to be ["+strData+"] and actual is ["+ActualText+"]";
                    Log.Error(strLogMessage);
                  }
                }else{
                  ActualMessage = "Tool Tip is not visible on the screen for "+ ObjectName;
                  Log.Error(StepNumber+" - Tool Tip is not displayed for the "+ ObjectName);  
                }  
            }else{ 
              ActualMessage = "Warning Icon is not displayed for " + ObjectName;
              // If the element was not found, post a message to the log
              Log.Error(StepNumber+" - The Warning Icon  object 'WarningIcon_Obj' does not exist.");
            }
        }else{
          ActualMessage = "Out side range warning is not displayed for " + ObjectName;
          // If the out side range element was not found, post a message to the log
          Log.Error(StepNumber+" - The out side range  object does not exist.");
        }  
     }catch(e){
        Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
     } 
    return ReturnValue;
  }
  
  
/*************************************************************************************************
// Function Name        : ConvertUnicode
   Function Description : To convert Unicode character to string
   Inputs               : Unicode Text
   Returns              : Cleaned Text
**************************************************************************************************/
function ConvertUnicode(sInputText){
  var ReturnValue = sInputText;
  try{
    if (!IsNullorUndefined(sInputText) || IsEmpty(sInputText)){
      if (ContainsText(sInputText,"\\u")){
          var UnicodePattern = /\\u([\d\w]{4})/gi; //  /\\u([\d\w]{4})/gi
          ReturnValue = sInputText.replace(UnicodePattern, 
                         function (match, grp) {
                          return String.fromCharCode(parseInt(grp, 16)); 
                         } 
                       );
      }
    }
    return ReturnValue;
   }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
   }
   return ReturnValue;
 }  
 

/*************************************************************************************************
//   Function Name        : WaitForLoadingSpinner
//   Function Description : To wait until the Loading spinner disappears.              
//   Inputs               : None
//   Returns              : True if the Loading spinner disappears within maxtimeout. If not then false.    
//*************************************************************************************************/ 

function WaitForLoadingSpinner(){

    var SpinnerObject = null;
    var SpinnerWasDisplayed = false;
    var iLoop = 0;
    var MaxTimeout = 5; //Seconds
    var ReturnValue = true;
    var SpinnerXPath = null;
    try{
    
        if (!IsAdequest){
            return ReturnValue; //do not perform if it is not adequest.
        }
    
        //Get the Property for the page 
        if (!IsNullorUndefined(oPageObject)){
            oPageObject = eval(g_PageObject);
        }
        
        //If any alert is present then do not wait for the spinner
        if((oPageObject.WaitConfirm(100)).Exists){
            return ReturnValue;    
        }
        
        //If the Loading spinner is displayed, then wait until it disappears
        if (oPageObject.Exists){
            for (iLoop = 1;iLoop <= MaxTimeout;iLoop++){
                SpinnerObject = oPageObject.FindChild("idStr","loading-spinner",2000);
                if (IsExists(SpinnerObject)){
                  Indicator.PushText("Waiting for the Loading Spinner to disappear.")
                  Log.Event("Waiting for the Loading Spinner to disappear.");
                  Wait(1);
                  iLoop--; //Decrement the loop if it exists
                  SpinnerWasDisplayed = true;
                  if (iLoop == MaxTimeout){
                      Log.Error("Loading Spinner is still displayed after maximum wait time.");
                      ReturnValue = false;
                      break; //Spinner is still displaying till MaxTimeOut
                  }                   
                }else{// if(SpinnerWasDisplayed){
                  //The Spinner was displayed and it is not displayed now. Then it disappeared, so stop the loop.
                  Log.Event("Loading Spinner is not displayed..");
                  break;
                }
            }
        }else{
            Log.Warning("Unable to wait until the spinner disappears since the page object itself is not found.")
        }
    }catch(e){
        Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    }
    return ReturnValue;
} 



//*************************************************************************************************
// Function Name        : VerifyCalculatedValues
// Function Description : To verify that the calculated value falls within the expected range.                    
// Inputs               : StepNumber - Step Number of the test step
//                      : ObjectName - Name of the object
//                      : objDesc - Evaluated Test complete object
//                      : strData - Expected Text     
// Returns              : Returns 0 for Pass and 1 for Fail.
//*************************************************************************************************    
    function VerifyCalculatedValues(StepNumber, ObjectName, NumericText, strData){

      var ReturnValue = 1;
      var ActualValue = 0;
      var ElementText = "";
      var ExpectedValue = 0;
      var DeciamlPosition = 0;
      var Difference = 0;
      var Average = 0;
      var DifferenceCheck = false;
      var DecimalPrecisionCheck = true;
      ExpectedMessage = "Text of "+ObjectName+" shall be '"+strData + "'";
      ActualMessage = "";
      strLogMessage = "";
      
      try{ 
          //convert the ExpectedValue to Numeric
          ExpectedValue = strData;
          if (IsNumeric(ExpectedValue)){
              ExpectedValue = Number(ExpectedValue);
          }
          
          //convert the ActualValue to Numeric. And perform the decimal precision check
          ActualValue = NumericText;
          if (IsNumeric(ActualValue)){
              ActualValue = Number(ActualValue);
              ElementText = aqConvert.VarToStr(ActualValue);
              var DeciamlPosition = ElementText.indexOf(".");
              if (DeciamlPosition > 0){
                  ElementText = ElementText.substring(DeciamlPosition+1 , ElementText.length);
                  DeciamlPosition = ElementText.length;
                  if (DeciamlPosition > 2){
                    DecimalPrecisionCheck = false;
                    ActualMessage = " And the decimal precision is more than 2."
                    strLogMessage = " And the decimal precision is more than 2."
                  }
              }
          }       
          
          //Perform the difference check. Difference Percentage = ((ExpectedValue-ActualValue)/((ExpectedValue+ActualValue)/2) * 100 ;
          Difference = ExpectedValue-ActualValue;
          if (Difference != 0){
            Average = (ExpectedValue + ActualValue)/2;
            Difference = (Difference / Average) * 100;
            Difference = Math.abs(Difference);
            Difference = RoundOff(Difference, 2);
          }
          
          if (Difference == 0){
            DifferenceCheck = true; //Pass in HTML
            ActualMessage = "Text of "+ObjectName+" is " + ActualValue + ". Difference is " + Difference + "%" + ActualMessage;
            strLogMessage = StepNumber +" - "+ObjectName+" expected is ["+ExpectedValue+"] and actual is ["+ActualValue+"]. The difference is ["+Difference+"%]." + strLogMessage;
            Log.Message(strLogMessage);//Message in Log
          }else if(Difference <= CalculationDifferenceTolerance){
            DifferenceCheck = true;//Pass in HTML
            ActualMessage = "Text of "+ObjectName+" is " + ActualValue + ". Difference is " + Difference + "%" + ActualMessage;
            strLogMessage = StepNumber +" - "+ObjectName+" expected is ["+ExpectedValue+"] and actual is ["+ActualValue+"]. The difference is ["+Difference+"%] which is less than " +  CalculationDifferenceTolerance + "%." + strLogMessage;
            Log.Message(strLogMessage);//Message in Log
          }else{
            DifferenceCheck = false;//Fail in HTML
            ActualMessage = "Text of "+ObjectName+" is " + ActualValue + ". Difference is " + Difference + "%" + ActualMessage;
            strLogMessage = StepNumber +" - "+ObjectName+" expected is ["+ExpectedValue+"] and actual is ["+ActualValue+"]. The difference is ["+Difference+"%] which is more than " +  CalculationDifferenceTolerance + "%." + strLogMessage;
            Log.Error(strLogMessage);//Error in Log
          }
          
          //Assign the step status and construct the acutal message.
          if (DifferenceCheck && DecimalPrecisionCheck){
              ReturnValue = 0;//Pass
          }else{
              ReturnValue = 1; //Fail
          }
          
      }catch(e){
        Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
      }
      return ReturnValue;
          
    }

/*************************************************************************************************
//   Function Name        : GetAge
   Function Description : To get the age              
   Inputs               : age between date1 and date2
   Returns              : age between date1 and date2   
*************************************************************************************************/     
function GetAge(FromDate, ToDate) {

  var ReturnValue = 0;
  //var MilliSec_Per_Year = 1000 * 60 * 60 * 24;
  try{
      // Discard the time and time-zone information.
      var utc1 = Date.UTC(FromDate.getFullYear(), FromDate.getMonth(), FromDate.getDate());
      var utc2 = Date.UTC(ToDate.getFullYear(), ToDate.getMonth(), ToDate.getDate());
      
      //Take the difference in milliseconds and conver to year.      
      ReturnValue = (utc2 - utc1);
      var ageDate = new Date(ReturnValue); // miliseconds from epoch
      ReturnValue = ageDate.getUTCFullYear() - 1970;
    
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }  
  return ReturnValue;
}       

/*************************************************************************************************
//   Function Name        : GetSalutation
   Function Description : To get the translation Key for a Saluation.            
   Inputs               : Saluation which is not translated
   Returns              : Translated Key of a Saluation   
*************************************************************************************************/     
function GetSalutation(inputvalue) {
  try{
      var ReturnValue = "";
      inputvalue = Trim(inputvalue.toUpperCase());
      switch (inputvalue){            
          case "DR":
              ReturnValue = "salutationDR";      
              break;        
      } 
    }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }  
  return ReturnValue;
}               
 



//*************************************************************************************************
// Function Name        : FetchRegionData
// Function Description : To get the parameters which changes by regions                    
// Inputs               : N/A     
// Returns              : Returns the Region Data requested. Ex: Environment Name, Schema Name 
//*************************************************************************************************
function FetchRegionData(DataKey){
    var WebApplicationURL = null;
    var EnvironmentName = null;
    var SharesourceSchema = null;
    var AdequestSchema = null;
    var ReturnValue = null;
    try{
        EnvironmentName = trim(g_DBTablename).toUpperCase();
        SharesourceSchema = EnvironmentName + "C_CORERENAL";
        AdequestSchema = EnvironmentName + "M_PDADEQUEST";
        EnvironmentName = EnvironmentName.replace("NH_",""); //Convert NH_ST3_2 to ST3_2
        EnvironmentName = EnvironmentName.replace("_","R");  //Convert ST3_2 to ST3R2
        ReturnValue = eval(DataKey);
    }catch(e){
        Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
        Log.Error( "Unable to fetch region specific data, so stopping the test execution.")
        Runner.Stop();        
    }
    return ReturnValue;
}

     

//*************************************************************************************************        
// Function Name        : VerifyTextContains
// Function Description : To verify that the expected text is contained the actual text                    
// Inputs               : StepNumber - Step Number of the test step
//                      : ObjectName - Name of the object
//                      : objDesc - Evaluated Test complete object
//                      : strData - Expected Text     
// Returns              : Returns 0 for Pass and 1 for Fail.
//*************************************************************************************************        
    function VerifyTextContains(StepNumber, ObjectName, objDesc, strData){

      var ReturnValue = 1;
      var ActualText = "";
      ExpectedMessage = "The text of "+ObjectName+" shall be "+strData;
      
      try{ 
          ActualText = GetText(objDesc);
          if(ContainsText(ActualText, strData)){
            ReturnValue = 0;//Pass 
            ActualMessage="The text of "+ObjectName+" is "+ActualText;
            strLogMessage = StepNumber +" - "+ObjectName+" value is verified as "+ActualText+"";
            Log.Message(strLogMessage);
          }else{
            ReturnValue = 1; //Fail
            ActualMessage = "The text for "+ObjectName+" is not "+strData;
            strLogMessage = StepNumber +" - "+ObjectName+" text is expected to be ["+strData+"] and actual is ["+ActualText+"]";
            Log.Error(strLogMessage);
          }
      }catch(e){
        Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
      }
      return ReturnValue;
          
    }
    
//*************************************************************************************************
// Function Name        : VerifyTextNotContains
// Function Description : To verify that the expected text is not contained the actual text                    
// Inputs               : StepNumber - Step Number of the test step
//                      : ObjectName - Name of the object
//                      : objDesc - Evaluated Test complete object
//                      : strData - Expected Text     
// Returns              : Returns 0 for Pass and 1 for Fail.
//*************************************************************************************************        
    function VerifyTextNotContains(StepNumber, ObjectName, objDesc, strData){

      var ReturnValue = 1;
      var ActualText = "";
      ExpectedMessage = "Text " + strData + " shall not be present in "+ObjectName;
      
      try{ 
          ActualText = GetText(objDesc);
          if(!ContainsText(ActualText, strData)){
            ReturnValue = 0;//Pass 
            ActualMessage = "Text " + strData + " is not present in "+ObjectName;
            strLogMessage = StepNumber +" - "+ActualMessage;
            Log.Message(strLogMessage);
          }else{
            ReturnValue = 1; //Fail
            ActualMessage = "Text " + strData + " is present in "+ObjectName;
            strLogMessage = StepNumber +" - "+ActualMessage;
            Log.Error(strLogMessage);
          }
      }catch(e){
        Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
      }
      return ReturnValue;
          
    }    
	
//*************************************************************************************************
//   Function Name        : CleanUpText
//   Function Description : To remove Un-necessary spaces,new lines
//   Inputs               : text
//   Returns              : cleanup input text
//**************************************************************************************************/
function CleanUpText(sInputText){
  var ReturnValue = sInputText;
  try{
    if (!IsNullorUndefined(sInputText) || IsEmpty(sInputText)){
      sInputText = aqConvert.VarToStr(sInputText);
      sInputText = sInputText.replace(/(\r\n)/gm,"");
      sInputText = sInputText.replace(/(\n)/gm," ");
      sInputText = sInputText.replace(/(\r)/gm,"");
      sInputText = sInputText.replace(/( ,)/gm,",") //To Handle Patient First name and Last Name
      sInputText = sInputText.replace(/(\s{2,})/gm," ") //Replace 2 or more spaces with single space.
      
      //Replace the Double quotes and backslashes
      if (aqString.Find(sInputText, String.fromCharCode(34)) != -1){
          sInputText = sInputText.replace(/(")/gm,"");
          sInputText = sInputText.replace(/(\\)/gm,"");
      }
	    sInputText = Trim(sInputText);
      
      if (sInputText.length > 1) {
          sFirstCharacter = aqString.GetChar(sInputText,0)
      	  if (sFirstCharacter == ":" || sFirstCharacter == "*") {
              sInputText = sInputText.substring(1,sInputText.length)
          }
          
          sLastCharacter = aqString.GetChar(sInputText,sInputText.length-1)
          if (sLastCharacter == ":" || sLastCharacter == "*") {
            sInputText = sInputText.substring(0,sInputText.length-1)
          }
          sInputText = Trim(sInputText);
      }
      ReturnValue = sInputText;
    }
   }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
   }
   return ReturnValue;
 }	
    
//*************************************************************************************************
//   Function Name        : ConfirmWindow
//   Function Description : To click on confirmation pop-up in the browser
//   Inputs               : PageObject, AllowNavigation --> true or false
//   Returns              : cleanup input text
//**************************************************************************************************/
function ConfirmWindow(PageObject, AllowNavigation){
    var ReturnValue = false;
    try{
        if(PageObject.Exists){
            //If the confirm popup is displayed then click on it.
            if((PageObject.WaitConfirm(500)).Exists){
                if (AllowNavigation){
                    PageObject.Confirm.Button("OK").Click(); 
                    ReturnValue = true;
                    Log.Message("Confirm window displayed. Clicking on 'OK' button."); 
                }else{
                    PageObject.Confirm.Button("Cancel").Click();
                    ReturnValue = true;
                    Log.Message("Confirm window displayed. Clicking on 'CANCEL' button.");
                }
            }
        }
  
        if((aqString.Find(g_browserinfo,"Chrome")!= -1) && (Sys.WaitBrowser("Chrome").Exists)){
           var ChromeDialogObject = Sys.Browser("chrome").FindChild("ObjectType", "Dialog", 200);
           if (ChromeDialogObject.Exists){
                var LeavePagePopup = Sys.Browser("chrome").Dialog("Do you want to leave this site?").Window("Intermediate D3D Window", "", 1);
                LeavePagePopup = eval(LeavePagePopup);
                if (IsExists(LeavePagePopup)){
                    Sys.Browser("chrome").Dialog("Do you want to leave this site?").Keys("[Enter]");
                }
            }
        }
  
  
    }catch(e){
        Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
    } 
  return ReturnValue;
}


//*************************************************************************************************
// Function Name        : VerifyTextEquals
// Function Description : To verify that the expected text matches with the actual text                    
// Inputs               : StepNumber - Step Number of the test step
//                      : ObjectName - Name of the object
//                      : objDesc - Evaluated Test complete object
//                      : strData - Expected Text     
// Returns              : Returns 0 for Pass and 1 for Fail.
//*************************************************************************************************    
    function VerifyTextEquals(StepNumber, ObjectName, objDesc, strData){
      var ReturnValue = 1;
      var ActualText = "";
      ExpectedMessage = "Text of "+ObjectName+" shall be '"+strData + "'";
      try{ 
          ActualText = GetText(objDesc);
          if(CompareText(strData, ActualText)){
            ReturnValue = 0;//Pass 
        ActualMessage="Text of "+ObjectName+" is "+ActualText;
            strLogMessage = StepNumber +" - "+ObjectName+" value is verified as '"+ActualText+ "'";
            Log.Message(strLogMessage);
          }else{
            ReturnValue = 1; //Fail
            ActualMessage = "Text for "+ObjectName+" is '"+ActualText+ "'";
            strLogMessage = StepNumber +" - "+ObjectName+" text is expected to be ["+strData+"] and actual is ["+ActualText+"]";
            Log.Error(strLogMessage);
          }
      }catch(e){
        Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
      }
      return ReturnValue;
}


//*************************************************************************************************
// Function Name        : VerifyTextContainsIgnorecase
// Function Description : To verify that the expected text is contained the actual text by ignoring the case is upper or lower                   
// Inputs               : StepNumber - Step Number of the test step
//                      : ObjectName - Name of the object
//                      : objDesc - Evaluated Test complete object
//                      : strData - Expected Text     
// Returns              : Returns 0 for Pass and 1 for Fail.
//*************************************************************************************************        
function VerifyTextContainsIgnorecase(StepNumber, ObjectName, objDesc, strData){

  var ReturnValue = 1;
  var ActualText = "";
  ExpectedMessage = "The content of "+ObjectName+" shall be "+strData;
      
  try{ 
      ActualText = GetText(objDesc);
      if(ContainTextIgnoreCase(ActualText, strData)){
        ReturnValue = 0;//Pass 
        ActualMessage="The content of "+ObjectName+" is "+ActualText;
        strLogMessage = StepNumber +" - "+ObjectName+" value is verified as "+ActualText+"";
        Log.Message(strLogMessage);
      }else{
        ReturnValue = 1; //Fail
        ActualMessage = "The content for "+ObjectName+" is not "+strData;
        strLogMessage = StepNumber +" - "+ObjectName+" text is expected to be ["+strData+"] and actual is ["+ActualText+"]";
        Log.Error(strLogMessage);
      }
  }catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
  return ReturnValue;
         
}    





//*************************************************************************************************
//  Function Name        : ContainTextIgnoreCase  
//  Function Description : To Compare two strings if they are same by ignoring case.
//  Inputs               : String1 the first string and String2 the second string
//  Returns              : Boolean, true if both the string are same and false otherwise.
//  Date                 : 24-Mar-2017
//**************************************************************************************************/
function ContainTextIgnoreCase(String1,String2){
     var ReturnValue = false;
     try{
          //Check if the arguments are valid.
          if (IsNullorUndefined(String1)){
              String1 = "NULL1";
          }
          if (IsNullorUndefined(String2)){
              String2 = "NULL2";
          }
          if (IsEmpty(String1)){
              String1 = "EMPTY";
          }
          if (IsEmpty(String2)){
              String2 = "EMPTY";
          } 
          //Convert the arguments of type string, incase if the type is different.
          String1 = CleanUpText(String1);
          String2 = CleanUpText(String2);
          //Converting the string to upper case
           String1 = String1.toUpperCase();
           String2 = String2.toUpperCase();
          //Find the string and update the result
          if (aqString.Find(String1, String2) != -1 ) {
               ReturnValue = true;
          }

     }catch(e){
          Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
     }
     return ReturnValue;   
  }

  
//*************************************************************************************************
// Function Name        : VerifyNumericDifference
// Function Description : To verify that the calculated value falls within the expected range where rounding of values is done.              
// Inputs               : StepNumber - Step Number of the test step
//                      : ObjectName - Name of the object
//                      : objDesc - Evaluated Test complete object
//                      : strData - Expected Text     
// Returns              : Returns 0 for Pass and 1 for Fail.
//*************************************************************************************************    
function VerifyNumericDifference(StepNumber, ObjectName, NumericText, strData){

      var ReturnValue = 1;
      var ActualValue = 0;
      var ElementText = "";
      var ExpectedValue = 0;
      var DeciamlPosition = 0;
      var Difference = 0;
      var Average = 0;
      var DifferenceCheck = false;
      var DecimalPrecisionCheck = true;
      ExpectedMessage = "Text of "+ObjectName+" shall be '"+strData + "'";
      ActualMessage = "";
      strLogMessage = "";
      var NumericDifference = "";
      
      try{ 
          //convert the ExpectedValue to Numeric
          ExpectedValue = strData;
          if (IsNumeric(ExpectedValue)){
              ExpectedValue = Number(ExpectedValue);
          }
          
          //convert the ActualValue to Numeric. And perform the decimal precision check
          ActualValue = NumericText;
          if (IsNumeric(ActualValue)){
              ActualValue = Number(ActualValue);
              ElementText = aqConvert.VarToStr(ActualValue);
              var DeciamlPosition = ElementText.indexOf(".");
              if (DeciamlPosition > 0){
                  ElementText = ElementText.substring(DeciamlPosition+1 , ElementText.length);
                  DeciamlPosition = ElementText.length;
                  if (DeciamlPosition > 2){
                    DecimalPrecisionCheck = false;
                    ActualMessage = " And the decimal precision is more than 2."
                    strLogMessage = " And the decimal precision is more than 2."
                  }
              }
          }       
          
          //Perform the difference check. Difference Percentage = ((ExpectedValue-ActualValue)/((ExpectedValue+ActualValue)/2) * 100 ;
          NumericDifference = ExpectedValue-ActualValue;
          NumericDifference = Difference;
          if (Difference != 0){
            Average = (ExpectedValue + ActualValue)/2;
            Difference = (Difference / Average) * 100;
            Difference = Math.abs(Difference);
            Difference = RoundOff(Difference, 2);
          }
          
          if (NumericDifference == 0){
            DifferenceCheck = true; //Pass in HTML
            ActualMessage = "Text of "+ObjectName+" is " + ActualValue + ".";
            strLogMessage = StepNumber +" - "+ObjectName+" expected is ["+ExpectedValue+"] and actual is ["+ActualValue+"]. The difference is ["+Difference+"%]." + strLogMessage;
            Log.Message(strLogMessage);//Message in Log
          }else if(NumericDifference <= 0.01){
            DifferenceCheck = true;//Pass in HTML
            ActualMessage = "Text of "+ObjectName+" is " + ActualValue + ".";
            strLogMessage = StepNumber +" - "+ObjectName+" expected is ["+ExpectedValue+"] and actual is ["+ActualValue+"]. The difference is ["+Difference+"%] which is less than " +  CalculationDifferenceTolerance + "%." + strLogMessage;
            Log.Message(strLogMessage);//Message in Log
          }else{
            DifferenceCheck = false;//Fail in HTML
            ActualMessage = "Text of "+ObjectName+" is " + ActualValue + ".";
            strLogMessage = StepNumber +" - "+ObjectName+" expected is ["+ExpectedValue+"] and actual is ["+ActualValue+"]. The difference is ["+Difference+"%] which is more than " +  CalculationDifferenceTolerance + "%." + strLogMessage;
            Log.Error(strLogMessage);//Error in Log
          }
          
          //Assign the step status and construct the acutal message.
          if (DifferenceCheck && DecimalPrecisionCheck){
              ReturnValue = 0;//Pass
          }else{
              ReturnValue = 1; //Fail
          }
          
      }catch(e){
        Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
      }
      return ReturnValue;
          
}


//*************************************************************************************************
// Function Name         : SwitchBrowser
// Function Description  : To switch parent object to primary or secondary browser
// Inputs                : SwitchToBrowser ->Primary/Secondary
// Returns               : None
//************************************************************************************************
function SwitchBrowser(SwitchToBrowser){
  var BrowserName;
  SwitchToBrowser = SwitchToBrowser.toUpperCase();
  try{
    if (CompareText(SwitchToBrowser,"PRIMARY")){
      BrowserName =  GetBrowserName(g_strBrowserName); //condition to call primary browser 
    }else if(CompareText(SwitchToBrowser,"SECONDARY") && (!g_TreatmentFileUpload)){
      BrowserName = g_strBrowserName1;  //condition to call secondary browser for non treatment file upload objects
    }else{
      BrowserName = "iexplore"; //condition to call secondary browser only for treatment file upload objects
    }
    g_PageObject = "Sys.Browser("+chr(34)+BrowserName+chr(34)+").Page("+chr(34)+g_strFormattedUrl+chr(34)+")";    
    oPageObject = eval(g_PageObject);
  }catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }
}

//*************************************************************************************************
//   Function Name        : StopCurrentTest
//   Function Description : Incrementing velue to stop execution.            
//   Inputs               : N/A.
//   Returns              : N/A.  
//************************************************************************************************* 
function StopCurrentTest(){
  try{
      Log.Error("Stopping current Test.");
      intIterCnt = 1000;
      objnotexist = 1000;
      intIniRwNo = 1000;
      intRowIteration_TC = intMaxRowCnt_TC+ 1000;
   }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
   }  
}

//*************************************************************************************************
// Function Name        : WriteResultData
// Function Description : To Get the Routine Result and store the flag in a file 
// Inputs               : None
// Returns              : None
//**************************************************************************************************/
function WriteResultData(strFilePath, strResult){
  try{    
    var ForReading = 1;
    var objfs;
    var objf;    
    //Creates a new file object
    objfs = Sys.OleObject("Scripting.FileSystemObject");
    objf = objfs.CreateTextFile(strFilePath, true); 
    var tempstr = "RoutineResult = " + strResult;          
    objf.Writeline(tempstr); 
    objf.Close();
    objf = null;
    objfs = null;     
  }catch(e){
      Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
 } 
}