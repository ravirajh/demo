//USEUNIT ApplicationFunction
//USEUNIT FunctionLibrary
//USEUNIT GlobalVariables
//USEUNIT Reports
//USEUNIT ExcelUtil
//USEUNIT PDAUtil

function Driver(){ 
  try{
    Indicator.Hide(); //Hide testcomplete indicator while execution 
    //Retrieve data from Config file
    var sFilePath      =  g_sProjPath+"\Stores\\Files\\Config.txt" //Config path
    var sResultPath      =  g_sProjPath+"\Stores\\Files\\TCResult.txt"
    var strResult = "Failed"
    KillExcel(); //Function call to kill excel processes
    var ConfigArry = new Array();
    var ReturnValue = "failed";
    var g_TotalFail_Count = 0;
    ReadConfigData(sFilePath) //Read data from text file
    g_strAppname      =  GetDicParam("ApplicationName"); //Application Name
    g_strWebAppURL    =  GetDicParam("WebApplicationURL"); //Web Application URL
    g_strBrowserName  =  GetDicParam("BrowserName"); //Browser Name 
    g_mailURL         =  GetDicParam("MailURL");
    g_strFormattedUrl     =  FormatURL(g_strWebAppURL); //Format Web Application URL
    g_strBrowser =  GetBrowserName(g_strBrowserName); //Get Test Complete specific Browser name
    g_DBUserName      =  trim(GetDicParam("DBUserName")); 
    g_DBPassword      =  trim(GetDicParam("DBPassword")); 
    g_DBServiceName = GetDicParam("DBServiceName"); 
    g_DBPort = GetDicParam("DBPort");
    g_DBHost = GetDicParam("DBHost");
    g_DBTablename = GetDicParam("DBTable");
    g_strFrameWorkChkSum = GetDicParam("FrameworkChecksum");
    getbrowser_OSversion();
    fnOpenHtmlFile(g_strAppname); //Open HTML file
    g_objXLApp      =  Sys.OleObject("Excel.Application"); //create object for excel application
    g_objXLBook =  g_objXLApp.WorkBooks.Open(g_sTestDataPath) ;
    g_objXLApp.Visible = true;  //Making excel visible/invisible
    g_objXLApp.DisplayAlerts = false; //Excel will not display alerts     
    Dictionary(); //Function call to read all the object from excel
    //Load necessary objects
    StartExcelUtil();    
    g_objXLSht  =  g_objXLBook.Sheets("TestCases"); //Open TestCases sheet in excel workbook
    g_objXLApp.WindowState = -4137 //Window state Maximized
    g_objXLApp.WindowState = -4140 //Window state minimized
    var intMaxRowCnt  =  g_objXLSht.UsedRange.Rows.Count; //Variable to store maximum used row count in excel sheet
    g_intColCnt     =  g_objXLSht.UsedRange.Columns.Count; //Variable to store maximum used column count in excel sheet
    Log.Link(g_sSummaryFileName,"Execution Summary File");
    
    
    for (intRowIteration = 2; intRowIteration <= intMaxRowCnt; intRowIteration++){
      var intKeywordCounter = 0; //Variable to store the keyword count    
      execute_status = g_objXLSht.cells(intRowIteration, c_intExecuteStatusCol).value; //Variable to store execute status
      sTestcaseType =  g_objXLSht.cells(intRowIteration, c_intPreconditionsCol).value; //Variable to store precondition
      g_sTestCaseName = g_objXLSht.cells(intRowIteration, c_intTestCaseNameCol).value; //Variable to store testcase name
      g_sAutomationTestCase = g_objXLSht.cells(intRowIteration, c_intTestCaseIdCol).value; //Variable to store testcase name
      sTestCaseDesc = g_objXLSht.cells(intRowIteration, c_intTestCaseDescCol).value; //Variable to store testcase description
      if (execute_status == "Y") {
        g_intFailCnt = 0; //Variable to store fail count of keyword
        g_TestDataSheet = g_objXLSht.cells(intRowIteration, c_intTestCaseNumCol).value; //Variable to store testdata sheet name
        fnInsertSection (g_sTestCaseName, sTestCaseDesc); //Function call to insert section in report
        
        Log.AppendFolder(g_TestDataSheet); //Create a Log for the test
        Log.Message("------- Started execution for ------- " + g_TestDataSheet);        
        
        //Get the Total Steps in the test case
        g_objXLSht_TC  =  g_objXLBook.Sheets(g_TestDataSheet); //Open TestCases sheet in excel workbook    
        intMaxRowCnt_TC  =  g_objXLSht_TC.UsedRange.Rows.Count+1; //Variable to store maximum used row count in excel sheet
        var iRowIndex;
        var sStepContent;
        for(iRowIndex = intMaxRowCnt_TC; iRowIndex > 1; iRowIndex--){
            sStepContent = "";
            sStepContent = sStepContent + ReadExcel(iRowIndex,c_intObjNameCol);
            sStepContent = sStepContent + ReadExcel(iRowIndex,c_intInputValCol);
            sStepContent = sStepContent + ReadExcel(iRowIndex,c_intActionCol);
            sStepContent = sStepContent + ReadExcel(iRowIndex,c_intInputVal1Col);
            sStepContent = sStepContent + ReadExcel(iRowIndex,c_intStepCol);
            if (Trim(sStepContent).length > 0){
              intMaxRowCnt_TC = iRowIndex +1;
              break;
            }
        }
        
        g_RunTimeData.RemoveAll() //Clear the RunTime Data stored in Previous Test
        var g_LastStepNo = ReadExcel(intMaxRowCnt_TC-1,c_intStepCol);  //Reads the step from c_intStepCol
        g_TotalSteps = GetStepNumber(g_LastStepNo);
        
        for (intRowIteration_TC = 2; intRowIteration_TC <= intMaxRowCnt_TC; intRowIteration_TC++){   
        
          strKeywordName = ReadExcel(intRowIteration_TC,1);
          if ((intKeywordCounter == 1) && (intRowIteration_TC == intMaxRowCnt_TC )) {
            g_intStrtRow = row_num
            g_intEndRow = intMaxRowCnt_TC-1;  
            eval(g_strFuncCall+"()");
            intKeywordCounter= 0
            i = row_num
          }
            
          if ((strKeywordName) != null && (strKeywordName) != "") { 
            intKeywordCounter = intKeywordCounter + 1
            if (intKeywordCounter == 1) {
              row_num = intRowIteration_TC
              g_strFuncCall = strKeywordName 
            }
            if (intKeywordCounter == 2) {
              lastrow_num = intRowIteration_TC
              keyword1 = strKeywordName
            }
            if ((intKeywordCounter == 2) && (g_strFuncCall != null)) {
              g_intStrtRow = row_num 
              g_intEndRow =  lastrow_num - 1
              intRowIteration_TC = g_intEndRow
              eval(g_strFuncCall+"()");
              intKeywordCounter = 0
            }
          } 
             
        }//Keyword Loop ends
        
        //Check if keyword is failed
        if (g_iFail_Count == 0 && g_iPass_Count>0) { 
          fnInsertSummaryResult (g_sAutomationTestCase,g_sTestCaseName,"PASS");
        }
        else{
          fnInsertSummaryResult (g_sAutomationTestCase,g_sTestCaseName,"FAIL");
          g_TotalFail_Count = g_TotalFail_Count + 1
        }
        
        fnCloseHtml(); 
         CloseAllBrowsers();
        Log.Message("------- Ended execution for ------- " + g_TestDataSheet);        
        
        Log.PopLogFolder();
        Log.PopLogFolder();//popup twice.
                
      }  
    } //end of test case for loop       

  }
  catch(e){
    Log.Error(GetExceptionLocation(), GetExceptionInfo(e), pmHighest);
  }finally{
      fnCloseSummaryHtml(); //Close HTML Summary file      
      CloseAllBrowsers();
      if (g_TotalFail_Count == 0){ 
        ReturnValue = "Passed";                   
        strResult = "Passed";
        WriteResultData(sResultPath, strResult);
      }else{
        ReturnValue = "Failed";
        strResult = "Failed";
        WriteResultData(sResultPath, strResult);
      }
      StopExcelUtil();
      KillExcel(); //Function call to kill excel processes  
  }
    return ReturnValue;
} 
     
    
  

