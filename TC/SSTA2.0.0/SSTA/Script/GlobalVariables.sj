//Declare global variables
var g_mailURL;    //Variable to store the webmail url
var g_objXLBook;  //Variable to store excel book object
var g_sProjSuitePath    =  ProjectSuite.Path;  //Projectsuite path
var g_sProjPath         =  Project.Path;       //Project Path
var gStoresPath     = g_sProjPath+"\Stores\\Files\\"; //Stores Path
var g_sTestDataPath =  gStoresPath + "TestScript.xlsx"; //Testdata path
var g_objXLApp;            //object for excel application
var g_objXLSht;            //object for excel sheet 
var g_intFailCnt;          //Failed testcases count
var g_TestDataSheet;       //Testdata sheet name
var g_strFuncCall;         //Function Name
var g_sTestCaseName;       //Test Case Name
var g_OtherInputValue;
var g_FrameworkVersion = "SSTA-2.0.0"; //Variable to store the framework version
var g_strAppname;          //Application Name
var g_strWebAppURL;        //Web Application URL
var g_strFrameWorkChkSum;
var g_strFormattedUrl;     //Formatted Web Application URL
var g_strBrowser;          //Browser Name
var g_PageObject;           //Page Object Property
var oPageObject;           //Page Object
var g_TCVersion;           //Testcomplete version
var g_strBrowserName;      //Variable to store Browser Name
var g_strBrowserName1;     //Variable to store Second Browser Name
var g_dicParams;       //Dictionary object to refer Object Repository
var g_dicColNames_obj = Sys.OleObject("Scripting.Dictionary");   //Variable to store the dictionary key
var g_dicParams = Sys.OleObject("Scripting.Dictionary");
var g_RunTimeData = Sys.OleObject("Scripting.Dictionary");//To store values and use them later
g_RunTimeData.CompareMode = 1 //Use TextComparison
var g_intStrtRow;          //Start row of a keyword
var g_intEndRow;           //End row of a keyword
var c_intObjNameCol = 2;   //ObjectName column in test data sheet of the excel
var c_intInputValCol = 3;  //Input Value column in test data sheet of the excel
var c_intActionCol = 4;    //Action column in test data sheet of the excel
var c_intInputVal1Col = 5; //Input value1 column in test data sheet of the excel
var c_intStepCol = 6;      //Step column in test data sheet of the excel  
var c_intExecuteStatusCol = 3;  //Exceute Status column in TestCases sheet of the excel
var c_intPreconditionsCol = 4;  //Preconditions column in TestCases sheet of the excel
var c_intTestCaseNameCol = 5;   //TestCase Name column in TestCases sheet of the excel
var c_intTestCaseDescCol = 6;   //TestCase Description column in TestCases sheet of the excel
var c_intTestCaseNumCol = 1;    //TestCase number column in TestCases sheet of the excel
var c_intTestCaseIdCol = 2;    //TestCase number column in TestCases sheet of the excel
var c_VerifnPointExpected = 8; // Expected Verification Point column
var c_VerifnPointActual = 9; //Actual Pass Verification Point Column
//var c_VerifnPointActualFail = 9;  //Actual Fail Verification Point Column
var c_intStepType = 7;    //Step Type column in test data sheet of the excel
//var c_intTestStepDescCol = 10;   //TestStep Description column in test data sheet of the excel
var g_strAddedPatient;  //Variable for storing patient name
var g_sAutomationTestCase;  //Variable to store Testcase Id
var exitloop=0
var g_intColCnt;  //Variable for storing column count
var g_sScreenName1;  //Variable for storing screen name
var PDFfilename1="";  //Variable for storing pdf file name
var PDFfilename="";  //Variable for storing pdf file name
var csvfilename="";  //Variable for storing csv file name
var csvfilename1="";  //Variable for storing csv file name
var xmlfilename = "";
var intMaxRowCnt_TC; // Variable for max count of testcase
var intRowIteration_TC; // Variable to store the row iteration
var g_mailURL; // Variable for mail url
var ExpectedMessage;
var ActualMessage;
var searchpatient=0;
var g_strObjectName;
var g_OSInfo;  //Variable to store OS name and version
var g_browserinfo; //Variable to store browser name and version
var g_TreatmentFileUpload=false; // variable for treatment file browser 
var strTime2;
var g_DBTablename; //Variable to store the database table name
var g_RowNumObjNotFound = 0;  //Variable to store the row number when object is not found. Used in WaitForObject method
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// About : Declaration of Constants and Variables for Reports Unit
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
var g_sFileName;                    //Report Log File Name
var g_sSummaryFileName;             //Summary Report Log File Name
var g_iCapture_Count = 0;           //Number of Images captured
var g_IsLastObjectWeb = true;
var g_iPass_Count = 0;                  //Pass Count
var g_iFail_Count = 0;                  //Fail Count
var g_iExecution_Count;             //Total Count
var g_iTestcaseStatusPass_Count;    //Variable for storing testcase pass count
var g_iTestcaseStatusFail_Count;    //Variable for storing testcase fail count
var g_iTestcaseStatusExecuted_Count;  //Variable for storing testcase executed count
var g_iTestcaseStatusPass_CountM ;
var g_iTestcaseStatusFail_CountM; 
var g_iTestcaseStatusNotExecuted_CountM; 
var g_tStart_Time;                       //Start Time
var g_tEnd_Time;                     //End Time
var g_sScreenName;                       //Screen shot name

var g_bitmap;
var strMainPath;                 //Test suite path
var g_iWarning_Count;                  //Warning Count
var g_iCapt_Count;                       //Capture Count

var g_SummaryFile;
var g_DetailFile;
var g_objReport;
var g_objFS;

var strFolderPath;
var g_sResultsPath   =  g_sProjPath+"\TestResults"; //Test Results Path
var strDate;
var sTime ;
var strTime;
var c_htmlbody = "<HTML> <style> .wrapword {white-space: -moz-pre-wrap !important;  /* Mozilla, since 1999 */white-space: -webkit-pre-wrap; /*Chrome & Safari */ white-space: pre-wrap;       /* css-3 */word-wrap: break-word;       /* Internet Explorer 5.5+ */word-break: break-all;white-space: normal;		}</style> <Title>Automation Results</Title><BODY><TABLE BORDER=0 CELLPADDING=3 CELLSPACING=1 WIDTH=100%>";
var c_htmlspace = "<TABLE BORDER=0 BGCOLOR=BLACK CELLPADDING=3 CELLSPACING=1 WIDTH=100%>";
var c_htmlend = "</TABLE></BODY></HTML>";
var c_htmlborder = "<TABLE BORDER=1 CELLPADDING=3 CELLSPACING=1 WIDTH=100%>";
var g_stepnum = "Step - 1";
var intColCnt;
var intIterCnt;
var intColIteration;
var g_report;  //To verify report content is copied in the notepad
//var Step_No=0;
var g_steptype;
 var g_ToolsFolderPath = "C:\\Tools\\7-Zip";
 var Txtfilename ;
 var g_TotalSteps  ;      
 var g_StepsNotExecuted ;
 var g_LastStepNo;
 var g_StepsExecuted;
 var g_DBUserName;
  var g_DBPassword;
  var g_DBServiceName;
  var g_DBPort;
  var g_DBHost;
var SettingsRequestxmlfilename = "";  //Variable to store SettingsRequest.xml file name
var GeneratedPDFReportTextFile  = ""; //Variable to store GeneratedPDFReport Name
var rep_steptype;
var g_tLastSessionExtensionTime = 0 ;  //Session Timeout for Adequest
var CalculationDifferenceTolerance = 0;
var IsAdequest = false;